Feature: Make a search

  Scenario: Make a search by name
    Given The user go to BATA IT site      
     When User search an item by name with search bar 
      | productName | 
      | nike        | 
      And check that the results showed are 6 and click on show more
     Then check that the user land on Searching page
  
  Scenario: Make a search by name + gender
    Given The user go to BATA IT site      
     When User search an item by name + gender with search bar 
      | productName | gender | 
      | nike        | donna  | 
      And check that the results showed are 6 and click on show more
     Then check that the user land on Searching page
  
  Scenario: Make a search by name + color
    Given The user go to BATA IT site      
     When User search an item by name + color with search bar 
      | productName | color | 
      | nike        | rosse | 
      And check that the results showed are 6 and click on show more
     Then check that the user land on Searching page
  
  Scenario: Make a search by name + color + gender
    Given The user go to BATA IT site      
     When User search an item by name + color + gender with search bar 
      | productName | color | gender | 
      | nike        | rosse | donna  | 
      And check that the results showed are 6 and click on show more
     Then check that the user land on Searching page
  
  Scenario: Make a search by name + color + gender
    Given The user go to BATA IT site      
     When User search an item by name + color + gender with search bar 
      | category | productName | color | gender | material | 
      | sandali  | nike        | nere  | donna  | pelle    | 
      And check that the results showed are 6 and click on show more
     Then check that the user land on Searching page
  
