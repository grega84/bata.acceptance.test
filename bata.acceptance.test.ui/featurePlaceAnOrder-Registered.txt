Feature: Place an order 

  Scenario Outline: Place an order for registered user home delivery single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order without promo and shippment at home
      | coupon | voucher | paymentMethod | 
      |        |         |               | 
      And the user pay with following data payment
      | paymentType | cardNumber | cvv | expiredDate | emailPaypal | passwordPaypal | 
      |             |            |     |             |             |                | 
     Then check confermation email received
  
  Scenario Outline: Place an order for registered user home delivery single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order with promo and shippment at home
      | coupon | voucher | paymentMethod | 
      |        |         |               | 
      And the user pay with following data payment
      | paymentType | cardNumber | cvv | expiredDate | emailPaypal | passwordPaypal | 
      |             |            |     |             |             |                | 
     Then check confermation email received
  
  Scenario Outline: Place an order for registered user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order with promo and shippment to desire store or located by GPS
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
      And the user pay with following data payment
      | paymentType | cardNumber | cvv | expiredDate | emailPaypal | passwordPaypal | 
      |             |            |     |             |             |                | 
     Then check confermation email received
  
  Scenario Outline: Place an order for registered user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order without promo and shippment to desire store or located by GPS
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
      And the user pay with following data payment
      | paymentType | cardNumber | cvv | expiredDate | emailPaypal | passwordPaypal | 
      |             |            |     |             |             |                | 
     Then check confermation email received
  
  Scenario Outline: Place an order for registered user desire store based on CAP - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order without promo and shippment to store desire or located by GPS
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
      And the user change store 
      And the user pay with following data payment
      | paymentType | cardNumber | cvv | expiredDate | emailPaypal | passwordPaypal | 
      |             |            |     |             |             |                | 
     Then check confermation email received  
  
  Scenario Outline: Place an order for registered user desire store single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order without promo and insert wrong info in select store field
      | dataStore | coupon | 
      |           |        | 
     Then NON SI SA! 
  
  Scenario Outline: Place an order for registered user desire store based on CAP - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order and shippment to store desire based on postal code
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
      And The user check the payment with paypal and accept t&s
      And the user insert wrong credential of paypal
      | paymentType | emailPaypal | passwordPaypal | 
      |             |             |                | 
     Then check confermation email received  
  
  Scenario Outline: Place an order for registered user desire store based on CAP - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order and shippment to store desire based on postal code
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
      And The user check the payment with paypal but doesn't accept t&s
      And the user insert credential of paypal
      | paymentType | emailPaypal | passwordPaypal | 
      |             |             |                | 
     Then check confermation email received  
  
  Scenario Outline: Place an order for registered user desire store based on CAP - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order and shippment to store desire based on postal code
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
      And The user check the payment with credit card
      And the user insert wrong data of credit card
      | paymentType | cardNumber | cvv | expiredDate | 
      |             |            |     |             | 
  
  Scenario Outline: Place an order for registered user desire store based on CAP - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order and shippment to store desire based on postal code with wrong coupon
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
  
  Scenario Outline: Place an order for registered user desire store based on CAP - single item
    Given The user log in to BATA IT site with credential
      | email                       | password    | phone | address | emailAddress | 
      | tester003@prismaprogetti.it | Prisma2018! |       |         |              | 
     When User choose an item and add it to cart 
      | product | quantity | size | color | 
      |         |          |      |       | 
      And place an order and shippment to store desire based on postal code with Voucher that can't be combined with other promo
      | dataStore | coupon | voucher | paymentMethod | 
      |           |        |         |               | 
     Then check confermation email received
  
  
