package com.bata.test.feature_impl;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;


import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.login.LoginPage;
import com.bata.pages.profile.ProfilePage;
import com.bata.test.TestOrchestator;

import bean.datatable.CredentialBean;
import common.CommonFields;
import common.WaitManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import test.automation.core.UIUtils;

public class Login 
{
	
	private static TestOrchestator test=TestOrchestator.get();
	private WebDriver driver;
	private HomePage home;
	private LoginPage login;
	private ProfilePage profilepage;

	@When("^The user log in to BATA site without credential$")
    public void the_user_log_in_to_bata_site_without_credential(List<CredentialBean> table) throws Throwable 
	{
		initLogin(table);
		
		gotoHomePage();
        //throw new PendingException();
    }
	
	private void gotoHomePage() throws Exception 
	{
		home=(HomePage) test.getPage(HomePage.class, test.driverConfig, this.test.language).get();
		home.closeCookieDiv();
	}

	private void initLogin(List<CredentialBean> table) throws Exception 
	{
		this.test=TestOrchestator.get();
		this.test.country=table.get(0).getCountry();
		this.test.getLanguage(table.get(0).getCountry());
		this.test.language=table.get(0).getCountry();
		
		DinamicData.getIstance().set(CommonFields.COUNTRY, this.test.country);

		//https://storefront:BT2018@
		String auth=table.get(0).getAuthUsername()+":"+table.get(0).getAuthPassword();
		DinamicData.getIstance().set(CommonFields.AUTHLOGIN, auth);
		
	    driver=test.getDriver(test.driverConfig);
	    
	    WaitManager.get().waitShortTime();
	    
	    if(UIUtils.ui().getCurrentProperties().getProperty("authlogin.image") != null)
		{
			this.test.baseImageToFindURL=java.net.URLDecoder.decode(Login.class.getClassLoader().getResource(UIUtils.ui().getCurrentProperties().getProperty("authlogin.image")).getPath(), StandardCharsets.UTF_8.name());
			
		    Utility.desktopStagingAccess(driver,table,this.test.baseImageToFindURL,this.test.typeOfTest);
		}
	    
	    if(Locators.getDriverType(driver) == Locators.DESKTOP_CHROME)
	    	driver.manage().window().maximize();
	   
	    
	    if(driver instanceof AndroidDriver && driver != null)
		{
			((AndroidDriver) driver).context("NATIVE_APP");
			
		    UIUtils.ui().startMobileVideoRecording((AppiumDriver<?>) driver);
		    
		    ((AndroidDriver) driver).context("CHROMIUM");
		    
		    this.test.videoRunning=true;
		}
	    if(Locators.getDriverType(driver) == Locators.IOS_SAFARI)
	    {
	    	this.test.baseVideoName=UIUtils.ui().videoRecoder(this.test.baseVideoNamePath+"-"+this.test.testIdJIRA, null);
			UIUtils.ui().startVideoRecording();
			this.test.videoRunning=true;
			
	    	Utility.IOSStagingAccess((IOSDriver<?>) driver, table.get(0).getAuthUsername(), table.get(0).getAuthPassword());
	    }
	}

	@When("^The user log in to BATA IT site with credential$")
	public void the_user_log_in_to_BATA_IT_site_with_credential(List<CredentialBean> table) throws Throwable 
	{
		initLogin(table);
		
		gotoHomePage();
		
		loginOK(table);
	}
	
	private void loginOK(List<CredentialBean> table) throws Exception 
	{
		home=(HomePage) test.getPage(HomePage.class, test.driverConfig, test.language);
    	login=home.goToLoginPage();
		login.loginOK(table.get(0));
	}

	@Given("^The user go to BATA site$")
	 public void the_user_go_to_bata_site(List<CredentialBean> credentialBean) throws Throwable 
	 {
	    initLogin(credentialBean);
		
	    gotoHomePage();
	 }
	 
	 @When("^The user insert the following credential$")
	 public void the_user_insert_the_following_credential(List<CredentialBean> credentialBean) throws Throwable 
	 {
		 loginOK(credentialBean);
	 }
	
	 @Then("^The user will be redirect on the profile page$")
	 public void the_user_will_be_redirect_on_the_profile_page() throws Throwable {
    	profilepage=(ProfilePage) test.getPage(ProfilePage.class, test.driverConfig, test.language);
	 }
	 
    @When("^The user insert the following wrong credential$")
    public void the_user_insert_the_following_wrong_credential(List<CredentialBean> credentialBean) throws Throwable 
    {
    	home=(HomePage) test.getPage(HomePage.class, test.driverConfig, test.language);
    	login=home.goToLoginPage();
    	login.loginKO(credentialBean.get(0));
    }

    @And("^The user logout to Bata site$")
    public void the_user_logout_to_bata_site() throws Throwable 
    {
    	login=(LoginPage) test.getPage(LoginPage.class, test.driverConfig, test.language);
    	login.goToHomePage();
    	login.logout(test.typeOfTest);
    }
    
    @And("^The user logs in again to Bata site$")
    public void the_user_logs_in_again_to_bata_site(List<CredentialBean> credentialBean) throws Throwable {
    	loginOK(credentialBean);    	
    }
    
    @Given("^The user go to BATA IT site$")
	public void the_user_go_to_BATA_IT_site(List<CredentialBean> credentialBean) throws Throwable 
    {
    	initLogin(credentialBean);
		
		gotoHomePage();
	}
    
	@Given("^The user go to BATA site and logged user$")

	public void the_user_go_to_BATA_site_and_logged_user(List<CredentialBean> table) throws Throwable 
	{
		initLogin(table);
		gotoHomePage();
		loginOK(table);
		profilepage=(ProfilePage) test.getPage(ProfilePage.class, test.driverConfig, test.language);
		this.profilepage.goToHomePage();
	}
}
