package com.bata.test.feature_impl;

import java.util.List;

import com.bata.pages.contactpage.ContactPage;
import com.bata.pages.homepage.HomePage;
import com.bata.test.TestOrchestator;

import bean.datatable.ContactBean;
import cucumber.api.java.en.And;

public class SendContact 
{
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage home;
	private ContactPage contact;
	
	@And("^go to send contact page$")
    public void go_to_send_contact_page() throws Throwable 
    {
		home=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        contact=home.gotoSendContactPage();
    }
	
	@And("^complete all fields with following data$")
    public void complete_all_fields_with_following_data(List<ContactBean> table) throws Throwable 
    {
		contact.compileForm(table.get(0),test.getLanguage(test.language));
        
    }

    @And("^send the request$")
    public void send_the_request() throws Throwable 
    {
    	contact.sendRequest();
    	contact.checkSendRequestPopup(test.getLanguage(test.language));
        contact.closePopup();
    }
}
