package com.bata.test.feature_impl;

import java.util.List;

import com.bata.pages.homepage.HomePage;
import com.bata.pages.seach.SearchingPage;
import com.bata.pages.seach.SearchingPageFiltered;
import com.bata.test.TestOrchestator;

import bean.datatable.ProductBean;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductSearch 
{
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage homepage;
	private SearchingPage searchingpage;
	private SearchingPageFiltered searchingPageFiltered;
	
	@When("^User search an item by name with search bar$")
	public void user_search_an_item_by_name_with_search_bar(List<ProductBean> productBean) throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.searchProduct(productBean.get(0));
	    homepage.countItemsPreviewSearched();
	    homepage.chooseItem();
	}
	
	@When("^check that the results showed are six and click on show more$")
	public void check_that_the_results_showed_are_six_and_click_on_show_more() throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.countItemsPreviewSearched();
	    homepage.showAll();
	}
	
	@Then("^check that the user land on Searching page$")
	public void check_that_the_user_land_on_Searching_page() throws Throwable 
	{
		searchingpage=(SearchingPage)test.getPage(SearchingPage.class, test.driverConfig, test.language);
		searchingpage.get();
	}
	
	@When("^User search an item by name \\+ gender with search bar$")
	public void user_search_an_item_by_name_gender_with_search_bar(List<ProductBean> productBean) throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.searchProduct(productBean.get(0))
	    .countItemsPreviewSearched();
	}

	@When("^User search an item by name \\+ color with search bar$")
	public void user_search_an_item_by_name_color_with_search_bar(List<ProductBean> productBean) throws Throwable {
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.searchProduct(productBean.get(0))
	    .countItemsPreviewSearched();
	}

	@When("^User search an item by name \\+ color \\+ gender with search bar$")
	public void user_search_an_item_by_name_color_gender_with_search_bar(List<ProductBean> productBean) throws Throwable {
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.searchProduct(productBean.get(0))
	    .countItemsPreviewSearched();
	}
	
	@When("^User search an item by bataID with search bar$")
	public void user_search_an_item_by_bataID_with_search_bar(List<ProductBean> productBean) throws Throwable {
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.searchProduct(productBean.get(0));
	    homepage.showAll();
	}
	
	@When("^User search an item and check category of results$")
    public void user_search_an_item_and_check_category_of_results(List<ProductBean> productBean) throws Throwable {
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.searchProductAndClickOnCategory(productBean.get(0));
    	searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	searchingPageFiltered.checkCategoryName(productBean.get(0));
    }
	
	@When("^User search an item$")
    public void user_search_an_item(List<ProductBean> productBean) throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.searchProduct(productBean.get(0));
        try {Thread.sleep(2000);}catch(Exception err) {}
    }
	
	@Then("^Are you searching for Suggestions is displayed$")
    public void are_you_searching_for_suggestions_is_displayed() throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.checkSuggetionSearchLink();
    }
	
	@And("^click on suggestion link$")
    public void click_on_suggestion_link() throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.clickOnSuggestionLinkSearch();
    }
	
	@And("^click on show more link$")
    public void click_on_show_more_link() throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.showAll();
    }
	
	@And("^click on popular search item$")
    public void click_on_popular_search_item() throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.checkPopularSearchSection();
        homepage.clickOnFirstPopularSearchElement();
    }

    @And("^click on category link$")
    public void click_on_category_link() throws Throwable 
    {
    	homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.checkCategorySection();
        homepage.clickOnFirstCategorySearchElement();
    }
}
