package com.bata.test.feature_impl;

import com.bata.pages.homepage.HomePage;
import com.bata.pages.login.LoginPage;
import com.bata.pages.minicart.MiniCartPage;
import com.bata.pages.product.ProductPage;
import com.bata.pages.seach.SearchingPage;
import com.bata.pages.wishlist.WishlistPage;
import com.bata.test.TestOrchestator;
import com.bata.ui.support.PageSupportUtility;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.automation.core.UIUtils;

public class WishList 
{
	private static TestOrchestator test=TestOrchestator.get();
	private ProductPage productpage;
	private SearchingPage searchinpage;
	private WishlistPage wishlistpage;
	private HomePage homepage;
	private LoginPage login;
	private MiniCartPage minicartpage;
	
	@When("^Click on add to whishlist guest$")
	public void click_on_add_to_whishlist_guest() throws Throwable 
	{
		productpage=(ProductPage)test.getPage(ProductPage.class,test.driverConfig,test.language);
		productpage.addToWishlistGuest();
	}
	
	@When("^Click on add to whishlist registered$")
	public void click_on_add_to_whishlist_registered() throws Throwable 
	{
		productpage=(ProductPage)test.getPage(ProductPage.class,test.driverConfig,test.language);
		productpage.addToWishlistRegistered();
	}
	
	@When("^Click on add to whishlist from list page registered$")
	public void click_on_add_to_whishlist_from_list_page_registered() throws Throwable {
		searchinpage=(SearchingPage)test.getPage(SearchingPage.class,test.driverConfig,test.language);
		searchinpage.focusOnItem()
			.addToWishlistItemFocusedRegistered();		
	}

	@When("^Click on add to whishlist from list page guest$")
	public void click_on_add_to_whishlist_from_list_page_guest() throws Throwable {
		searchinpage=(SearchingPage)test.getPage(SearchingPage.class,test.driverConfig,test.language);
		searchinpage.focusOnItem()
				.addToWishlistItemFocusedGuest();		
	}
	
	@Then("^Item is added in wishlist page registered$")
	public void item_is_added_in_wishlist_page_registered() throws Throwable {
		//productpage=new ProductPage(driver).get();
		//productpage.clickOnWishlistIcon();
		wishlistpage=PageSupportUtility.clickOnWishlistIcon(test.getDriver(test.driverConfig),test.getLanguage(test.language));
	    //new WishlistPage(driver).get();
	    wishlistpage.checkElementAddedToWishlist();
	}

	@When("^User go to wishlist page$")
	public void user_go_to_wishlist_page() throws Throwable 
	{
		homepage = (HomePage)test.getPage(HomePage.class,test.driverConfig,test.language);
		homepage.goToWishlistPage();
	}
	   
	@When("^Check if item is added in wishlist page$")
	public void check_if_item_is_added_in_wishlist_page() throws Throwable {
		productpage.clickOnWishlistIcon();
	    wishlistpage=(WishlistPage)test.getPage(WishlistPage.class,test.driverConfig,test.language);
	    wishlistpage.checkElementAddedToWishlist();
	}

	@When("^Remove the item from wishlist page$")
	public void remove_the_item_from_wishlist_page() throws Throwable {
		 wishlistpage=(WishlistPage)test.getPage(WishlistPage.class,test.driverConfig,test.language);
		 wishlistpage.removeItemAddedFromWishlistPage();
	}

	@Then("^The user remove the item from wishlist$")
	public void the_user_remove_the_item_from_wishlist() throws Throwable {
		//wishlistpage=PageSupportUtility.clickOnWishlistIcon(driver);
		wishlistpage=(WishlistPage)test.getPage(WishlistPage.class,test.driverConfig,test.language);
		wishlistpage.removeItemAddedFromWishlistPage();
		Thread.sleep(2000);
	}
	
	@Then("^Item is removed from wishlist page$")
	public void item_is_removed_from_wishlist_page() throws Throwable {
		wishlistpage=PageSupportUtility.clickOnWishlistIcon(test.getDriver(test.driverConfig),test.getLanguage(test.language));
	    wishlistpage.checkWishlistEmpty();
	}
	
	@And("^Remove the item previously putted in the wishlist from list page$")
	public void remove_the_item_previously_putted_in_the_wishlist_from_list_page() throws Throwable {
		
		searchinpage=(SearchingPage)test.getPage(SearchingPage.class,test.driverConfig,test.language);
		searchinpage.focusOnItem()
				.removeToWishlistItemFocused();	
		
		Thread.sleep(2000);
		UIUtils.ui().scroll(test.getDriver(test.driverConfig), UIUtils.SCROLL_DIRECTION.UP, 100);
	}
	

    @Then("^User will be redirect on the login page$")
    public void user_will_be_redirect_on_the_login_page() throws Throwable 
    {
    	login=(LoginPage)test.getPage(LoginPage.class,test.driverConfig,test.language).get();
    }
	   
    
    @And("^Click on copy url and paste in new chrome tab$")
    public void click_on_copy_url_and_paste_in_new_chrome_tab() throws Throwable {
    	Thread.sleep(1000);
		UIUtils.ui().scroll(test.getDriver(test.driverConfig), UIUtils.SCROLL_DIRECTION.UP, 100);
    	wishlistpage=PageSupportUtility.clickOnWishlistIcon(test.getDriver(test.driverConfig),test.getLanguage(test.language));
    	wishlistpage.copyUrl();
    }
    
    @Then("^The user will be reidirect in wish list$")
    public void the_user_will_be_reidirect_in_wish_list() throws Throwable {
    	Thread.sleep(2000);
    	wishlistpage=(WishlistPage)test.getPage(WishlistPage.class,test.driverConfig,test.language).get();
		
    }
    
    @And("^The user add to cart one item in wishlist page$")
    public void the_user_add_to_cart_one_item_in_wishlist_page() throws Throwable 
    {
    	wishlistpage=PageSupportUtility.clickOnWishlistIcon(test.getDriver(test.driverConfig),test.getLanguage(test.language))
    			.checkElementAddedToWishlist()
    			.addToCartItemFromWishlist();
    }
    
    @Then("^Item added to the cart, minicart slider open$")
    public void item_added_to_the_cart_minicart_slider_open() throws Throwable 
    {
    	minicartpage=(MiniCartPage)test.getPage(MiniCartPage.class,test.driverConfig,test.language);
    }
}
