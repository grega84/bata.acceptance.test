package com.bata.test.feature_impl;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;


import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.guerillamail.GuerrillaMailPage;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.paypalpayment.PaypalPaymentPage;
import com.bata.pages.paypalpayment.PaypalPaymentRiepilogoPage;
import com.bata.pages.purchaseandoders.PurchasesAndOrdersPage;
import com.bata.pages.trackanorder.TrackAnOrderDetailsPage;
import com.bata.pages.trackanorder.TrackAnOrderPage;
import com.bata.test.TestOrchestator;

import bean.datatable.CredentialBean;
import bean.datatable.EmailRecieveBean;
import bean.datatable.PaymentBean;
import bean.datatable.ProductBean;
import bean.datatable.RiepilogoBean;
import bean.datatable.SearchItemBean;
import bean.datatable.ShippingBean;
import bean.datatable.TrackOrderBean;
import common.CommonFields;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import test.automation.core.UIUtils;

public class TrackAnOrder 
{
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage home;
	private TrackAnOrderPage trackOrder;
	private TrackAnOrderDetailsPage trakOrderDetails;
	private SimpleDateFormat dateFormat=new SimpleDateFormat("dd.MM.yyyy");
	
    

    @Then("^check the order details$")
    public void check_the_order_details(List<TrackOrderBean> table) throws Throwable 
    {
    	trakOrderDetails=(TrackAnOrderDetailsPage)test.getPage(TrackAnOrderDetailsPage.class, test.driverConfig, test.language);
        this.trakOrderDetails.checkOrder(table.get(0));
    }

   

    @Then("^check the error message is correctly displayed$")
    public void check_the_error_message_is_correctly_displayed(List<TrackOrderBean> table) throws Throwable 
    {
    	trackOrder=(TrackAnOrderPage)test.getPage(TrackAnOrderPage.class, test.driverConfig, test.language);
        this.trackOrder.checkErrorMessage(table.get(0).getTrackErrorMessage());
    }

    @And("^Access to the track order page from the footer link with following data$")
    public void access_to_the_track_order_page_from_the_footer_link_with_following_data(List<TrackOrderBean> table) throws Throwable 
    {
        TrackOrderBean b=table.get(0);
        if(b.getRegisteredUser() == null || b.getRegisteredUser().equals("n"))
        {
        	home=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        	this.trackOrder=home.gotoTrackAnOrderPage();
            this.trakOrderDetails=this.trackOrder.gotoDetails(table.get(0), test.getLanguage(test.language));
        }
        else if(b.getRegisteredUser() != null || b.getRegisteredUser().equals("y"))
        {
        	home=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        	PurchasesAndOrdersPage p=home.gotoPurchaseAndOrdersPage();
        	this.trakOrderDetails=p.gotoTrackOrderDetails(b, test.getLanguage(test.language));
        }
    }

    @And("^check the order status is equal to$")
    public void check_the_order_status_is_equal_to(List<TrackOrderBean> table) throws Throwable 
    {
    	trakOrderDetails=(TrackAnOrderDetailsPage)test.getPage(TrackAnOrderDetailsPage.class, test.driverConfig, test.language);
        
    	if(test.riepilogoBean == null)
    		this.trakOrderDetails.checkOrderStatus(table.get(0));
    	else
    	{
    		//aggiorno i dati dinamici
    		TrackOrderBean b=table.get(0);
    		String orderNumber=Utility.replacePlaceHolders(b.getOrderNumber(), new Entry("orderNumber",test.riepilogoBean.getOrderNumber().replace(test.ORDER_NUMBER_HEADER, "").trim()));
    		
    		Calendar c=Calendar.getInstance();
    		Date date=c.getTime();
    		
    		String orderHeader=Utility.replacePlaceHolders(b.getOrderDetailHeader(), new Entry("orderDate",dateFormat.format(date)));
    		
    		b.setOrderDetailHeader(orderHeader);
    		b.setOrderNumber(orderNumber);
    		
    		this.trakOrderDetails.checkOrderStatus(b);
    	}
    }
    
    
}
