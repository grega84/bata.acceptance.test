package com.bata.test.feature_impl;

import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Utility;
import com.bata.pages.gmail.GmailMailPage;
import com.bata.pages.javaxmail.JavaxMailPage;
import com.bata.pages.trackanorder.TrackAnOrderDetailsPage;
import com.bata.test.TestOrchestator;

import bean.datatable.ContactBean;
import bean.datatable.EmailRecieveBean;
import bean.datatable.JIRATestBean;
import bean.datatable.RiepilogoBean;
import common.CommonFields;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import test.automation.core.UIUtils;

public class Emails 
{
	private static TestOrchestator test=TestOrchestator.get();
	private WebDriver guerrillaDriver;
	private JavaxMailPage guerilla;
	private WebDriver guerillaDriver;
	private TrackAnOrderDetailsPage trakOrderDetails;
	
	@And("^The user check the received email$")
	 public void the_user_check_the_received_email(List<JIRATestBean> table) throws Throwable 
	 {
		JIRATestBean b=table.get(0);
		
		 guerrillaDriver = test.getDriver(test.driverMailConfig);
		 
		 if (test.typeOfTest.equals("webTest"))
			 guerrillaDriver.manage().window().maximize();
		 
		 String subject="";
		 
		 if(test.country.equals("ita"))
		 {
			 subject="Benvenuto nel Bata Club";
		 }
		 else if(test.country.equals("cz"))
		 {
			 subject="Potvrďte svou registraci do Baťa klubu";
		 }
		 else if(test.country.equals("pl"))
		 {
			 subject="MyBata - Autoryzacja adres e-mail";
		 }
		 else if(test.country.equals("sk"))
		 {
			 subject="Potvrďte svoju registráciu do Baťa klubu";
		 }
		 
		 guerilla=(JavaxMailPage) test.getPage(JavaxMailPage.class, test.driverMailConfig, test.language);
		 guerilla.init(b.getDriverMailConfig())
		 .checkEmail((String) DinamicData.getIstance().get(CommonFields.TEMP_MAIL),subject)
		 .openMail(subject.trim())
		 .clickOnRegisterLink();
		 
		 guerrillaDriver.quit();
	 }
	
	@And("^click on confirmation email$")
    public void click_on_confirmation_email() throws Throwable 
    {
        throw new PendingException();
    }
	
	@Then("^check confermation email received$")
	public void check_confermation_email_received(List<EmailRecieveBean> table) throws Throwable 
	{
		
		openMailProvider();
		
	    RiepilogoBean bean=(RiepilogoBean) DinamicData.getIstance().get(CommonFields.RECAP_ORDER);
	    //ricavo il numero dell'ordine
	    String orderNumber=bean.getOrderNumber().replace(test.ORDER_NUMBER_HEADER, "").trim();
	    
	    switch(test.country)
	    {
	    case "cz":
	    	orderNumber=bean.getOrderNumber().replace(test.ORDER_NUMBER_HEADER_CZ, "").trim();
	    	break;
	    case "pl":
	    	orderNumber=bean.getOrderNumber().replace(test.ORDER_NUMBER_HEADER_PL, "").trim();
	    	break;
	    case "sk":
	    	orderNumber=bean.getOrderNumber().replace(test.ORDER_NUMBER_HEADER_SK, "").trim();
	    	break;
	    }
	    
	    //controllo che la mail sia stata ricevuta
	    EmailRecieveBean mail=table.get(0);
	    
	    guerilla
	    .init(table.get(0).getDriverMailConfig())
	    .checkEmail(mail.getEmailAddress(), Utility.replacePlaceHolders(mail.getEmailSubject(), new Entry("orderNumber",orderNumber)))
	    .openMail(Utility.replacePlaceHolders(mail.getEmailSubject(), new Entry("orderNumber",orderNumber)))
	    
	    //controllo struttura mail di riepilogo
	    .checkSummaryMail(Utility.replacePlaceHolders(mail.getEmailSubject(), new Entry("orderNumber",orderNumber)),bean,test.country);
	    
	    //guerillaDriver.quit();
	}
	
	@And("^check the notification mail is sent to the email address$")
    public void check_the_notification_mail_is_sent_to_the_email_address(List<ContactBean> table) throws Throwable 
    {
		openMailProvider();
	    
	    ContactBean b=table.get(0);
	    
	    String reason=b.getReason();
	    
	    b.setReason(test.getLanguage(test.language).getProperty(b.getReason()));
	    
	    guerilla
	    .login(null, null)
	    .checkEmail(b.getEmail(), Utility.replacePlaceHolders(b.getEmailSubject(), new Entry("reason",test.getLanguage(test.language).getProperty(reason+"-subject"))))
	    .openMail(Utility.replacePlaceHolders(b.getEmailSubject(), new Entry("reason",test.getLanguage(test.language).getProperty(reason+"-subject"))))
	    .checkSummaryMail(Utility.replacePlaceHolders(b.getEmailSubject(), new Entry("reason",test.getLanguage(test.language).getProperty(reason+"-subject"))), b, test.SEND_REQUEST+test.country);
	    
	    guerillaDriver.quit();
	
    }

	private void openMailProvider() throws Exception 
	{
		//apro il browser su guerilla mail
	    //guerillaDriver = UIUtils.ui().driver("guerrillaMail");
		guerillaDriver = test.getDriver(test.driverMailConfig);
	    guerillaDriver.manage().window().maximize();
	    
	    if(!test.typeOfTest.contains("web"))
	    {
	    	test.webVideo=UIUtils.ui().videoRecoder(test.baseVideo+"_WEB_", null);
	    	UIUtils.ui().startVideoRecording();
	    }
	    
	    //GuerrillaMailPage guerilla = GuerrillaMailPage.from(driver, language);
	    guerilla= (JavaxMailPage) test.getPage(JavaxMailPage.class, test.driverMailConfig, test.language);
	    
	}
	
	@And("^Access to the track order page from the email summary link$")
    public void access_to_the_track_order_page_from_the_email_summary_link() throws Throwable 
    {
		String registered=(String) DinamicData.getIstance().get(CommonFields.REGISTERED_USER);
		
		if(registered == null || registered.equals("n"))
			this.trakOrderDetails=guerilla.clickRiepilogoOrdine(test.getLanguage(test.language), Duration.ofMinutes(3).toMillis());
		else if(registered != null && registered.equals("y"))
		{
			guerilla.clickRiepilogoOrdineRegisteredUser(test.getLanguage(test.language),(String) DinamicData.getIstance().get(CommonFields.ORDER_ID), Duration.ofMinutes(3).toMillis());
		}
		
        //ricavo il bean del riepilogo
        this.test.riepilogoBean=(RiepilogoBean) DinamicData.getIstance().get(CommonFields.RECAP_ORDER);
        Assert.assertTrue(test.riepilogoBean != null);
    }
}
