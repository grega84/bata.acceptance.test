package com.bata.test.feature_impl;

import java.util.List;

import com.bata.pages.login.LoginPage;
import com.bata.pages.myprofile.MyProfilePage;
import com.bata.pages.profile.ProfilePage;
import com.bata.test.TestOrchestator;

import bean.datatable.CredentialBean;
import bean.datatable.DatiDiContattoBean;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class UserUpdateInfo 
{
	private static TestOrchestator test=TestOrchestator.get();
	private ProfilePage profile;
	private MyProfilePage myProfile;
	private LoginPage login;

    @And("^access to myprofile$")
    public void access_to_myprofile() throws Throwable 
    {
    	profile=(ProfilePage) test.getPage(ProfilePage.class, test.driverConfig, test.language);
        myProfile=profile.gotoMyProfile(test.getLanguage(test.language));
    }

    @And("^edit contact details with following data$")
    public void edit_contact_details_with_following_data(List<DatiDiContattoBean> table) throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile.modificaDatiContatto(table.get(0));
    }
    
    @Then("^check if the contact details are updated$")
    public void check_if_the_contact_details_are_updated() throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile.checkModificaDatiContatto();
    }
    
    @And("^change the password with this$")
    public void change_the_password_with_this(List<CredentialBean> table) throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile.changePassword(table.get(0));
    }
    
    @Then("^check if the password is changed$")
    public void check_if_the_password_is_changed(List<CredentialBean> table) throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        
        //procedo con il logout
    	login=myProfile.logout();
    	
    	//effettuo la login con i nuovi dati
    	this.profile=login.loginOK(table.get(0));
    	
    	//accedo a my profile
    	this.myProfile=profile.gotoMyProfile(test.getLanguage(test.language));
    }
    
    @And("^set the password to the old value$")
    public void set_the_password_to_the_old_value(List<CredentialBean> table) throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile.changePassword(table.get(0));
    	//procedo con il logout
    	login=myProfile.logout();
    	
    	String passwd=table.get(0).getNewPassword();
    	table.get(0).setPassword(passwd);
    	
    	//effettuo la login con i nuovi dati
    	this.profile=login.loginOK(table.get(0));
    }
    
    @And("^edit personal data with following data$")
    public void edit_personal_data_with_following_data() throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile.changePersonalData(test.getLanguage(test.language));
    }
    
    @Then("^check if the personal data is updated$")
    public void check_if_the_personal_data_is_updated() throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        this.myProfile.checkModificaDatiPersonali();
    }
    
    @And("^add a new address$")
    public void add_a_new_address() throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile.addNuovoIndirizzo();
    }

    @Then("^check if the new address is correctly added$")
    public void check_if_the_new_address_is_correctly_added() throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile.checkNewAddressAdded();
        
    }
    
    @And("^complete all the loyalty mandatory fields$")
    public void complete_all_the_loyalty_mandatory_fields() throws Throwable 
    {
    	myProfile=(MyProfilePage) test.getPage(MyProfilePage.class, test.driverConfig, test.language);
        myProfile=profile.gotoMyProfile(test.getLanguage(test.language));
        myProfile.completeMandatoryFields(test.getLanguage(test.language));
    }
    
    @Then("^check if points and tier are updated$")
    public void check_if_points_and_tier_are_updated() throws Throwable {
        throw new PendingException();
    }
    
    @Then("^check if points and tier are updated with 20 loyalty points$")
    public void check_if_points_and_tier_are_updated_with_20_loyalty_points() throws Throwable {
        throw new PendingException();
    }
}
