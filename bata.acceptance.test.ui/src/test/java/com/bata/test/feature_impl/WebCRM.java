package com.bata.test.feature_impl;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Utility;
import com.bata.pages.crm.CRMPage;
import com.bata.test.TestOrchestator;

import bean.datatable.CRMCustomerInfoSearchBean;
import bean.datatable.CredentialBean;
import common.CommonFields;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import test.automation.core.UIUtils;

public class WebCRM 
{
	private static TestOrchestator test=TestOrchestator.get();
	private WebDriver driver;
	private CRMPage crm;
	
	@When("^login to the web CRM with following credential$")
    public void login_to_the_web_crm_with_following_credential(List<CredentialBean> table) throws Throwable 
    {
		this.test.country=table.get(0).getCountry();
		this.test.getLanguage(table.get(0).getCountry());
		this.test.language=table.get(0).getCountry();
		
		DinamicData.getIstance().set(CommonFields.COUNTRY, this.test.country);
		
		driver=test.getDriver(test.driverConfig);
		driver.get("http://test.bata.serilab.eu/login");
		driver.manage().window().maximize();
		
		crm=(CRMPage) test.getPage(CRMPage.class, test.driverConfig, test.language);
    }
	
	@And("^goto customer section$")
    public void goto_customer_section() throws Throwable 
    {
	   crm=(CRMPage) test.getPage(CRMPage.class, test.driverConfig, test.language);
       crm.gotoCustomerSection();
    }

    @And("^select an user$")
    public void select_an_user(List<CRMCustomerInfoSearchBean> table) throws Throwable 
    {
    	crm=(CRMPage) test.getPage(CRMPage.class, test.driverConfig, test.language);
        crm.searchAndSelectUser(table.get(0));
    }

    @And("^update the customer info$")
    public void update_the_customer_info() throws Throwable 
    {
    	crm=(CRMPage) test.getPage(CRMPage.class, test.driverConfig, test.language);
        crm.updateSelectedCustomerInfo();
    }

    @And("^save the modify$")
    public void save_the_modify() throws Throwable 
    {
    	crm=(CRMPage) test.getPage(CRMPage.class, test.driverConfig, test.language);
        crm.saveUpdateCustomerInfo();
        driver.quit();
    }
}
