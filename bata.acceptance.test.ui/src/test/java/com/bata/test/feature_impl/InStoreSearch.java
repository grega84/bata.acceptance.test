package com.bata.test.feature_impl;

import java.util.List;

import com.bata.pages.findinstore.FindInStoreProductPage;
import com.bata.pages.product.ProductPage;
import com.bata.test.TestOrchestator;

import bean.datatable.FindAStoreProductBean;
import cucumber.api.java.en.And;

public class InStoreSearch 
{
	private static TestOrchestator test=TestOrchestator.get();
	private ProductPage productPage;
	private FindInStoreProductPage findAStore;

	@And("^Click on find in store button$")
    public void click_on_find_in_store_button() throws Throwable 
    {
		productPage=(ProductPage)test.getPage(ProductPage.class, test.driverConfig, test.language);
        findAStore=productPage.clickFindAStore();
    }
	
	@And("^Search an store based on this criteria$")
    public void search_an_store_based_on_this_criteria(List<FindAStoreProductBean> table) throws Throwable 
    {
        findAStore=(FindInStoreProductPage)test.getPage(FindInStoreProductPage.class, test.driverConfig, test.language);
		findAStore.search(table.get(0));
    }
	
	@And("^the store list is displayed$")
    public void the_store_list_is_displayed(List<FindAStoreProductBean> table) throws Throwable 
    {
		findAStore=(FindInStoreProductPage)test.getPage(FindInStoreProductPage.class, test.driverConfig, test.language);
		findAStore.checkList(table.get(0),test.getLanguage(test.language));
    }

    @And("^click on load more button$")
    public void click_on_load_more_button(List<FindAStoreProductBean> table) throws Throwable 
    {
    	findAStore=(FindInStoreProductPage)test.getPage(FindInStoreProductPage.class, test.driverConfig, test.language);
		findAStore.clickLoadMore();
        findAStore.checkListAfterClickMore(table.get(0).getCriteria());
    }
	
}
