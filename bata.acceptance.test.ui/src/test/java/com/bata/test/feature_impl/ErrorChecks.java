package com.bata.test.feature_impl;

import java.util.List;

import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pages.cartpage.CartPage;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.login.LoginPage;
import com.bata.pages.paypalpayment.PaypalPaymentPage;
import com.bata.pages.registration.RegistrationPage;
import com.bata.pages.shipping.ShippingPage;
import com.bata.test.TestOrchestator;

import bean.datatable.RegistrationBean;
import bean.datatable.ShippingBean;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;

public class ErrorChecks 
{
	private static TestOrchestator test=TestOrchestator.get();
	private LoginPage login;
	private RegistrationPage registrationpage;
	private PaypalPaymentPage paypal;
	private CartPage cart;
	private ShippingPage shipPage;
	private HomePage homepage;
	
	@Then("^The user is notified with an error message$")
    public void the_user_is_notified_with_an_error_message() throws Throwable 
	{
		login=(LoginPage) test.getPage(LoginPage.class, test.driverConfig, test.language);
    	login.checkMessageWrongCredential();
    }
	
	@Then("^check the error message for the field$")
    public void check_the_error_message_for_the_field(List<RegistrationBean> table) throws Throwable 
    {
		registrationpage=(RegistrationPage) test.getPage(RegistrationPage.class, test.driverConfig, test.language);
    	registrationpage.checkErrorMessage(table.get(0));
    }
	
	 @Then("^check the paypal error is displayed$")
    public void check_the_paypal_error_is_displayed() throws Throwable 
	{
		 paypal=(PaypalPaymentPage) test.getPage(PaypalPaymentPage.class, test.driverConfig, test.language);
		 paypal.checkErrorMessage(test.getLanguage(test.language));
    }
	 
    @Then("^check the coupon error message is displayed$")
    public void check_the_coupon_error_message_is_displayed() throws Throwable 
    {
    	cart=(CartPage)test.getPage(CartPage.class, test.driverConfig, test.language);
        cart.checkCouponErrorMessage(test.getLanguage(test.language));
    }
    
    @Then("^check the voucher error message is displayed$")
    public void check_the_voucher_error_message_is_displayed() throws Throwable {
        throw new PendingException();
    }
    
    @Then("^check the store error message is displayed$")
    public void check_the_store_error_message_is_displayed(List<ShippingBean> t) throws Throwable 
    {
    	shipPage=(ShippingPage)test.getPage(ShippingPage.class, test.driverConfig, test.language);
        this.shipPage.checkStoreNotFound(test.getLanguage(test.language),t.get(0));
    }

	@Then("^the Sorry, we did not find any products message and other popular searches are displayed$")
    public void the_sorry_we_did_not_find_any_products_message_other_popular_searches_are_displayed() throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
	    homepage.checkNoMoreElementFindTextDisplayed()
        .checkPopularSearchSection();
    }
}
