package com.bata.test.feature_impl;

import java.util.List;

import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.login.LoginPage;
import com.bata.pages.registration.RegistrationPage;
import com.bata.test.TestOrchestator;

import bean.datatable.RegistrationBean;
import common.CommonFields;
import cucumber.api.java.en.When;

public class Registration 
{
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage homepage;
	private LoginPage login;
	private RegistrationPage registrationpage;
	
	@When("^The user insert the following data for the registration$")
	public void the_user_insert_the_following_data_for_the_registration(List<RegistrationBean> registrationBean) throws Throwable 
	{
		homepage=(HomePage) test.getPage(HomePage.class,test.driverConfig,test.language);
		login=homepage.goToLoginPage();
		registrationpage=login.goToRegistrationPage();
		
		if(registrationBean.get(0).getField() != null && registrationBean.get(0).getField().equals(test.TC_NOT_ACCEPTED_ERROR_CHECK))
		{
			DinamicData.getIstance().set(test.TC_NOT_ACCEPTED_ERROR_CHECK, test.TC_NOT_ACCEPTED_ERROR_CHECK);
		}
		else if(registrationBean.get(0).getField() != null && registrationBean.get(0).getField().equals(test.INVALID_PHONE_ERROR_CHECK))
		{
			DinamicData.getIstance().set(CommonFields.INVALID_PHONE_ERROR_CHECK, test.INVALID_PHONE_ERROR_CHECK);
		}
		registrationpage.insertDataRegistration(registrationBean.get(0),test.country);
	}

}
