package com.bata.test.feature_impl;

import java.util.ArrayList;
import java.util.List;

import com.bata.pagepattern.DinamicData;
import com.bata.pages.cartpage.CartPage;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.minicart.MiniCartPage;
import com.bata.pages.product.ProductPage;
import com.bata.test.TestOrchestator;

import bean.datatable.ProductBean;
import bean.datatable.SearchItemBean;
import common.CommonFields;
import cucumber.api.java.en.And;

public class ProductDetails 
{
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage home;
	private ProductPage productPage;
	private ProductPage product;
	private MiniCartPage miniCart;
	private CartPage cart;

	@And("^Go on a specific product detail page$")
    public void go_on_a_specific_product_detail_page(List<ProductBean> table) throws Throwable 
    {
		home=(HomePage)test.getPage(HomePage.class, test.driverConfig,test.language);
        home.searchProduct(table.get(0));
        productPage=home.chooseItem();
    }
	
	@And("^Select a size$")
    public void select_a_size(List<ProductBean> table) throws Throwable 
    {
		productPage=(ProductPage)test.getPage(ProductPage.class, test.driverConfig, test.language);
        productPage.selectSize(table.get(0));
    }
	

	
	@And("^User select items for a total amount of 500€$")
    public void user_select_items_for_a_total_amount_of_500(List<ProductBean> table) throws Throwable 
    {
    	List<SearchItemBean> beans=new ArrayList<SearchItemBean>();
    	
    	home=(HomePage)test.getPage(HomePage.class, test.driverConfig,test.language);
        
    	for(ProductBean prod : table)
	    {
	    	//ricerco il prodotto
			home.searchProduct(prod);
			//home.countItemsPreviewSearched();
			
			//seleziono il prodotto
			product=home.chooseItem();
			product.checkItemPage();
			
			//lo aggiungo all carrello
			product.selectDetailOfItemAndAddToCart();
			
			//controllo presenza nel carrello
			product.checkMiniCart();
			
			//chiudo il carrello
			product.closeMiniCart();
			
			//torno alla homepage
			home=product.gotoHomePage();
			
			//apro il mini cart menu
			miniCart=home.openMiniCart();
			
			//vado sulla cart page
			cart=miniCart.clickShowCart();
			
			//incremento di x unità l'item scelto
			cart.increaseItemQuantity(prod.getQuantity(),(SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM));
			
			//ritorno alla home
			home=cart.goToHome(test.getLanguage(test.language));
			
			SearchItemBean item=(SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM);
			item.setQuantity(prod.getQuantity());
			
			beans.add((SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM));
	    }
    	
    	DinamicData.getIstance().set(CommonFields.SELECTED_ITEMS, beans);
    }
}
