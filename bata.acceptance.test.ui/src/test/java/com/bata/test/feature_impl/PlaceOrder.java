package com.bata.test.feature_impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import com.bata.pagepattern.DinamicData;
import com.bata.pages.PayWith;
import com.bata.pages.benvenutocheckout.BenvenutoCheckoutPage;
import com.bata.pages.cartpage.CartPage;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.minicart.MiniCartPage;
import com.bata.pages.paypalpayment.PaypalPaymentPage;
import com.bata.pages.paypalpayment.PaypalPaymentRiepilogoPage;
import com.bata.pages.riepilogo.RiepilogoPage;
import com.bata.pages.shipping.ShippingPage;
import com.bata.test.TestOrchestator;

import bean.datatable.CreditCardErrorCheckBean;
import bean.datatable.PaymentBean;
import bean.datatable.ProductBean;
import bean.datatable.SearchItemBean;
import bean.datatable.ShippingBean;
import common.CommonFields;
import common.WaitManager;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PlaceOrder 
{
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage home;
	private CartPage cart;
	private BenvenutoCheckoutPage checkOut;
	private ShippingPage shipPage;
	private PayWith payWith;
	private RiepilogoPage riepilogo;
	private MiniCartPage miniCart;
	private PaypalPaymentPage paypal;
	
	@And("^place an order to \"([^\"]*)\"$")
    public void place_an_order_to_something(String shippingMethod,List<ShippingBean> t) throws Throwable 
	{
		home=(HomePage)test.getPage(HomePage.class, test.driverConfig,test.language);
        //cart = new CartPage(driver); 
		
		WaitManager.get().waitMediumTime();
				
		if (test.typeOfTest.equals("webTest")) {
			//apro il mini cart menu
			//vado sulla cart page
			cart=home.openMiniCart().clickShowCart();
		}
		else {
			cart=home.goToCartPage();
		}
		
		//cart=home.goToCartPage();
		
		//controllo carrello
		List<SearchItemBean> elements=(List<SearchItemBean>) DinamicData.getIstance().get(CommonFields.SELECTED_ITEMS);
		cart.checkCart(elements);
		
		ShippingBean b=t.get(0);
		
		if(!StringUtils.isBlank(b.getCoupon()) || !StringUtils.isBlank(b.getVoucher()))
		{
			cart.addCoupon(b);
		}
		
		WaitManager.get().waitHighTime();
		
		String registered=b.getRegisteredUser();
		
		DinamicData.getIstance().set(CommonFields.REGISTERED_USER, registered);
		
		if(registered == null || registered.equals("n"))
		{
			//clicco su checkout
			checkOut=cart.procediAlCheckoutGuest();
			
			//clicco su procedi al pagamento
			shipPage=checkOut.checkoutOspite();
			
			switch(shippingMethod.toLowerCase())
			{
			case "home":
				shipPage.selectShippingToHome();
				this.payWith=shipPage.chooseHomeShippingGuestUser(b);
				break;
			case "store":
				shipPage.selectShippingStore(b);
				this.payWith=shipPage.chooseStoreShippingGuestUser(b);
				this.payWith.insertBillingDataGuestUser(b);
				break;
			}
		}
		else if(registered != null && registered.equals("y"))
		{
			cart.clickSuCheckOutRegistered();
			shipPage=(ShippingPage)test.getPage(ShippingPage.class, test.driverConfig, test.language);
			switch(shippingMethod.toLowerCase())
			{
			case "home":
				shipPage.selectShippingToHome();
				this.payWith=shipPage.chooseHomeShippingRegisteredUser();
				break;
			case "store":
				shipPage.selectShippingStore(b);
				this.payWith=shipPage.chooseStoreShippingRegisteredUser(b);
				this.payWith.insertBillingDataGuestUser(b);
				break;
			}
		}
		
	    
	}
	
	@When("^the user pay with following data payment$")
	public void the_user_pay_with_following_data_payment(List<PaymentBean> table) throws Throwable 
	{
		PaymentBean b=table.get(0);//come sezione
		
		if(!StringUtils.isBlank(b.getCoupon()) || !StringUtils.isBlank(b.getVoucher()))
			this.payWith.applyCoupon(b);
		
		boolean isPaypal=b.getPaymentType().toLowerCase().equals("paypal");
		
		//procedo con l'ordine
		switch(b.getPaymentType().toLowerCase())
		{
		case "card":
			if(test.country.equals("cz"))
			{
				this.riepilogo=this.payWith.payWithWebPay(b).payWithCreditCard(b);
			}
			else
				this.riepilogo=this.payWith.payWithCreditCard(b);
			break;
		case "paypal":
			PaypalPaymentPage paypal=this.payWith.payWithPayPal(b);
			PaypalPaymentRiepilogoPage riepilogo=paypal.insertCredential(b);
			this.riepilogo=riepilogo.confirmRiepilogo();
			break;
		case "cod":
			this.riepilogo=this.payWith.payWithCod(b);
			break;
		}
		
		//controllo dati di riepilogo
		this.riepilogo.checkRiepilogo(b,b.getPaymentType(),test.country,test.getLanguage(test.language));
		
	}
	
	@And("^place an order to store and change with new store before payment$")
    public void place_an_order_to_store_and_change_with_new_store_before_payment(List<ShippingBean> t) throws Throwable 
	{
		home=(HomePage)test.getPage(HomePage.class, test.driverConfig,test.language);
        
		//cart=home.goToCartPage();
		
	    //apro il mini cart menu
		miniCart=home.openMiniCart();
		//vado sulla cart page
		cart=miniCart.clickShowCart();
		
		//controllo carrello
		List<SearchItemBean> elements=(List<SearchItemBean>) DinamicData.getIstance().get(CommonFields.SELECTED_ITEMS);
		cart.checkCart(elements);
		
		ShippingBean b=t.get(0);
		
		if(!StringUtils.isBlank(b.getCoupon()) || !StringUtils.isBlank(b.getVoucher()))
		{
			cart.addCoupon(b);
		}
		
		
		//clicco su checkout
		checkOut=cart.procediAlCheckoutGuest();
		
		//clicco su procedi al pagamento
		shipPage=checkOut.checkoutOspite();
		
		shipPage.selectShippingStore(b);
		shipPage.changeStore(b);
		this.payWith=shipPage.chooseStoreShippingGuestUser(b);
		this.payWith.insertBillingDataGuestUser(b);
		
    }
	
	@And("^the user pay with following data payment and incorrect paypal account$")
    public void the_user_pay_with_following_data_payment_and_incorrect_paypal_account(List<PaymentBean> table) throws Throwable 
    {
		PaymentBean b=table.get(0);//come sezione
		
		if(!StringUtils.isBlank(b.getCoupon()) || !StringUtils.isBlank(b.getVoucher()))
			this.payWith.applyCoupon(b);
		
		paypal=this.payWith.payWithPayPal(b);
		paypal.insertWrongCredential(b);
    }
	
	@And("^the user pay with following data payment and don't accept ts$")
    public void the_user_pay_with_following_data_payment_and_dont_accept_ts(List<PaymentBean> table) throws Throwable 
	{
		PaymentBean b=table.get(0);//come sezione
		
		if(!StringUtils.isBlank(b.getCoupon()) || !StringUtils.isBlank(b.getVoucher()))
			this.payWith.applyCoupon(b);
		
		boolean isPaypal=b.getPaymentType().toLowerCase().equals("paypal");
		
		//procedo con l'ordine
		switch(b.getPaymentType().toLowerCase())
		{
		case "card":
			this.payWith.payWithCreditCardWithoutTS(b);
			break;
		case "paypal":
			this.payWith.payWithPayPalWithoutTS(b);
			break;
		}
    }
	
	@Then("^check the ts error is displayed$")
    public void check_the_ts_error_is_displayed() throws Throwable 
    {
        this.payWith.checkTsErrorMessages(test.getLanguage(test.language));
    }
	
	 @And("^the user select to pay with credit card$")
    public void the_user_select_to_pay_with_credit_card() throws Throwable 
    {
        this.payWith.selectCreditCardPaymentMethod();
    }
    
    @And("^insert the following credit card info and check the error message$")
    public void insert_the_following_credit_card_info_and_check_the_error_message(List<CreditCardErrorCheckBean> table) throws Throwable 
    {
        for(CreditCardErrorCheckBean error : table)
        {
        	this.payWith.checkCreditCardErrorMessage(error);
        }
    }
    
    @And("^insert a wrong coupon$")
    public void place_an_order_to_something_with_wrong_coupon(List<PaymentBean> t) throws Throwable 
    {
    	PaymentBean b=t.get(0);
    	
		if(!StringUtils.isBlank(b.getCoupon()))
		{
			payWith.applyCoupon(b);
		}
		else
		{
			Assert.assertTrue("errore coupon vuoto inserire un valore corretto Scenario Outline: place an order for a guest user - insert a wrong coupon",false);
		}
    }
    
    @And("^the user pay with following data payment and voucher that can't be combined with other promo$")
    public void the_user_pay_with_following_data_payment_and_voucher_that_cant_be_combined_with_other_promo() throws Throwable {
        throw new PendingException();
    }
    
    @And("^place an order to store with wrong store info$")
    public void place_an_order_to_store_with_wrong_store_info(List<ShippingBean> t) throws Throwable 
    {
    	home=(HomePage)test.getPage(HomePage.class, test.driverConfig,test.language);
        
	    //apro il mini cart menu
		miniCart=home.openMiniCart();
		//vado sulla cart page
		cart=miniCart.clickShowCart();
		
		//controllo carrello
		List<SearchItemBean> elements=(List<SearchItemBean>) DinamicData.getIstance().get(CommonFields.SELECTED_ITEMS);
		cart.checkCart(elements);
		
		ShippingBean b=t.get(0);
		
		if(!StringUtils.isBlank(b.getCoupon()) || !StringUtils.isBlank(b.getVoucher()))
		{
			cart.addCoupon(b);
		}
		
		
		//clicco su checkout
		checkOut=cart.procediAlCheckoutGuest();
		
		//clicco su procedi al pagamento
		shipPage=checkOut.checkoutOspite();
		//shipPage.setLanguage(language);
		
		shipPage.searchStore(b);
    }
    
    @Then("^check the disappearing of COD payment$")
    public void check_the_disappearing_of_cod_payment() throws Throwable 
    {
        this.payWith.checkCODIsntDisplayed();
    }
}
