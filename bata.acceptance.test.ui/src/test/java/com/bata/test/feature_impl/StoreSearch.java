package com.bata.test.feature_impl;

import java.util.List;

import com.bata.pages.homepage.HomePage;
import com.bata.pages.shop.ShopPage;
import com.bata.test.TestOrchestator;

import bean.datatable.StoreSearchBean;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreSearch 
{
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage homepage;
	private ShopPage shoppage;

	@When("^The user insert Negozi in the search bar$")
	public void the_user_insert_Negozi_in_the_search_bar(List<StoreSearchBean> storeSearchBean) throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        homepage.searchNegozi(storeSearchBean.get(0));
	}
	
	@Then("^The user will be redirect on store locator$")
	public void the_user_will_be_redirect_on_store_locator() throws Throwable {
		shoppage=(ShopPage)test.getPage(ShopPage.class, test.driverConfig, test.language);
	}
	
	@When("^The user insert field in the search bar with Map Mode$")
	public void the_user_insert_field_in_the_search_bar_with_Map_Mode(List<StoreSearchBean> storeSearchBean) throws Throwable 
	{
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        shoppage=homepage.goToShopPage();
		shoppage.searchStoreByMap(storeSearchBean.get(0));
		shoppage.openAndCheckDetailsStoreDirettoByMap();
	}

	@Then("^Website show closest stores based on input received$")
	public void website_show_closest_stores_based_on_input_received() throws Throwable {
	}
	
	@When("^The user insert field in the search bar with List Mode$")
	public void the_user_insert_field_in_the_search_bar_with_List_Mode(List<StoreSearchBean> storeSearchBean) throws Throwable {
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        shoppage=homepage.goToShopPage();
		shoppage.searchStoreByList(storeSearchBean.get(0));
		shoppage.openAndCheckDetailsStoreByList();
	}

	@When("^The user insert field in the search bar with Original filter$")
	public void the_user_insert_field_in_the_search_bar_with_Original_filter(List<StoreSearchBean> storeSearchBean) throws Throwable {
		homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        shoppage=homepage.goToShopPage();
		shoppage.searchStoreByMap(storeSearchBean.get(0));
		shoppage.clickFranchisingFilter();
	}
	
    @When("^The user insert field in the search bar with Franchising filter$")
    public void the_user_insert_field_in_the_search_bar_with_franchising_filter(List<StoreSearchBean> storeSearchBean) throws Throwable {
    	homepage=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
        shoppage=homepage.goToShopPage();
		shoppage.searchStoreByMap(storeSearchBean.get(0));
    	shoppage.clickDirettoFilter();
    }
    
    @Then("^Website show just store with the refinement selected original$")
    public void website_show_just_store_with_the_refinement_selected_original() throws Throwable {
    }

    @Then("^Website show just store with the refinement selected franchising$")
    public void website_show_just_store_with_the_refinement_selected_franchising() throws Throwable {
    }
}
