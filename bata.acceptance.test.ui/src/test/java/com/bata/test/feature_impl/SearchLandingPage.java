package com.bata.test.feature_impl;

import java.util.List;

import com.bata.pages.seach.SearchingPageFiltered;
import com.bata.test.TestOrchestator;

import bean.datatable.SearchFilterBean;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SearchLandingPage 
{
	private static TestOrchestator test=TestOrchestator.get();
	private SearchingPageFiltered searchingPageFiltered;
	
	@Then("^check that the user land on Searching page filtered$")
	public void check_that_the_user_land_on_Searching_page_filtered() throws Throwable 
	{
		searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	searchingPageFiltered.get();
	}
	
	@And("^filter results by$")
    public void filter_results_by(List<SearchFilterBean> table) throws Throwable 
	{
		searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	
		for(SearchFilterBean filter : table)
        {
        	searchingPageFiltered.applyFilter(filter,test.country);
        }
    }

	@And("^check the resulting page by applied filter$")
    public void check_the_resulting_page_by_applied_filter(List<SearchFilterBean> table) throws Throwable 
	{
		searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	
		for(SearchFilterBean filter : table)
        {
			//controllo che il filtro sia stato applicato
        	searchingPageFiltered.checkAppliedFilter(filter);
        	
        	//controllo che i risultati mostrati siano conformi al filtro applicato
        	searchingPageFiltered.checkFilteredResults(filter);
        }
		
		
    }
	
	@And("^remove selected filter$")
    public void remove_selected_filter(List<SearchFilterBean> table) throws Throwable 
	{
		searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	
		for(SearchFilterBean filter : table)
        {
        	searchingPageFiltered.removeFilter(filter);
        }
    }
	
	@And("^check the resulting page by removed filter$")
    public void check_the_resulting_page_by_removed_filter(List<SearchFilterBean> table) throws Throwable 
	{
		searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	searchingPageFiltered.checkListAfterRemovingFilter();
    }
	
	@And("^click on order filter$")
    public void click_on_order_filter(List<SearchFilterBean> table) throws Throwable 
	{
		searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	searchingPageFiltered.applyOrderFilter(table.get(0));
    }
	
	@And("^check the resulting page after ordering list$")
    public void check_the_resulting_page_after_ordering_list(List<SearchFilterBean> table) throws Throwable 
	{
		searchingPageFiltered=(SearchingPageFiltered)test.getPage(SearchingPageFiltered.class, test.driverConfig, test.language);
    	searchingPageFiltered.checkAfterApplingFilter(table.get(0));
    }
}
