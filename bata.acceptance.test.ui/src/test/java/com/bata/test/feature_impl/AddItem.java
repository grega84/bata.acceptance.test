package com.bata.test.feature_impl;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.bata.pagepattern.DinamicData;
import com.bata.pages.cartpage.CartPage;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.minicart.MiniCartPage;
import com.bata.pages.product.ProductPage;
import com.bata.test.TestOrchestator;

import bean.datatable.ProductBean;
import bean.datatable.SearchItemBean;
import common.CommonFields;
import common.WaitManager;
import cucumber.api.java.en.*;
import io.appium.java_client.ios.IOSDriver;

public class AddItem 
{
	private static final String PDP = "PDP";
	private static final String PLP = "PLP - Quick Buy";
	private static TestOrchestator test=TestOrchestator.get();
	private HomePage home;
	private ProductPage product;
	private CartPage cart;
	private MiniCartPage miniCart;

	@And("^add an item to cart from \"([^\"]*)\"$")
    public void add_an_item_to_cart_from_something(String section,List<ProductBean> table) throws Throwable 
    {
		home=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
    	
		for(ProductBean prod : table)
    	{
    		//ricerco il prodotto
			home.searchProduct(prod);
			
			switch(section)
			{
			case PDP:
				//seleziono il prodotto
				product=home.chooseItem()
				.checkItemPage()
				//lo aggiungo all carrello
				.selectDetailOfItemAndAddToCart();
				break;
			case PLP:
				//seleziono mostra tutti
				//if (!typeOfTest.equals("webTest"))
					//((AppiumDriver<?>)driver).hideKeyboard();
				Thread.sleep(1000);
				
				home.showAll()
				//metto il focus sul primo prodotto
				.focusOnItem()
				.addItemToCart();
				//product=ProductPage.from(driver, language);
				test.fromPLP=true;
				break;
			}
    	}
    }

	@Then("^Check that item will be correctly add to cart$")
    public void check_that_item_will_be_correctly_add_to_cart() throws Throwable 
    {
		WebDriver driver=test.getDriver(test.driverConfig);
		
    	if (driver instanceof IOSDriver)  {
        	//controllo presenza nel carrello
    		//product=ProductPage.from(driver, language);
    		product.checkMiniCart()
    		//chiudo il carrello
    		.closeMiniCart();
    	}
    	else 
    	{
    		if(!test.fromPLP)
    		{
    			home=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
    			WaitManager.get().waitLongTime();
        		home.openMiniCart().checkPresenceOfAddedElement().clickShowCart();
    		}
    		else
    		{
    			miniCart=(MiniCartPage)test.getPage(MiniCartPage.class, test.driverConfig, test.language);
        		miniCart.checkPresenceOfAddedElement().clickShowCart();
    		}
    		
    		this.cart=(CartPage)test.getPage(CartPage.class, test.driverConfig, test.language);
    		SearchItemBean bean=(SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM);
    		ArrayList<SearchItemBean> list = new ArrayList<SearchItemBean>();
    		list.add(bean);
    		cart.checkCart(list);
    	}
		
    }
	
	@And("^if the cart isnt empty$")
    public void if_the_cart_isnt_empty() throws Throwable 
	{
		home=(HomePage)test.getPage(HomePage.class, test.driverConfig, test.language);
		
		WaitManager.get().waitMediumTime();
		//clicco sulla bag icon
	    cart=home.clickBagIcon();
	    //controllo che il cart sia vuoto
	    test.isEmpty=cart.isEmpty();
	    
	    
    }
	
	@Then("^clear the cart$")
    public void clear_the_cart() throws Throwable 
	{
        //se ci sono elementi procedo con la pulizia e ritorno alla home page.
		if(!test.isEmpty)
		{
//			if(test.typeOfTest.contains("web"))
//			{
//				miniCart=(MiniCartPage)test.getPage(MiniCartPage.class, test.driverConfig, test.language);
//				cart=miniCart.clickShowCart();
//			}
//			else
//			{
//				cart=(CartPage)test.getPage(CartPage.class, test.driverConfig, test.language);
//			}
			this.home=cart.clearCart();
		}
    }
	
	@And("^User choose an item with \"([^\"]*)\" and add it to cart$")
    public void user_choose_an_item_with_something_and_add_it_to_cart(String strArg1,List<ProductBean> table) throws Throwable 
	{
		List<SearchItemBean> beans=new ArrayList<SearchItemBean>();
		
		home=(HomePage)test.getPage(HomePage.class, test.driverConfig,test.language);
		
		//controllo se è multiitem
		ProductBean b=table.get(0);
		
		if(b.getMultiItems() != null)
		{
			String[] multi=b.getMultiItems().split(",");
			
			for(String s : multi)
			{
				ProductBean prod=new ProductBean();
				prod.setProductName(s);
				
				home.searchProduct(prod);
				//home.countItemsPreviewSearched();
				
				//seleziono il prodotto
				product=home.chooseItem();
				product.checkItemPage();
				
				//lo aggiungo all carrello
				product.selectDetailOfItemAndAddToCart();
				
				//if (typeOfTest.equals("webTest")) {
					//controllo presenza nel carrello
//					product.checkMiniCart();
//					//chiudo il carrello
//					product.closeMiniCart();
				//}
				WaitManager.get().waitLongTime();
				//torno alla homepage
				home=product.gotoHomePage();
				
				beans.add((SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM));
			}
		}
		else
		{
			for(ProductBean prod : table)
		    {
		    	//ricerco il prodotto
				home.searchProduct(prod);
				//home.countItemsPreviewSearched();
				
				//seleziono il prodotto
				product=home.chooseItem();
				product.checkItemPage();
				
				//lo aggiungo all carrello
				product.selectDetailOfItemAndAddToCart();
				
				//if (typeOfTest.equals("webTest")) {
					//controllo presenza nel carrello
//					product.checkMiniCart();
//					//chiudo il carrello
//					product.closeMiniCart();
				//}
				WaitManager.get().waitLongTime();
				//torno alla homepage
				home=product.gotoHomePage();
				
				beans.add((SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM));
		    }
		}
        
		
	    
	    
	    DinamicData.getIstance().set("SELECTED_ITEMS", beans);
	}
}
