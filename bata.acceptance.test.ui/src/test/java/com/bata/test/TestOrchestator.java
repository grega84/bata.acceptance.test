package com.bata.test;


import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bata.pagepattern.Utility;

import bean.datatable.RiepilogoBean;
import test.automation.core.UIUtils;

public class TestOrchestator 
{
	private static final Logger logger = LoggerFactory.getLogger(TestOrchestator.class);

	public static final String TC_NOT_ACCEPTED_ERROR_CHECK = "tc-not-accepted";
	public static final String INVALID_PHONE_ERROR_CHECK = "invalid-phone-number";

	public static final String ORDER_NUMBER_HEADER = "NUMERO ORDINE: ";
	public static final String ORDER_NUMBER_HEADER_CZ = "ČÍSLO OBJEDNÁVKY: ";
	public static final String ORDER_NUMBER_HEADER_PL = "NUMER ZAMÓWIENIA: ";
	public static final String ORDER_NUMBER_HEADER_SK = "ČÍSLO OBJEDNÁVKY : ";

	public static final String SEND_REQUEST = "send_request-";
	
	private static TestOrchestator singleton;
	private HashMap<Class,LoadableComponent> pages;
	private HashMap<String,WebDriver> drivers;
	private HashMap<String,Properties> languages;

	public RiepilogoBean riepilogoBean;
	public String baseImageToFindURL;
	public String baseVideoNamePath;

	public String typeOfTest;

	public String testIdJIRA;

	public String baseVideoName;

	public boolean videoRunning;

	public String driverConfig;

	public String driverMailConfig;

	public String webVideo;

	public String country;

	public String language;

	public String baseVideo;

	public boolean isEmpty;

	public boolean fromPLP;
	

	private TestOrchestator() 
	{
		pages=new HashMap<Class,LoadableComponent>();
		drivers=new HashMap<String,WebDriver>();
		languages=new HashMap<String,Properties>();
		baseImageToFindURL="";
		baseVideoNamePath="";
		typeOfTest="";
		testIdJIRA="";
		baseVideoName="";
		videoRunning=false;
		driverConfig="";
		driverMailConfig="";
		webVideo="";
		country="";
		language="";
		baseVideo="";
		riepilogoBean=null;
		isEmpty=false;
		fromPLP=false;
	}

	public static TestOrchestator get()
	{
		if(singleton == null)
			singleton = new TestOrchestator();
		
		return singleton;
	}
	
	public WebDriver getDriver(String config)
	{
		if(!drivers.containsKey(config))
			drivers.put(config, UIUtils.ui().driver(config));
		
		return drivers.get(config);
	}
	
	public Properties getLanguage(String country)
	{
		if(!languages.containsKey(country))
			languages.put(country, Utility.getLanguageLocators(country));
		
		return languages.get(country);
	}
	
	public LoadableComponent getPage(Class clazz,String driverConfig,String country) throws Exception
	{
		if(drivers.get(driverConfig) == null && languages.get(country) == null)
		{
			logger.error("[ERROR] driver and language they cannot be null, please initialize them using the getDriver and getLanguage methods");
			throw new RuntimeException("[ERROR] driver and language they cannot be null, please initialize them using the getDriver and getLanguage methods");
		}
		
		if(!pages.containsKey(clazz))
		{
			try {
				Constructor[] C=clazz.getDeclaredConstructors();
				System.out.println(Arrays.toString(C));
				Constructor<?> cons = clazz.getDeclaredConstructor(WebDriver.class,Properties.class);
				cons.setAccessible(true);
				
				LoadableComponent obj=(LoadableComponent) cons.newInstance(drivers.get(driverConfig),languages.get(country));
				
				//obj.get();
				
				pages.put(clazz, obj);
			} catch (Exception e) 
			{
				e.printStackTrace();
				throw e;
			}
		}
		
		return pages.get(clazz);
	}

	public void clear() 
	{
		pages.clear();
		drivers.clear();
		languages.clear();
		baseImageToFindURL="";
		baseVideoNamePath="";
		typeOfTest="";
		testIdJIRA="";
		baseVideoName="";
		videoRunning=false;
		driverConfig="";
		driverMailConfig="";
		webVideo="";
		country="";
		language="";
		baseVideo="";
		riepilogoBean=null;
		isEmpty=false;
		fromPLP=false;
	}
}
