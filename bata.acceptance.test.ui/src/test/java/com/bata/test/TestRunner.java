package com.bata.test;


import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Regression.json" },features = {
		"src/test/resources/com/bata/additem/AddAnItemToCart.feature", 
		"src/test/resources/com/bata/customerLogin/CustomerLogin.feature",
		"src/test/resources/com/bata/customerRegistration/CustomerRegistration.feature",
		"src/test/resources/com/bata/customerUpdateInfo/CustomerUpdateInfo.feature",
		"src/test/resources/com/bata/instoreavalaibility/InStoreAvalaibility.feature",
		"src/test/resources/com/bata/placeanorder/guest/multiitem/PlaceAnOrderGuestUserMultiItem.feature",
		"src/test/resources/com/bata/placeanorder/guest/singleitem/PlaceAnOrderGuestUserSingleItem.feature",
		"src/test/resources/com/bata/placeanorder/registered/multiitem/PlaceAnOrderRegistredUserMultiItem.feature",
		"src/test/resources/com/bata/placeanorder/registered/singleitem/PlaceAnOrderRegistredUserSingleItem.feature",
		"src/test/resources/com/bata/placeanorder-error/guest/multiitem/PlaceAnOrderErrorGuestUserMultiItem.feature",
		"src/test/resources/com/bata/placeanorder-error/guest/singleitem/PlaceAnOrderErrorGuestSingleItem.feature",
		"src/test/resources/com/bata/placeanorder-error/registered/multiitem/PlaceAnOrderErrorRegisteredUserMultiItem.feature",
		"src/test/resources/com/bata/placeanorder-error/registered/singleitem/PlaceAnOrderErrorRegisteredUserSingleItem.feature",
		"src/test/resources/com/bata/searchproduct/SearchProduct.feature",
		"src/test/resources/com/bata/sendcontactrequest/SendContactRequest.feature",
		"src/test/resources/com/bata/storesearch/StoreSearch.feature",
		"src/test/resources/com/bata/trackanorder/guest/TrackAnOrderGuest.feature",
		"src/test/resources/com/bata/trackanorder/registered/TrackAnOrderRegistered.feature",
		"src/test/resources/com/bata/wishlist/Wishlist.feature",
		},

tags = "@UAT_PS20_ita" +
		"", 
glue= {"classpath:com/bata/test/feature_impl"})



@Ui
public class TestRunner {

}
