######################################
#
# scenari di regressione che comprendono le funzionalità core di bata
#
#######################################
Feature: Regression Core su Bata

  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   |
      | <productName> | <quantity> | <size> | <color> |
    Then Check that item will be correctly add to cart
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | cz      | nike        |          |      |       |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | cz      | nike        |          |      |       |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       |

  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   |
      | <productName> | <quantity> | <size> | <color> |
    Then Check that item will be correctly add to cart
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | cz      | nike        |          |      |       | automation.test.prisma+cz@gmail.com | Alice.it0$ |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | cz      | nike        |          |      |       | automation.test.prisma+cz@gmail.com | Alice.it0$ |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       | automation.test.prisma+pl@gmail.com | Alice.it0$ |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       | automation.test.prisma+pl@gmail.com | Alice.it0$ |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       | automation.test.prisma+sk@gmail.com | Alice.it0$ |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       | automation.test.prisma+sk@gmail.com | Alice.it0$ |

  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following wrong credential
      | email   | password   |
      | <email> | <password> |
    Then The user is notified with an error message
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | ita     |
      | webTest    | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | ita     |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | ita     |
      | androidTest | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | ita     |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | cz      |
      | webTest    | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | cz      |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | cz      |
      | androidTest | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | cz      |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | pl      |
      | webTest    | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | pl      |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | pl      |
      | androidTest | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | pl      |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | sk      |
      | webTest    | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | sk      |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                       | password    | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | sk      |
      | androidTest | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | sk      |

  Scenario Outline: Customer login on MYACCOUNT page-Login again just after logout
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following credential
      | email   | password   |
      | <email> | <password> |
    And The user logout to Bata site
    And The user logs in again to Bata site
      | email   | password   |
      | <email> | <password> |
    Then The user will be redirect on the profile page
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                                    | password   | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                                    | password   | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                               | password   | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.5 | automation.test.prisma+cz@gmail.com | Alice.it0$ | cz      |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                               | password   | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.5 | automation.test.prisma+cz@gmail.com | Alice.it0$ | cz      |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                               | password   | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.5 | automation.test.prisma+pl@gmail.com | Alice.it0$ | pl      |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                               | password   | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.5 | automation.test.prisma+pl@gmail.com | Alice.it0$ | pl      |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | email                               | password   | country |
      | webTest    | storefront   | BT2018       | UAT_LG-2.5 | automation.test.prisma+sk@gmail.com | Alice.it0$ | sk      |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | email                               | password   | country |
      | androidTest | storefront   | BT2018       | UAT_LG-2.5 | automation.test.prisma+sk@gmail.com | Alice.it0$ | sk      |

  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig | driverMailConfig |
      | <testIdJIRA> | <typeOfTest> | default      | gmail            |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   |
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> |
    Then The user will be redirect on the profile page
    And The user check the received email
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | ita     | Alice.it0$      | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 2458977568 |      01 | Gennaio   |     1950 |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | ita     | Alice.it0$      | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 2458977568 |      01 | Gennaio   |     1950 |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | cz      | Alice.it0$      | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 2458977568 |      01 | Gennaio   |     1950 |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | cz      | Alice.it0$      | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 2458977568 |      01 | Gennaio   |     1950 |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Poland | Napoli   | 2458977568 |      01 | styczeń   |     1950 |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Poland | Napoli   | 2458977568 |      01 | styczeń   |     1950 |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 2458977568 |      01 | Gennaio   |     1950 |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 2458977568 |      01 | Gennaio   |     1950 |

  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   |
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> |
    Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   |
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> |

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | field                | email                              | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | test0001prismabata@sharklasers.com | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | field                | email                              | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | test0001prismabata@sharklasers.com | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | field                | email                               | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | automation.test.prisma+cz@gmail.com | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | field                | email                               | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | automation.test.prisma+cz@gmail.com | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | field                | email                               | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | automation.test.prisma+pl@gmail.com | Prisma2018! | pl      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | field                | email                               | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | automation.test.prisma+pl@gmail.com | Prisma2018! | pl      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | field                | email                               | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | webTest    | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | automation.test.prisma+sk@gmail.com | Prisma2018! | sk      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | field                | email                               | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn |
      | androidTest | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | automation.test.prisma+sk@gmail.com | Prisma2018! | sk      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 |

  Scenario Outline: Customer update info-Edit Personal Data
    Given the JIRA test
      | testIdJIRA | typeOfTest   | driverConfig |
      | UAT_CU_3.3 | <typeOfTest> | default      |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And access to myprofile
    And edit personal data with following data
    Then check if the personal data is updated
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | country | email                              | password   | authUsername | authPassword |
      | webTest    | ita     | prisma_update_info@sharklasers.com | Alice.it0$ | storefront   | BT2018       |

    @ita_mobile
    Examples: 
      | typeOfTest  | country | email                              | password   | authUsername | authPassword |
      | androidTest | ita     | prisma_update_info@sharklasers.com | Alice.it0$ | storefront   | BT2018       |

    @cz
    Examples: 
      | typeOfTest | country | email                                      | password   | authUsername | authPassword |
      | webTest    | cz      | automation.test.prisma+cz.update@gmail.com | Alice.it0$ | storefront   | BT2018       |

    @cz_mobile
    Examples: 
      | typeOfTest  | country | email                                      | password   | authUsername | authPassword |
      | androidTest | cz      | automation.test.prisma+cz.update@gmail.com | Alice.it0$ | storefront   | BT2018       |

    @pl
    Examples: 
      | typeOfTest | country | email                                | password   | authUsername | authPassword |
      | webTest    | pl      | automation.test.prisma+pl3@gmail.com | Alice.it0$ | storefront   | BT2018       |

    @pl_mobile
    Examples: 
      | typeOfTest  | country | email                                | password   | authUsername | authPassword |
      | androidTest | pl      | automation.test.prisma+pl3@gmail.com | Alice.it0$ | storefront   | BT2018       |

    @sk
    Examples: 
      | typeOfTest | country | email                                      | password   | authUsername | authPassword |
      | webTest    | sk      | automation.test.prisma+sk.update@gmail.com | Alice.it0$ | storefront   | BT2018       |

    @sk_mobile
    Examples: 
      | typeOfTest  | country | email                                      | password   | authUsername | authPassword |
      | androidTest | sk      | automation.test.prisma+sk.update@gmail.com | Alice.it0$ | storefront   | BT2018       |

  Scenario Outline: Customer update info-Change PWD
    Given the JIRA test
      | testIdJIRA | typeOfTest   | driverConfig |
      | UAT_CU_3.2 | <typeOfTest> | default      |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And access to myprofile
    And change the password with this
      | password   | newPassword   |
      | <password> | <newPassword> |
    Then check if the password is changed
      | email   | password   |
      | <email> | <password> |
    And set the password to the old value
      | email   | password   | newPassword   |
      | <email> | <password> | <newPassword> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | country | email                                  | password   | authUsername | authPassword | newPassword      |
      | webTest    | ita     | prisma_change_password@sharklasers.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

    @ita_mobile
    Examples: 
      | typeOfTest  | country | email                                  | password   | authUsername | authPassword | newPassword      |
      | androidTest | ita     | prisma_change_password@sharklasers.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

    @cz
    Examples: 
      | typeOfTest | country | email                                    | password   | authUsername | authPassword | newPassword      |
      | webTest    | cz      | automation.test.prisma+cz.pswd@gmail.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

    @cz_mobile
    Examples: 
      | typeOfTest  | country | email                                    | password   | authUsername | authPassword | newPassword      |
      | androidTest | cz      | automation.test.prisma+cz.pswd@gmail.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

    @sk
    Examples: 
      | typeOfTest | country | email                                    | password   | authUsername | authPassword | newPassword      |
      | webTest    | sk      | automation.test.prisma+sk.pswd@gmail.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

    @sk_mobile
    Examples: 
      | typeOfTest  | country | email                                    | password   | authUsername | authPassword | newPassword      |
      | androidTest | sk      | automation.test.prisma+sk.pswd@gmail.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

    @pl
    Examples: 
      | typeOfTest | country | email                                      | password   | authUsername | authPassword | newPassword      |
      | webTest    | pl      | automation.test.prisma+pl.update@gmail.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

    @pl_mobile
    Examples: 
      | typeOfTest  | country | email                                      | password   | authUsername | authPassword | newPassword      |
      | androidTest | pl      | automation.test.prisma+pl.update@gmail.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$!nuovo |

  Scenario Outline: in store avalaibility for guest user
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | webTest    | storefront   | BT2018       | UAT_ISA-14 | ita     | nike        |      | geoip    |     |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | androidTest | storefront   | BT2018       | UAT_ISA-14 | ita     | nike        |      | geoip    |     |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | webTest    | storefront   | BT2018       | UAT_ISA-14 | cz      | nike        |      | geoip    |     |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | androidTest | storefront   | BT2018       | UAT_ISA-14 | cz      | nike        |      | geoip    |     |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | webTest    | storefront   | BT2018       | UAT_ISA-14 | pl      | nike        |      | geoip    |     |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | androidTest | storefront   | BT2018       | UAT_ISA-14 | pl      | nike        |      | geoip    |     |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | webTest    | storefront   | BT2018       | UAT_ISA-14 | sk      | nike        |      | geoip    |     |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA | country | productName | size | criteria | key |
      | androidTest | storefront   | BT2018       | UAT_ISA-14 | sk      | nike        |      | geoip    |     |

  Scenario Outline: Place an order for guest user
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig | driverMailConfig |
      | <testIdJIRA> | <typeOfTest> | default      | gmail            |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color |
      | nike        |          |      |       |
    And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   |
      | home        | <emailAddress> | <emailSubject> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.1   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.3   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.1   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.3   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.3   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.3   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.3   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.3   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} |

  Scenario Outline: Place an order for guest user delivery on store
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig | driverMailConfig |
      | <testIdJIRA> | <typeOfTest> | default      | gmail            |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color |
      | sneakers    |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   |
      | store       | <emailAddress> | <emailSubject> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             | dataStore |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.25 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             | dataStore |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.25 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   | dataStore |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   | dataStore |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     | dataStore |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     | dataStore |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  | dataStore |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  | dataStore |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   |

  Scenario Outline: Place an order for registered user
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig | driverMailConfig |
      | <testIdJIRA> | <typeOfTest> | default      | gmail            |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color |
      | nike        |          |      |       |
    And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   |
      | home        | <emailAddress> | <emailSubject> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             | email                                            | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.1   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.3   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             | email                                            | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.1   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.3   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | cod         | Přijali jsme Vaši objednávku č. ${orderNumber} | automation.test.prisma+cz@gmail.com | Alice.it0$ |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.3   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.3   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | cod         | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | automation.test.prisma+pl@gmail.com | Alice.it0$ |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.3   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.3   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | cod         | Prijali sme Vašu objednávku č. ${orderNumber} | automation.test.prisma+sk@gmail.com | Alice.it0$ |

  Scenario Outline: Place an order for registered user delivery on store
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig | driverMailConfig |
      | <testIdJIRA> | <typeOfTest> | default      | gmail            |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color |
      | nike        |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   |
      | store       | <emailAddress> | <emailSubject> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                            | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.25 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                            | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.25 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   | dataStore | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber       | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city  | province | country | paymentType | emailSubject                                   | dataStore | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | paypal      | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      | prisma10 |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+cz@gmail.com | Alice.it0$ |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     | dataStore | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   | automation.test.prisma+pl@gmail.com | Alice.it0$ |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city   | province | country | paymentType | emailSubject                                     | dataStore | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   | automation.test.prisma+pl@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. | !geoip!   | automation.test.prisma+pl@gmail.com | Alice.it0$ |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  | dataStore | email                               | password   |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | webTest    | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+sk@gmail.com | Alice.it0$ |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                     | phone      | name   | surname | address                         | cap    | city       | province | country | paymentType | emailSubject                                  | dataStore | email                               | password   |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+sk@gmail.com | Alice.it0$ |
      | androidTest | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} | !geoip!   | automation.test.prisma+sk@gmail.com | Alice.it0$ |

  Scenario Outline: place an oder insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color |
      | nike        |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user select to pay with credit card
    And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   |
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | all_field_empty        |                          |     |               | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    |                        4 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    | 4111 1111 1111           | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 4458 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_with_alpha      | 4111 1111 1111 eee       | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      |   1 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | all_field_empty        |                          |     |               | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    |                        4 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    | 4111 1111 1111           | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 4458 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_with_alpha      | 4111 1111 1111 eee       | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      |   1 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Poland | Napoli   | Italia  | card        |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city       | province | country | paymentType |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | all_field_empty        |                          |     |               | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_less_than_16    | 4111 1111 1111           | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_greater_than_16 | 4111 1111 1111 1111 4458 | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_with_alpha      | 4111 1111 1111 eee       | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      |   1 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | webTest    | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city       | province | country | paymentType |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | all_field_empty        |                          |     |               | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_less_than_16    | 4111 1111 1111           | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_greater_than_16 | 4111 1111 1111 1111 4458 | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | number_with_alpha      | 4111 1111 1111 eee       | 737 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      |   1 | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |
      | androidTest | storefront   | BT2018       | UAT_OE-12.27 | sk      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | Október          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 123 45 | Slovacchia | Napoli   | Italia  | card        |

  Scenario Outline: place an order for a guest user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color |
      | nike        |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the coupon error message is displayed
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | webTest    | storefront   | BT2018       | UAT_OE-12.29 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | androidTest | storefront   | BT2018       | UAT_OE-12.29 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | webTest    | storefront   | BT2018       | UAT_OE-12.29 | cz      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | androidTest | storefront   | BT2018       | UAT_OE-12.29 | cz      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | webTest    | storefront   | BT2018       | UAT_OE-12.29 | pl      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | androidTest | storefront   | BT2018       | UAT_OE-12.29 | pl      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | webTest    | storefront   | BT2018       | UAT_OE-12.29 | sk      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | androidTest | storefront   | BT2018       | UAT_OE-12.29 | sk      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

  Scenario Outline: Make a search by name + color + gender + material
    Given the JIRA test
      | testIdJIRA | typeOfTest   | driverConfig |
      | UAT_PS-6.5 | <typeOfTest> | default      |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name + color + gender with search bar
      | productName   | brand   | color   | gender   | material   |
      | <productName> | <brand> | <color> | <gender> | <material> |
    And check that the results showed are six and click on show more
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName       | brand | color | gender | material |
      | webTest    | ita     | storefront   | BT2018       | sneakers in pelle | nike  | nere  | uomo   | camoscio |

    @ita_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName       | brand | color | gender | material |
      | androidTest | ita     | storefront   | BT2018       | sneakers in pelle | nike  | nere  | uomo   | camoscio |

    @cz
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName    | brand | color | gender | material |
      | webTest    | cz      | storefront   | BT2018       | kožne tenisice | nike  | crna  | čovjek | antilop  |

    @cz_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName    | brand | color | gender | material |
      | androidTest | cz      | storefront   | BT2018       | kožne tenisice | nike  | crna  | čovjek | antilop  |

    @pl
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName      | brand | color | gender | material |
      | webTest    | pl      | storefront   | BT2018       | skórzane trampki |       |       |        |          |

    @pl_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName      | brand | color | gender | material |
      | androidTest | pl      | storefront   | BT2018       | skórzane trampki |       |       |        |          |

    @sk
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName    | brand | color  | gender | material |
      | webTest    | sk      | storefront   | BT2018       | kožené tenisky | nike  | čierna |        |          |

    @sk_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName    | brand | color  | gender | material |
      | androidTest | sk      | storefront   | BT2018       | kožené tenisky | nike  | čierna |        |          |

  Scenario Outline: Product search-No results scenario
    Given the JIRA test
      | testIdJIRA | typeOfTest   | driverConfig |
      | UAT_PS-6.9 | <typeOfTest> | default      |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    Then the Sorry, we did not find any products message and other popular searches are displayed
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName |
      | webTest    | ita     | storefront   | BT2018       | camicia     |

    @ita_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName |
      | androidTest | ita     | storefront   | BT2018       | camicia     |

    @cz
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName |
      | webTest    | cz      | storefront   | BT2018       | camicia     |

    @cz_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName |
      | androidTest | cz      | storefront   | BT2018       | camicia     |

    @pl
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName |
      | webTest    | pl      | storefront   | BT2018       | camicia     |

    @pl_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName |
      | androidTest | pl      | storefront   | BT2018       | camicia     |

    @sk
    Examples: 
      | typeOfTest | country | authUsername | authPassword | productName |
      | webTest    | sk      | storefront   | BT2018       | camicia     |

    @sk_mobile
    Examples: 
      | typeOfTest  | country | authUsername | authPassword | productName |
      | androidTest | sk      | storefront   | BT2018       | camicia     |

  Scenario Outline: Product search-Refine search by filters
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    And click on show more link
    And check that the user land on Searching page filtered
    And filter results by
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And check the resulting page by applied filter
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And remove selected filter
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And check the resulting page by removed filter
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | webTest    | UAT_PS-6.12 | size      |             | cerimonia   | ita     | storefront   | BT2018       |
      | webTest    | UAT_PS-6.15 | color     |             | cerimonia   | ita     | storefront   | BT2018       |

    @ita_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | androidTest | UAT_PS-6.12 | size      |             | cerimonia   | ita     | storefront   | BT2018       |
      | androidTest | UAT_PS-6.15 | color     |             | cerimonia   | ita     | storefront   | BT2018       |

    @cz
    Examples: 
      | typeOfTest | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | webTest    | UAT_PS-6.12 | size      |             | SNEAKERS    | cz      | storefront   | BT2018       |
      | webTest    | UAT_PS-6.15 | color     |             | SNEAKERS    | cz      | storefront   | BT2018       |

    @cz_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | androidTest | UAT_PS-6.12 | size      |             | SNEAKERS    | cz      | storefront   | BT2018       |
      | androidTest | UAT_PS-6.15 | color     |             | SNEAKERS    | cz      | storefront   | BT2018       |

    @pl
    Examples: 
      | typeOfTest | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | webTest    | UAT_PS-6.12 | size      |             | nike        | pl      | storefront   | BT2018       |
      | webTest    | UAT_PS-6.15 | color     |             | nike        | pl      | storefront   | BT2018       |

    @pl_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | androidTest | UAT_PS-6.12 | size      |             | nike        | pl      | storefront   | BT2018       |
      | androidTest | UAT_PS-6.15 | color     |             | nike        | pl      | storefront   | BT2018       |

    @sk
    Examples: 
      | typeOfTest | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | webTest    | UAT_PS-6.12 | size      |             | nike        | sk      | storefront   | BT2018       |
      | webTest    | UAT_PS-6.15 | color     |             | nike        | sk      | storefront   | BT2018       |

    @sk_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | filterKey | filterValue | productName | country | authUsername | authPassword |
      | androidTest | UAT_PS-6.12 | size      |             | nike        | sk      | storefront   | BT2018       |
      | androidTest | UAT_PS-6.15 | color     |             | nike        | sk      | storefront   | BT2018       |

  Scenario Outline: Product search-Sort result
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    And click on show more link
    And check that the user land on Searching page filtered
    And click on order filter
      | orderFilter   |
      | <orderFilter> |
    And check the resulting page after ordering list
      | orderFilter   |
      | <orderFilter> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | webTest    | UAT_PS-6.17 | price-low-to-high | ita     | storefront   | BT2018       | cerimonia   |
      | webTest    | UAT_PS-6.17 | price-high-to-low | ita     | storefront   | BT2018       | cerimonia   |

    @ita_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | androidTest | UAT_PS-6.17 | price-low-to-high | ita     | storefront   | BT2018       | cerimonia   |
      | androidTest | UAT_PS-6.17 | price-high-to-low | ita     | storefront   | BT2018       | cerimonia   |

    @cz
    Examples: 
      | typeOfTest | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | webTest    | UAT_PS-6.17 | price-low-to-high | cz      | storefront   | BT2018       | sneakers    |
      | webTest    | UAT_PS-6.17 | price-high-to-low | cz      | storefront   | BT2018       | sneakers    |

    @cz_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | androidTest | UAT_PS-6.17 | price-low-to-high | cz      | storefront   | BT2018       | sneakers    |
      | androidTest | UAT_PS-6.17 | price-high-to-low | cz      | storefront   | BT2018       | sneakers    |

    @pl
    Examples: 
      | typeOfTest | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | webTest    | UAT_PS-6.17 | price-low-to-high | pl      | storefront   | BT2018       | nike        |
      | webTest    | UAT_PS-6.17 | price-high-to-low | pl      | storefront   | BT2018       | nike        |

    @pl_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | androidTest | UAT_PS-6.17 | price-low-to-high | pl      | storefront   | BT2018       | nike        |
      | androidTest | UAT_PS-6.17 | price-high-to-low | pl      | storefront   | BT2018       | nike        |

    @sk
    Examples: 
      | typeOfTest | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | webTest    | UAT_PS-6.17 | price-low-to-high | sk      | storefront   | BT2018       | nike        |
      | webTest    | UAT_PS-6.17 | price-high-to-low | sk      | storefront   | BT2018       | nike        |

    @sk_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | orderFilter       | country | authUsername | authPassword | productName |
      | androidTest | UAT_PS-6.17 | price-low-to-high | sk      | storefront   | BT2018       | nike        |
      | androidTest | UAT_PS-6.17 | price-high-to-low | ak      | storefront   | BT2018       | nike        |

  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig | driverMailConfig |
      | <testIdJIRA> | <typeOfTest> | default      | gmail            |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And go to send contact page
    And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   |
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> |
    When send the request
    Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   |
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                   |
      | webTest    | storefront   | BT2018       | UAT_CONT-15.1 | ita     | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta: ${reason} |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                   |
      | androidTest | storefront   | BT2018       | UAT_CONT-15.1 | ita     | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta: ${reason} |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                  |
      | webTest    | storefront   | BT2018       | UAT_CONT-15.1 | cz      | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta ${reason} |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                  |
      | androidTest | storefront   | BT2018       | UAT_CONT-15.1 | cz      | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta ${reason} |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                  |
      | webTest    | storefront   | BT2018       | UAT_CONT-15.1 | pl      | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta ${reason} |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                  |
      | androidTest | storefront   | BT2018       | UAT_CONT-15.1 | pl      | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta ${reason} |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                  |
      | webTest    | storefront   | BT2018       | UAT_CONT-15.1 | sk      | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta ${reason} |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA    | country | emailAddress                     | phone      | name  | surname   | reason   | comment                                                                       | emailSubject                                  |
      | androidTest | storefront   | BT2018       | UAT_CONT-15.1 | sk      | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link <a href="https://www.google.com/">clicca qui</a> | BATA richiesta di contatto ricevuta ${reason} |

  Scenario Outline: Store search-Store locator-City-Address-CAP - List Mode
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert field in the search bar with List Mode
      | searchField   |
      | <searchField> |
    Then Website show closest stores based on input received
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | testIdJIRA  | searchField | country |
      | webTest    | UAT_STR-4.2 | Napoli      | ita     |

    @ita_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | searchField | country |
      | androidTest | UAT_STR-4.2 | Napoli      | ita     |

    @cz
    Examples: 
      | typeOfTest | testIdJIRA  | searchField | country |
      | webTest    | UAT_STR-4.2 | Praha       | cz      |

    @cz_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | searchField | country |
      | androidTest | UAT_STR-4.2 | Praha       | cz      |

    @sk
    Examples: 
      | typeOfTest | testIdJIRA  | searchField | country |
      | webTest    | UAT_STR-4.2 | Bratislava  | sk      |

    @sk_mobile
    Examples: 
      | typeOfTest  | testIdJIRA  | searchField | country |
      | androidTest | UAT_STR-4.2 | Bratislava  | sk      |

  Scenario Outline: Track an order for a guest user via footer link
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | orderId   | emailAddress   | withError |
      | <orderId> | <emailAddress> | no        |
    And check the order status is equal to
      | orderDetailHeader   | orderStatus   |
      | <orderDetailHeader> | <orderStatus> |
    Then check the order details
      | orderNumber   | contactData   | deliveryAddress   | billingAddress   | payment   | totalPrice   | productName   | productBrand   | productId   | productSize   | productColor   | productQuantity   | productPrice   |
      | <orderNumber> | <contactData> | <deliveryAddress> | <billingAddress> | <payment> | <totalPrice> | <productName> | <productBrand> | <productId> | <productSize> | <productColor> | <productQuantity> | <productPrice> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | orderId       | emailAddress                 | orderDetailHeader                       | orderStatus            | orderNumber   | contactData                                 | deliveryAddress                                                                    | billingAddress                                                  | payment                                 | totalPrice | productName               | productBrand       | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023919 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023919 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 59.99 €    | Sandali Insolia con zeppa | BATA               |   7699251 |          35 | Blu          |               1 | 59.99 €      |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024034 | f.annunziata0585@hotmail.com | 08.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024034 | f.annunziata0585@hotmail.com\n+3495047035   | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 44.99 €    | Nike Tanjun               | NIKE               |   3096177 |        10,5 | Nero         |               1 | 39.99 €      |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023904 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023904 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 99.99 €    | Mocassini con nappa       | BATA THE SHOEMAKER |   8533140 |          44 | Marrone      |               1 | 99.99 €      |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024118 | f.annunziata0585@hotmail.com | 12.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024118 | f.annunziata0585@hotmail.com\n+393495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 44.99 €    | Infradito in vera pelle   | BATA               |   8646186 |          45 | Nero         |               1 | 39.99 €      |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | orderId       | emailAddress                 | orderDetailHeader                       | orderStatus            | orderNumber   | contactData                                 | deliveryAddress                                                                    | billingAddress                                                  | payment                                 | totalPrice | productName               | productBrand       | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023919 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023919 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 59.99 €    | Sandali Insolia con zeppa | BATA               |   7699251 |          35 | Blu          |               1 | 59.99 €      |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024034 | f.annunziata0585@hotmail.com | 08.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024034 | f.annunziata0585@hotmail.com\n+3495047035   | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 44.99 €    | Nike Tanjun               | NIKE               |   3096177 |        10,5 | Nero         |               1 | 39.99 €      |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023904 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023904 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 99.99 €    | Mocassini con nappa       | BATA THE SHOEMAKER |   8533140 |          44 | Marrone      |               1 | 99.99 €      |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024118 | f.annunziata0585@hotmail.com | 12.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024118 | f.annunziata0585@hotmail.com\n+393495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 44.99 €    | Infradito in vera pelle   | BATA               |   8646186 |          45 | Nero         |               1 | 39.99 €      |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | orderId        | emailAddress                                       | orderDetailHeader                         | orderStatus              | orderNumber    | contactData                                                          | deliveryAddress                                   | billingAddress                                     | payment       | totalPrice  | productName  | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | cz      | 00406100015002 | automation.test.prisma+testeruno33476491@gmail.com | 11.11.2019 OBJEDNÁVKA NA ODBĚR Z PRODEJNY | PŘÍPRAVA VAŠÍ OBJEDNÁVKY | 00406100015002 | automation.test.prisma+testeruno33476491@gmail.com\n+420324234232342 | Benešov\nBATA Tyršova 173\nBenešov, CZ, 256 87    | test prisma\nsadsad fadsad\nRoma, , 123 45         | Platba kartou | 2 798,00 Kč | NIKE 8036206 | NIKE         |   8036206 |         8,5 | černá        |               1 | 1 399,00 Kč  |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | cz      | 00406100015001 | automation.test.prisma@gmail.com                   | 11.11.2019 HLAVNÍ STRANA OBJEDNÁVKY BAŤA  | PŘÍPRAVA VAŠÍ OBJEDNÁVKY | 00406100015001 | automation.test.prisma@gmail.com\n+420324324234234                   | FdD DDD\nSofijské náměstí 3406/1\nPraha, , 143 00 | test prisma\nPiazza Garibaldi n.34\nRoma, , 123 45 | Dobírka       | 1 428,00 Kč | NIKE 8036206 | NIKE         |   8036206 |           8 | černá        |               1 | 1 399,00 Kč  |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | orderId        | emailAddress                                       | orderDetailHeader                         | orderStatus              | orderNumber    | contactData                                                          | deliveryAddress                                   | billingAddress                                     | payment       | totalPrice  | productName  | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | cz      | 00406100015002 | automation.test.prisma+testeruno33476491@gmail.com | 11.11.2019 OBJEDNÁVKA NA ODBĚR Z PRODEJNY | PŘÍPRAVA VAŠÍ OBJEDNÁVKY | 00406100015002 | automation.test.prisma+testeruno33476491@gmail.com\n+420324234232342 | Benešov\nBATA Tyršova 173\nBenešov, CZ, 256 87    | test prisma\nsadsad fadsad\nRoma, , 123 45         | Platba kartou | 2 798,00 Kč | NIKE 8036206 | NIKE         |   8036206 |         8,5 | černá        |               1 | 1 399,00 Kč  |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | cz      | 00406100015001 | automation.test.prisma@gmail.com                   | 11.11.2019 HLAVNÍ STRANA OBJEDNÁVKY BAŤA  | PŘÍPRAVA VAŠÍ OBJEDNÁVKY | 00406100015001 | automation.test.prisma@gmail.com\n+420324324234234                   | FdD DDD\nSofijské náměstí 3406/1\nPraha, , 143 00 | test prisma\nPiazza Garibaldi n.34\nRoma, , 123 45 | Dobírka       | 1 428,00 Kč | NIKE 8036206 | NIKE         |   8036206 |           8 | černá        |               1 | 1 399,00 Kč  |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | orderId        | emailAddress                     | orderDetailHeader                           | orderStatus | orderNumber    | contactData                                   | deliveryAddress                                                  | billingAddress                                                   | payment                                                | totalPrice | productName                    | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | pl      | 00408100012009 | automation.test.prisma@gmail.com | 09.11.2019 ZAMÓWIENIE Z DOSTAWĄ DO ODBIORCY | RELEASED    | 00408100012009 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | Płatność online kartą płatniczą VISA *************1111 | 415,00 zł  | NIKE 8096639                   | NIKE         |   8096639 |         8,5 | Czarny       |               1 | 415,00 zł    |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | pl      | 00408100012011 | automation.test.prisma@gmail.com | 09.11.2019 ZAMÓWIENIE Z DOSTAWĄ DO ODBIORCY | RELEASED    | 00408100012011 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | Płatność przy odbiorze                                 | 385,00 zł  | Skórzane buty męskie za kostkę | NIKE         |   8068435 |           8 | Brązowy      |               1 | 379,00 zł    |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | orderId        | emailAddress                     | orderDetailHeader                           | orderStatus | orderNumber    | contactData                                   | deliveryAddress                                                  | billingAddress                                                   | payment                                                | totalPrice | productName                    | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | pl      | 00408100012009 | automation.test.prisma@gmail.com | 09.11.2019 ZAMÓWIENIE Z DOSTAWĄ DO ODBIORCY | RELEASED    | 00408100012009 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | Płatność online kartą płatniczą VISA *************1111 | 415,00 zł  | NIKE 8096639                   | NIKE         |   8096639 |         8,5 | Czarny       |               1 | 415,00 zł    |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | pl      | 00408100012011 | automation.test.prisma@gmail.com | 09.11.2019 ZAMÓWIENIE Z DOSTAWĄ DO ODBIORCY | RELEASED    | 00408100012011 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 12-345 | Płatność przy odbiorze                                 | 385,00 zł  | Skórzane buty męskie za kostkę | NIKE         |   8068435 |           8 | Brązowy      |               1 | 379,00 zł    |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | orderId        | emailAddress                     | orderDetailHeader                   | orderStatus                  | orderNumber    | contactData                                   | deliveryAddress                                                  | billingAddress                                                   | payment                              | totalPrice | productName                                   | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | sk      | 00407100013100 | automation.test.prisma@gmail.com | 09.11.2019 DOMÁCA OBJEDNÁVKA U BAŤU | RELEASED                     | 00407100013100 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | Platba kartou VISA *************1111 | 25,99 €    | Dámske ružové tenisky v štýle Chunky Sneakers | NORTH STAR   |   6415602 |          36 | Ružová       |               1 | 25,99 €      |
      | webTest    | storefront   | BT2018       | UAT_OT-13.1 | sk      | 00407100013107 | automation.test.prisma@gmail.com | 09.11.2019 DOMÁCA OBJEDNÁVKA U BAŤU | VYBAVOVANIE VAŠEJ OBJEDNÁVKY | 00407100013107 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | Dobierka                             | 26,99 €    | Dámske ružové tenisky v štýle Chunky Sneakers | NORTH STAR   |   6415602 |          36 | Ružová       |               1 | 25,99 €      |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | orderId        | emailAddress                     | orderDetailHeader                   | orderStatus                  | orderNumber    | contactData                                   | deliveryAddress                                                  | billingAddress                                                   | payment                              | totalPrice | productName                                   | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | sk      | 00407100013100 | automation.test.prisma@gmail.com | 09.11.2019 DOMÁCA OBJEDNÁVKA U BAŤU | RELEASED                     | 00407100013100 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | Platba kartou VISA *************1111 | 25,99 €    | Dámske ružové tenisky v štýle Chunky Sneakers | NORTH STAR   |   6415602 |          36 | Ružová       |               1 | 25,99 €      |
      | androidTest | storefront   | BT2018       | UAT_OT-13.1 | sk      | 00407100013107 | automation.test.prisma@gmail.com | 09.11.2019 DOMÁCA OBJEDNÁVKA U BAŤU | VYBAVOVANIE VAŠEJ OBJEDNÁVKY | 00407100013107 | automation.test.prisma@gmail.com\n+3333333333 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | tester prisma\nPiazza Giuseppe Garibaldi n. 34\nNapoli, , 123 45 | Dobierka                             | 26,99 €    | Dámske ružové tenisky v štýle Chunky Sneakers | NORTH STAR   |   6415602 |          36 | Ružová       |               1 | 25,99 €      |

  Scenario Outline: Track an order for a registered user via footer link
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig |
      | <testIdJIRA> | <typeOfTest> | default      |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | orderId   | orderDetailHeader | orderStatus   |
      | <orderId> | <orderHeader>     | <orderStatus> |
    Then check the order details
      | orderNumber   | orderStatus   | contactData   | deliveryAddress   | billingAddress   | payment   | totalPrice   | productName   | productBrand   | productId   | productSize   | productColor   | productQuantity   | productPrice   |
      | <orderNumber> | <orderStatus> | <contactData> | <deliveryAddress> | <billingAddress> | <payment> | <totalPrice> | <productName> | <productBrand> | <productId> | <productSize> | <productColor> | <productQuantity> | <productPrice> |
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | email                                    | password   | orderId       | orderHeader                             | orderNumber   | orderStatus            | contactData                                           | deliveryAddress                                                                    | billingAddress                                           | payment                                 | totalPrice | productName        | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024201 | 13.08.2019 ORDINE CON SPEDIZIONE A CASA | 0027210024201 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142                           | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Carta di credito VISA *************4305 | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          37 | Bianco       |               1 | 23.99 €      |
      | webTest    | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024202 | 13.08.2019 ORDINE CLICK&COLLECT         | 0027210024202 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Carta di credito VISA *************4305 | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          41 | Bianco       |               1 | 23.99 €      |
      | webTest    | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024203 | 13.08.2019 ORDINE CON SPEDIZIONE A CASA | 0027210024203 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142                           | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Paga con Pay Pal                        | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          40 | Bianco       |               1 | 23.99 €      |
      | webTest    | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024204 | 13.08.2019 ORDINE CLICK&COLLECT         | 0027210024204 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Paga con Pay Pal                        | 28.99 €    | Ciabatte in pelle  | BATA         |   7711113 |          40 | Bianco       |               1 | 23.99 €      |

    @ita_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | email                                    | password   | orderId       | orderHeader                             | orderNumber   | orderStatus            | contactData                                           | deliveryAddress                                                                    | billingAddress                                           | payment                                 | totalPrice | productName        | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024201 | 13.08.2019 ORDINE CON SPEDIZIONE A CASA | 0027210024201 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142                           | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Carta di credito VISA *************4305 | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          37 | Bianco       |               1 | 23.99 €      |
      | androidTest | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024202 | 13.08.2019 ORDINE CLICK&COLLECT         | 0027210024202 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Carta di credito VISA *************4305 | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          41 | Bianco       |               1 | 23.99 €      |
      | androidTest | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024203 | 13.08.2019 ORDINE CON SPEDIZIONE A CASA | 0027210024203 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142                           | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Paga con Pay Pal                        | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          40 | Bianco       |               1 | 23.99 €      |
      | androidTest | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024204 | 13.08.2019 ORDINE CLICK&COLLECT         | 0027210024204 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Paga con Pay Pal                        | 28.99 €    | Ciabatte in pelle  | BATA         |   7711113 |          40 | Bianco       |               1 | 23.99 €      |

    @cz
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | email                               | password   | orderId        | orderHeader                               | orderNumber    | orderStatus | contactData                                      | deliveryAddress                                | billingAddress                                          | payment       | totalPrice  | productName                         | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.4 | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | 00406100014708 | 09.11.2019 OBJEDNÁVKA NA ODBĚR Z PRODEJNY | 00406100014708 | RELEASED    | automation.test.prisma+cz@gmail.com\n+4205664646 | Benešov\nBATA Tyršova 173\nBenešov, CZ, 256 87 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, , 123 45 | Platba kartou | 2 798,00 Kč | Šedé pánské tenisky z broušené kůže | NIKE         |   8032106 |           8 | Šedá         |               1 | 2 798,00 KČ  |

    @cz_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | email                               | password   | orderId        | orderHeader                               | orderNumber    | orderStatus | contactData                                      | deliveryAddress                                | billingAddress                                          | payment       | totalPrice  | productName                         | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.4 | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | 00406100014708 | 09.11.2019 OBJEDNÁVKA NA ODBĚR Z PRODEJNY | 00406100014708 | RELEASED    | automation.test.prisma+cz@gmail.com\n+4205664646 | Benešov\nBATA Tyršova 173\nBenešov, CZ, 256 87 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, , 123 45 | Platba kartou | 2 798,00 Kč | Šedé pánské tenisky z broušené kůže | NIKE         |   8032106 |           8 | Šedá         |               1 | 2 798,00 KČ  |

    @pl
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | email                               | password   | orderId        | orderHeader                                 | orderNumber    | orderStatus | contactData                                         | deliveryAddress                                                   | billingAddress                                                    | payment                | totalPrice | productName  | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.4 | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | 00408100011815 | 09.11.2019 ZAMÓWIENIE Z DOSTAWĄ DO ODBIORCY | 00408100011815 | RELEASED    | automation.test.prisma+pl@gmail.com\n+4641313131313 | Test Prisma\nVia Giuseppe Garibalid n. 35\nPoland, Polska, 12-345 | Test Prisma\nVia Giuseppe Garibalid n. 35\nPoland, Polska, 12-345 | Płatność przy odbiorze | 161,00 zł  | NIKE 4095403 | NIKE         |   4095403 |         4,5 | Różowy       |               1 | 155,00 zł    |

    @pl_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | email                               | password   | orderId        | orderHeader                                 | orderNumber    | orderStatus | contactData                                         | deliveryAddress                                                   | billingAddress                                                    | payment                | totalPrice | productName  | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.4 | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | 00408100011815 | 09.11.2019 ZAMÓWIENIE Z DOSTAWĄ DO ODBIORCY | 00408100011815 | RELEASED    | automation.test.prisma+pl@gmail.com\n+4641313131313 | Test Prisma\nVia Giuseppe Garibalid n. 35\nPoland, Polska, 12-345 | Test Prisma\nVia Giuseppe Garibalid n. 35\nPoland, Polska, 12-345 | Płatność przy odbiorze | 161,00 zł  | NIKE 4095403 | NIKE         |   4095403 |         4,5 | Różowy       |               1 | 155,00 zł    |

    @sk
    Examples: 
      | typeOfTest | authUsername | authPassword | testIdJIRA  | country | email                               | password   | orderId        | orderHeader                         | orderNumber    | orderStatus | contactData                                      | deliveryAddress                                 | billingAddress                                                  | payment       | totalPrice | productName                   | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | storefront   | BT2018       | UAT_OT-13.4 | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | 00407100013111 | 09.11.2019 VYZDVIHNITE SI V OBCHODE | 00407100013111 | RELEASED    | automation.test.prisma+sk@gmail.com\n+6461313131 | Bratislava\nNám. SNP 16\nBratislava, SK, 811 06 | Francesco Annunziata\nvia pantano gnilino n. 2\nSarno, , 123 45 | Ship To store | 79,98 €    | Biele tenisky detské športové | NIKE         |   4091386 |           4 | Biela        |               1 | 39,99 €      |

    @sk_mobile
    Examples: 
      | typeOfTest  | authUsername | authPassword | testIdJIRA  | country | email                               | password   | orderId        | orderHeader                         | orderNumber    | orderStatus | contactData                                      | deliveryAddress                                 | billingAddress                                                  | payment       | totalPrice | productName                   | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | androidTest | storefront   | BT2018       | UAT_OT-13.4 | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | 00407100013111 | 09.11.2019 VYZDVIHNITE SI V OBCHODE | 00407100013111 | RELEASED    | automation.test.prisma+sk@gmail.com\n+6461313131 | Bratislava\nNám. SNP 16\nBratislava, SK, 811 06 | Francesco Annunziata\nvia pantano gnilino n. 2\nSarno, , 123 45 | Ship To store | 79,98 €    | Biele tenisky detské športové | NIKE         |   4091386 |           4 | Biela        |               1 | 39,99 €      |

  Scenario Outline: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest   | driverConfig |
      | UAT_WL-5.1 | <typeOfTest> | default      |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User search an item by name with search bar
      | productName   |
      | <productName> |
    And Click on add to whishlist registered
    Then Item is added in wishlist page registered
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | country | email                                    | password    | authUsername | authPassword | productName |
      | webTest    | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | nike        |

    @ita_mobile
    Examples: 
      | typeOfTest  | country | email                                    | password    | authUsername | authPassword | productName |
      | androidTest | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | nike        |

    @cz
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @cz_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @pl
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @pl_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @sk
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @sk_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

  Scenario Outline: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest   | driverConfig |
      | UAT_WL-5.3 | <typeOfTest> | default      |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User go to wishlist page
    And Remove the item from wishlist page
    Then Item is removed from wishlist page
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | country | email                                    | password    | authUsername | authPassword | productName |
      | webTest    | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | nike        |

    @ita_mobile
    Examples: 
      | typeOfTest  | country | email                                    | password    | authUsername | authPassword | productName |
      | androidTest | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | nike        |

    @cz
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @cz_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @pl
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @pl_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @sk
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @sk_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

  Scenario Outline: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest   | driverConfig |
      | UAT_WL-5.8 | <typeOfTest> | default      |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name with search bar
      | productName   |
      | <productName> |
    And Click on add to whishlist guest
    Then User will be redirect on the login page
    And close the browser and clean all resources

    @ita
    Examples: 
      | typeOfTest | country | email                                    | password    | authUsername | authPassword | productName |
      | webTest    | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | nike        |

    @ita_mobile
    Examples: 
      | typeOfTest  | country | email                                    | password    | authUsername | authPassword | productName |
      | androidTest | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | nike        |

    @cz
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @cz_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | cz      | automation.test.prisma+cz@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @pl
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @pl_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | pl      | automation.test.prisma+pl@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @sk
    Examples: 
      | typeOfTest | country | email                               | password   | authUsername | authPassword | productName |
      | webTest    | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |

    @sk_mobile
    Examples: 
      | typeOfTest  | country | email                               | password   | authUsername | authPassword | productName |
      | androidTest | sk      | automation.test.prisma+sk@gmail.com | Alice.it0$ | storefront   | BT2018       | nike        |
