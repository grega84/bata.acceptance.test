Feature: UAT_WL: Whishlist-Registered user-add to WL-PLP
###########################################################################
# COUNTRY ITA
###########################################################################
@ita
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.1 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.3 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.2 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist from list page registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.4 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Remove the item previously putted in the wishlist from list page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.8 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.7 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.5 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.6 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  #OUT OF SCOPE FOR MOBILE
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.7 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  ###########################################################################
  # COUNTRY CZ
  ###########################################################################
  @cz
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.1 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName | 
      | 5411600     | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.3 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.2 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName | 
      | 5411600     | 
      And Click on add to whishlist from list page registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.4 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName | 
      | 5411600     | 
      And Remove the item previously putted in the wishlist from list page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.8 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | cz      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName                   | 
      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.7 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | cz      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName | 
      | SANDÁLY     | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.5 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName | 
      | 5411600     | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.6 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName | 
      | 5411600     | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  ###########################################################################
  # COUNTRY ITA MOBILE
  ###########################################################################
  @ita @mobile
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.1 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.3 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.8 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.5 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.6 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  ###########################################################################
  # COUNTRY PL
  ###########################################################################
  @pl
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.1 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.3 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.2 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist from list page registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.4 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Remove the item previously putted in the wishlist from list page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.8 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.7 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.5 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.6 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  #OUT OF SCOPE FOR MOBILE
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.7 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  ###########################################################################
  # COUNTRY PL MOBILE
  ###########################################################################
  @pl @mobile
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.1 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.3 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.8 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.5 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.6 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resourcesFeature: UAT_WL: Whishlist-Registered user-add to WL-PLP
###########################################################################
# COUNTRY ITA
###########################################################################
@ita
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.1 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.3 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.2 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist from list page registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.4 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Remove the item previously putted in the wishlist from list page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.8 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.7 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.5 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @ita
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.6 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                    | password    | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Prisma2018! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  #OUT OF SCOPE FOR MOBILE
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.7 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  ###########################################################################
  # COUNTRY CZ
  ###########################################################################
  @cz
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.1 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName | 
      | 5411600     | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.3 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.2 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName | 
      | 5411600     | 
      And Click on add to whishlist from list page registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.4 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName | 
      | 5411600     | 
      And Remove the item previously putted in the wishlist from list page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.8 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | cz      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName                   | 
      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.7 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | cz      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName | 
      | SANDÁLY     | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.5 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName | 
      | 5411600     | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @cz
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.6 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                          | password     | authUsername | authPassword | 
      | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName | 
      | 5411600     | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  ###########################################################################
  # COUNTRY ITA MOBILE
  ###########################################################################
  @ita @mobile
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.1 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.3 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.8 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.5 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @ita @mobile
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.6 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                    | password   | authUsername | authPassword | 
      | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName       | 
      | sneakers in pelle | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  ###########################################################################
  # COUNTRY PL
  ###########################################################################
  @pl
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.1 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.3 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.2 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist from list page registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.4 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Remove the item previously putted in the wishlist from list page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.8 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.7 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.5 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @pl
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.6 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  #OUT OF SCOPE FOR MOBILE
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.7 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  ###########################################################################
  # COUNTRY PL MOBILE
  ###########################################################################
  @pl @mobile
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.1 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.3 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.8 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.5 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @pl @mobile
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.6 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | skórzane trampki | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
      
      
        ###########################################################################
  # COUNTRY SK
  ###########################################################################
  @sk
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.1 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @sk
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.3 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @sk
  Scenario: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.2 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist from list page registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @sk
  Scenario: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.4 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | členkové topánky | 
      And Remove the item previously putted in the wishlist from list page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @sk
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.8 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | sk      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @sk
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.7 | webTest    | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | sk      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @sk
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.5 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @sk
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest | 
      | UAT_WL-5.6 | webTest    | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources
  
  #OUT OF SCOPE FOR MOBILE
  Scenario: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.7 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | sk      | storefront   | BT2018       | 
     When User search an item by name with search bar and click on showall
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist from list page guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  ###########################################################################
  # COUNTRY PL MOBILE
  ###########################################################################
  @sk @mobile
  Scenario: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.1 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist registered
     Then Item is added in wishlist page registered
      And close the browser and clean all resources
  
  @sk @mobile
  Scenario: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.3 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User go to wishlist page
      And Remove the item from wishlist page
     Then Item is removed from wishlist page
      And close the browser and clean all resources
  
  @sk @mobile
  Scenario: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.8 | androidTest | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | sk      | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist guest
     Then User will be redirect on the login page
      And close the browser and clean all resources
  
  @sk @mobile
  Scenario: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.5 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist registered
      And Click on copy url and paste in new chrome tab
     Then The user will be reidirect in wish list
      And close the browser and clean all resources
  
  @sk @mobile
  Scenario: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA | typeOfTest  | 
      | UAT_WL-5.6 | androidTest | 
     When The user go to BATA site and logged user
      | country | email                                | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
     When User search an item by name with search bar
      | productName      | 
      | členkové topánky | 
      And Click on add to whishlist registered
      And The user add to cart one item in wishlist page
     Then Item added to the cart, minicart slider open
      And The user remove the item from wishlist
      And close the browser and clean all resources