@UAT_WL
Feature: Whishlist

  @UAT_WL1 @sid:testIdJIRA
  Scenario Outline: Whishlist-Registered user-add to WL-PDP
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User search an item by name with search bar
      | productName   |
      | <productName> |
    And Click on add to whishlist registered
    Then Item is added in wishlist page registered
    And close the browser and clean all resources

    @UAT_WL1_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.1 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | nike |

    @UAT_WL1_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.1 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL1_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.1 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL1_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.1 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL1_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.1 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL1_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.1 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL1_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.1 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL2 @sid:testIdJIRA
  Scenario Outline: Whishlist-Registered user-remove from WL
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User go to wishlist page
    And Remove the item from wishlist page
    Then Item is removed from wishlist page
    And close the browser and clean all resources

    @UAT_WL2_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.3 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL2_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.3 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL2_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.3 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL2_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.3 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL2_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.3 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL2_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.3 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL2_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.3 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL3 @sid:testIdJIRA
  Scenario Outline: Whishlist-Registered user-add to WL-PLP
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User search an item by name with search bar and click on showall
      | productName   |
      | <productName> |
    And Click on add to whishlist from list page registered
    Then Item is added in wishlist page registered
    And close the browser and clean all resources

    @UAT_WL3_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.2 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL3_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.2 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL3_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.2 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL3_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.2 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL3_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.2 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL3_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.2 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL3_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.2 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL4 @sid:testIdJIRA
  Scenario Outline: Whishlist-Registered user-remove from WL-PDP
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User search an item by name with search bar and click on showall
      | productName   |
      | <productName> |
    And Remove the item previously putted in the wishlist from list page
    Then Item is removed from wishlist page
    And close the browser and clean all resources

    @UAT_WL4_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.4 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL4_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.4 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL4_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.4 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL4_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.4 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL4_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.4 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL4_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.4 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL4_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.4 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL5 @sid:testIdJIRA
  Scenario Outline: Whishlist-Guest-add to WL-PDP
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name with search bar
      | productName   |
      | <productName> |
    And Click on add to whishlist guest
    Then User will be redirect on the login page
    And close the browser and clean all resources

    @UAT_WL5_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.8 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL5_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.8 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL5_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.8 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL5_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.8 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL5_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.8 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL5_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.8 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL5_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.8 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL6 @sid:testIdJIRA
  Scenario Outline: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User search an item
      | productName   |
      | <productName> |
    And click on show more link
    And Click on add to whishlist from list page guest
    Then User will be redirect on the login page
    And close the browser and clean all resources

    @UAT_WL6_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.7 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL6_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.7 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL6_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.7 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL6_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL6_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL6_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL6_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL7 @sid:testIdJIRA
  Scenario Outline: Whishlist-Registered user-Share WL
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User search an item by name with search bar
      | productName   |
      | <productName> |
    And Click on add to whishlist registered
    And Click on copy url and paste in new chrome tab
    Then The user will be reidirect in wish list
    And close the browser and clean all resources

    @UAT_WL7_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.7 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL7_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.7 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL7_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.7 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL7_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL7_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL7_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL7_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL8 @sid:testIdJIRA
  Scenario Outline: Whishlist-Registered user-Add to cart
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site and logged user
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    When User search an item by name with search bar
      | productName   |
      | <productName> |
    And Click on add to whishlist registered
    And The user add to cart one item in wishlist page
    Then Item added to the cart, minicart slider open
    And The user remove the item from wishlist
    And close the browser and clean all resources

    @UAT_WL8_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.6 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL8_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.6 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL8_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.6 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL8_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.6 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL8_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.6 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL8_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.6 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL8_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.6 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

  @UAT_WL9 @sid:testIdJIRA
  Scenario Outline: Whishlist-Guest-add to WL-PLP
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User search an item
      | productName   |
      | <productName> |
    And click on show more link
    And Click on add to whishlist from list page guest
    Then User will be redirect on the login page
    And close the browser and clean all resources

    @UAT_WL9_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.7 | webTest    | default_ita  | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL9_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | email                                    | password   | authUsername | authPassword | productName       |
      | UAT_WL-5.7 | androidTest | mobilebrowser_ita | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | sneakers in pelle |

    @UAT_WL9_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                          | password     | authUsername | authPassword | productName |
      | UAT_WL-5.7 | webTest    | default_cz   | cz      | e2eceprisma+prisma40@gmail.com | testPrisma1! | storefront   | BT2018       |     5411600 |

    @UAT_WL9_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | webTest    | default_pl   | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL9_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | androidTest | mobilebrowser_pl | pl      | prisma_wishlist_test@sharklasers.com | Alice.it0$ | storefront   | BT2018       | skórzane trampki |

    @UAT_WL9_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | webTest    | default_sk   | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |

    @UAT_WL9_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | email                                        | password   | authUsername | authPassword | productName      |
      | UAT_WL-5.7 | androidTest | mobilebrowser_sk | sk      | e2eceprisma+automation_wishlist_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | členkové topánky |
