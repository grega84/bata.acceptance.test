Feature: Customer update info
###############################################################
#COUNTRY ITA
###############################################################

@ita
  Scenario: Customer update info-Edit Contact details
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When The user insert the following data for the registration
      | password   | confirmPassword | name  | surname | address          | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | Alice.it0$ | Alice.it0$      | Prova | Test    | via delle pialle | 80100 | Napoli | Napoli   | 3214567898 | 01      | Gennaio   | 1960     | 
      And access to myprofile
      And edit contact details with following data
      | submitPassword | 
      | Alice.it0$     | 
     Then check if the contact details are updated
      And close the browser and clean all resources
  
  @ita  
  Scenario: Customer update info-Change PWD
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.2 | 
     When The user log in to BATA IT site with credential
      | country | email                                  | password   | authUsername | authPassword | 
      | ita     | prisma_change_password@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And change the password with this
      | password   | newPassword      | 
      | Alice.it0$ | Alice.it0$!nuovo | 
     Then check if the password is changed
      | email                                  | password         | 
      | prisma_change_password@sharklasers.com | Alice.it0$!nuovo | 
      And set the password to the old value
      | email                                  | password         | newPassword | 
      | prisma_change_password@sharklasers.com | Alice.it0$!nuovo | Alice.it0$  | 
      And close the browser and clean all resources
  
  @ita  
  Scenario: Customer update info-Edit Personal Data
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.3 | 
     When The user log in to BATA IT site with credential
      | country | email                              | password   | authUsername | authPassword | 
      | ita     | prisma_update_info@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And edit personal data with following data
     Then check if the personal data is updated
      And close the browser and clean all resources
  
  @ita  
  Scenario: Customer update info-Add new address
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.4 | 
     When The user log in to BATA IT site with credential
      | country | email                              | password   | authUsername | authPassword | 
      | ita     | prisma_add_address@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And add a new address
     Then check if the new address is correctly added
      And close the browser and clean all resources
  
  @ita  
  Scenario: Customer update info-accelerators - loyality 15 points added to customer profile
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | ita     | storefront   | BT2018       | 
     When The user insert the following data for the registration
      | password   | confirmPassword | name  | surname   | address                    | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | Alice.it0$ | Alice.it0$      | Nello | Specifico | Viale della Villa Romana 1 | 80100 | Napoli | Napoli   | 3214567898 | 01      | Gennaio   | 1960     | 
     Then The user will be redirect on the profile page
      And complete all the loyalty mandatory fields
     Then check if points and tier are updated
      And close the browser and clean all resources
  
  @ita  
  Scenario Outline: Customer update info-accelerators - 20 points loyalty added after clicking on registration email
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername   | authPassword   | 
      | ita     | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And click on confirmation email
      And The user log in to BATA IT site with credential
      | country | email   | password   | authUsername   | authPassword   | 
      | ita     | <email> | <password> | <authUsername> | <authPassword> | 
     Then check if points and tier are updated with 20 loyalty points
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | password | confirmPassword | name | surname | address | cap | city | province | phone | dayBorn | monthBorn | yearBorn | 
      | authUsername | authPassword | testIdJIRA | country | password | confirmPassword | name | surname | address | cap | city | province | phone | dayBorn | monthBorn | yearBorn | 
  
  @ita  
  Scenario: Customer update info-use CRM UI
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.3 | 
     When login to the web CRM with following credential
      | country | email  | password | 
      | ita     | PRISMA | PRISMA   | 
      And goto customer section
      And select an user
      | nome | cognome | email | cellulare | numeroCarta | codiceCliente | 
      |      |         |       |           |             | 1000564527    | 
      And update the customer info
      And save the modify
      And The user log in to BATA IT site with credential
      | country | email                             | password   | authUsername | authPassword | 
      | ita     | prisma_crm_update@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
     Then check if the personal data is updated
      And close the browser and clean all resources
  
  ###############################################################
  #COUNTRY PL
  ###############################################################
  
  @pl
  Scenario: Customer update info-Edit Contact details
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When The user insert the following data for the registration
      | password   | confirmPassword | name  | surname | address          | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | Alice.it0$ | Alice.it0$      | Prova | Test    | via delle pialle | 12-345 | Napoli | Napoli   | 3214567898 | 01      | Gennaio   | 1960     | 
      And access to myprofile
      And edit contact details with following data
      | submitPassword | 
      | Alice.it0$     | 
     Then check if the contact details are updated
      And close the browser and clean all resources
  
  @pl
  Scenario: Customer update info-Change PWD
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.2 | 
     When The user log in to BATA IT site with credential
      | country | email                                  | password   | authUsername | authPassword | 
      | pl      | prisma_change_password@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And change the password with this
      | password   | newPassword      | 
      | Alice.it0$ | Alice.it0$!nuovo | 
     Then check if the password is changed
      | email                                  | password         | 
      | prisma_change_password@sharklasers.com | Alice.it0$!nuovo | 
      And set the password to the old value
      | email                                  | password         | newPassword | 
      | prisma_change_password@sharklasers.com | Alice.it0$!nuovo | Alice.it0$  | 
      And close the browser and clean all resources
  
  @pl 
  Scenario: Customer update info-Edit Personal Data
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.3 | 
     When The user log in to BATA IT site with credential
      | country | email                              | password   | authUsername | authPassword | 
      | pl      | prisma_update_info@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And edit personal data with following data
     Then check if the personal data is updated
      And close the browser and clean all resources
  
  @pl
  Scenario: Customer update info-Add new address
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.4 | 
     When The user log in to BATA IT site with credential
      | country | email                              | password   | authUsername | authPassword | 
      | pl      | prisma_add_address@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And add a new address
     Then check if the new address is correctly added
      And close the browser and clean all resources
  
  @pl 
  Scenario: Customer update info-accelerators - loyality 15 points added to customer profile
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | pl      | storefront   | BT2018       | 
     When The user insert the following data for the registration
      | password   | confirmPassword | name  | surname   | address                    | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | Alice.it0$ | Alice.it0$      | Nello | Specifico | Viale della Villa Romana 1 | 80100 | Napoli | Napoli   | 3214567898 | 01      | Gennaio   | 1960     | 
     Then The user will be redirect on the profile page
      And complete all the loyalty mandatory fields
     Then check if points and tier are updated
      And close the browser and clean all resources
  
  @pl  
  Scenario Outline: Customer update info-accelerators - 20 points loyalty added after clicking on registration email
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername   | authPassword   | 
      | pl      | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And click on confirmation email
      And The user log in to BATA IT site with credential
      | country | email   | password   | authUsername   | authPassword   | 
      | pl      | <email> | <password> | <authUsername> | <authPassword> | 
     Then check if points and tier are updated with 20 loyalty points
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | password | confirmPassword | name | surname | address | cap | city | province | phone | dayBorn | monthBorn | yearBorn | 
      | authUsername | authPassword | testIdJIRA | country | password | confirmPassword | name | surname | address | cap | city | province | phone | dayBorn | monthBorn | yearBorn | 
  
  @pl
  Scenario: Customer update info-use CRM UI
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.3 | 
     When login to the web CRM with following credential
      | country | email  | password | 
      | pl      | PRISMA | PRISMA   | 
      And goto customer section
      And select an user
      | nome | cognome | email | cellulare | numeroCarta | codiceCliente | 
      |      |         |       |           |             | 1000564527    | 
      And update the customer info
      And save the modify
      And The user log in to BATA IT site with credential
      | country | email                             | password   | authUsername | authPassword | 
      | pl      | prisma_crm_update@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
     Then check if the personal data is updated
      And close the browser and clean all resources
      
       ###############################################################
  #COUNTRY SK
  ###############################################################
  
  @sk-da-verificare
  Scenario: Customer update info-Edit Contact details
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | sk      | storefront   | BT2018       | 
     When The user insert the following data for the registration
      | password   | confirmPassword | name  | surname | address          | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | Alice.it0$ | Alice.it0$      | Prova | Test    | via delle pialle | 12-345 | Napoli | Napoli   | 3214567898 | 01      | Gennaio   | 1960     | 
      And access to myprofile
      And edit contact details with following data
      | submitPassword | 
      | Alice.it0$     | 
     Then check if the contact details are updated
      And close the browser and clean all resources
  
  @sk
  Scenario: Customer update info-Change PWD
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.2 | 
     When The user log in to BATA IT site with credential
      | country | email                                  | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_updatepwd_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And change the password with this
      | password   | newPassword      | 
      | Alice.it0$ | Alice.it0$!nuovo | 
     Then check if the password is changed
      | email                                  | password         | 
      | e2eceprisma+automation_updatepwd_sk@gmail.com | Alice.it0$!nuovo | 
      And set the password to the old value
      | email                                  | password         | newPassword | 
      | e2eceprisma+automation_updatepwd_sk@gmail.com | Alice.it0$!nuovo | Alice.it0$  | 
      And close the browser and clean all resources
  
  @sk 
  Scenario: Customer update info-Edit Personal Data
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.3 | 
     When The user log in to BATA IT site with credential
      | country | email                              | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_updateinfo_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And edit personal data with following data
     Then check if the personal data is updated
      And close the browser and clean all resources
  
  @sk
  Scenario: Customer update info-Add new address
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_3.4 | 
     When The user log in to BATA IT site with credential
      | country | email                              | password   | authUsername | authPassword | 
      | sk      | e2eceprisma+automation_updateaddress_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
      And add a new address
     Then check if the new address is correctly added
      And close the browser and clean all resources
  
  @sk-da-verificare 
  Scenario: Customer update info-accelerators - loyality 15 points added to customer profile
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername | authPassword | 
      | sk      | storefront   | BT2018       | 
     When The user insert the following data for the registration
      | password   | confirmPassword | name  | surname   | address                    | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | Alice.it0$ | Alice.it0$      | Nello | Specifico | Viale della Villa Romana 1 | 80100 | Napoli | Napoli   | 3214567898 | 01      | Gennaio   | 1960     | 
     Then The user will be redirect on the profile page
      And complete all the loyalty mandatory fields
     Then check if points and tier are updated
      And close the browser and clean all resources
  
  @sk-da-verificare  
  Scenario Outline: Customer update info-accelerators - 20 points loyalty added after clicking on registration email
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.2 | 
     When The user go to BATA site
      | country | authUsername   | authPassword   | 
      | sk      | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And click on confirmation email
      And The user log in to BATA IT site with credential
      | country | email   | password   | authUsername   | authPassword   | 
      | sk      | <email> | <password> | <authUsername> | <authPassword> | 
     Then check if points and tier are updated with 20 loyalty points
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | password | confirmPassword | name | surname | address | cap | city | province | phone | dayBorn | monthBorn | yearBorn | 
      | authUsername | authPassword | testIdJIRA | country | password | confirmPassword | name | surname | address | cap | city | province | phone | dayBorn | monthBorn | yearBorn | 
  
  @sk-da-verificare
  Scenario: Customer update info-use CRM UI
    Given the JIRA test
      | testIdJIRA | 
      | UAT_CU_4.3 | 
     When login to the web CRM with following credential
      | country | email  | password | 
      | sk      | PRISMA | PRISMA   | 
      And goto customer section
      And select an user
      | nome | cognome | email | cellulare | numeroCarta | codiceCliente | 
      |      |         |       |           |             | 1000564527    | 
      And update the customer info
      And save the modify
      And The user log in to BATA IT site with credential
      | country | email                             | password   | authUsername | authPassword | 
      | sk      | prisma_crm_update@sharklasers.com | Alice.it0$ | storefront   | BT2018       | 
      And access to myprofile
     Then check if the personal data is updated
      And close the browser and clean all resources