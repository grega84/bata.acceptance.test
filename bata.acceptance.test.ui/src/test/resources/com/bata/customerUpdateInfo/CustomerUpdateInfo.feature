@UAT_CU
Feature: Customer update

  @UAT_CU42 @sid:testIdJIRA
  Scenario Outline: Customer update info-Edit Contact details
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   |
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> |
    And access to myprofile
    And edit contact details with following data
      | submitPassword   |
      | <submitPassword> |
    Then check if the contact details are updated
    And close the browser and clean all resources

    @UAT_CU42_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | password   | confirmPassword | name  | surname | address          | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | submitPassword |
      | UAT_CU_4.2 | webTest    | default_ita  | ita     | storefront   | BT2018       | Alice.it0$ | Alice.it0$      | Prova | Test    | via delle pialle | 80100 | Napoli | Napoli   | 3214567898 |      01 | Gennaio   |     1960 | Alice.it0$     |

    @UAT_CU42_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | password   | confirmPassword | name  | surname | address          | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | submitPassword |
      | UAT_CU_4.2 | webTest    | default_pl   | pl      | storefront   | BT2018       | Alice.it0$ | Alice.it0$      | Prova | Test    | via delle pialle | 12-345 | Napoli | Napoli   | 3214567898 |      01 | Gennaio   |     1960 | Alice.it0$     |

    @UAT_CU42_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | password   | confirmPassword | name  | surname | address          | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | submitPassword |
      | UAT_CU_4.2 | webTest    | default_sk   | sk      | storefront   | BT2018       | Alice.it0$ | Alice.it0$      | Prova | Test    | via delle pialle | 12-345 | Napoli | Napoli   | 3214567898 |      01 | Gennaio   |     1960 | Alice.it0$     |

  @UAT_CU32 @sid:testIdJIRA
  Scenario Outline: Customer update info-Change PWD
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And access to myprofile
    And change the password with this
      | password   | newPassword   |
      | <password> | <newPassword> |
    Then check if the password is changed
      | email   | password      |
      | <email> | <newPassword> |
    And set the password to the old value
      | email   | password      | newPassword |
      | <email> | <newPassword> | <password>  |
    And close the browser and clean all resources

    @UAT_CU32_ita
    Examples: 
      | testIdJIRA | typeOfTest | country | driverConfig | email                                  | password   | authUsername | authPassword | password   | newPassword      | email                                  |
      | UAT_CU_3.2 | webTest    | ita     | default_ita  | prisma_change_password@sharklasers.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$ | Alice.it0$!nuovo | prisma_change_password@sharklasers.com |

    @UAT_CU32_pl
    Examples: 
      | testIdJIRA | typeOfTest | country | driverConfig | email                                  | password   | authUsername | authPassword | password   | newPassword      | email                                  |
      | UAT_CU_3.2 | webTest    | pl      | default_pl   | prisma_change_password@sharklasers.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$ | Alice.it0$!nuovo | prisma_change_password@sharklasers.com |

    @UAT_CU32_sk
    Examples: 
      | testIdJIRA | typeOfTest | country | driverConfig | email                                         | password   | authUsername | authPassword | password   | newPassword      | email                                  |
      | UAT_CU_3.2 | webTest    | sk      | default_sk   | e2eceprisma+automation_updatepwd_sk@gmail.com | Alice.it0$ | storefront   | BT2018       | Alice.it0$ | Alice.it0$!nuovo | prisma_change_password@sharklasers.com |

  @UAT_CU33 @sid:testIdJIRA
  Scenario Outline: Customer update info-Edit Personal Data
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And access to myprofile
    And edit personal data with following data
    Then check if the personal data is updated
    And close the browser and clean all resources

    @UAT_CU33_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                              | password   | authUsername | authPassword |
      | UAT_CU_3.3 | webTest    | default_ita  | ita     | prisma_update_info@sharklasers.com | Alice.it0$ | storefront   | BT2018       |

    @UAT_CU33_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                              | password   | authUsername | authPassword |
      | UAT_CU_3.3 | webTest    | default_pl   | pl      | prisma_update_info@sharklasers.com | Alice.it0$ | storefront   | BT2018       |

    @UAT_CU33_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                          | password   | authUsername | authPassword |
      | UAT_CU_3.3 | webTest    | default_sk   | sk      | e2eceprisma+automation_updateinfo_sk@gmail.com | Alice.it0$ | storefront   | BT2018       |

  @UAT_CU34 @sid:testIdJIRA
  Scenario Outline: Customer update info-Add new address
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And access to myprofile
    And add a new address
    Then check if the new address is correctly added
    And close the browser and clean all resources

    @UAT_CU34_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                              | password   | authUsername | authPassword |
      | UAT_CU_3.4 | webTest    | default_ita  | ita     | prisma_add_address@sharklasers.com | Alice.it0$ | storefront   | BT2018       |

    @UAT_CU34_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                              | password   | authUsername | authPassword |
      | UAT_CU_3.4 | webTest    | default_pl   | pl      | prisma_add_address@sharklasers.com | Alice.it0$ | storefront   | BT2018       |

    @UAT_CU34_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email                                             | password   | authUsername | authPassword |
      | UAT_CU_3.4 | webTest    | default_sk   | sk      | e2eceprisma+automation_updateaddress_sk@gmail.com | Alice.it0$ | storefront   | BT2018       |

  @UAT_CU43 @sid:testIdJIRA
  Scenario Outline: Customer update info-use CRM UI
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When login to the web CRM with following credential
      | country   | email   | password   |
      | <country> | <email> | <password> |
    And goto customer section
    And select an user
      | nome   | cognome   | email   | cellulare   | numeroCarta   | codiceCliente   |
      | <nome> | <cognome> | <email> | <cellulare> | <numeroCarta> | <codiceCliente> |
    And update the customer info
    And save the modify
    And The user log in to BATA IT site with credential
      | country   | email   | password      | authUsername   | authPassword   |
      | <country> | <email> | <passwordWeb> | <authUsername> | <authPassword> |
    And access to myprofile
    Then check if the personal data is updated
    And close the browser and clean all resources

    @UAT_CU43_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email  | password | nome | cognome | email | cellulare | numeroCarta | codiceCliente | authUsername | authPassword | passwordWeb |
      | UAT_CU_4.3 | webTest    | default_ita  | ita     | PRISMA | PRISMA   |      |         |       |           |             |    1000564527 | storefront   | BT2018       | Alice.it0$  |

    @UAT_CU43_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email  | password | nome | cognome | email | cellulare | numeroCarta | codiceCliente | authUsername | authPassword | passwordWeb |
      | UAT_CU_4.3 | webTest    | default_pl   | pl      | PRISMA | PRISMA   |      |         |       |           |             |    1000564527 | storefront   | BT2018       | Alice.it0$  |

    @UAT_CU43_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | email  | password | nome | cognome | email | cellulare | numeroCarta | codiceCliente | authUsername | authPassword | passwordWeb |
      | UAT_CU_4.3 | webTest    | default_sk   | sk      | PRISMA | PRISMA   |      |         |       |           |             |    1000564527 | storefront   | BT2018       | Alice.it0$  |
