@UAT_PS
Feature: Product Search

  @UAT_PS1 @sid:testIdJIRA
  Scenario Outline: Make a search by name
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name with search bar
      | productName   |
      | <productName> |
    And check that the results showed are six and click on show more
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @UAT_PS1_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName       |
      | UAT_PS-6.1 | webTest    | default_ita  | ita     | storefront   | BT2018       | sneakers in pelle |

    @UAT_PS1_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      |
      | UAT_PS-6.1 | webTest    | default_cz   | cz      | storefront   | BT2018       | boty na podpatku |

    @UAT_PS1_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName       |
      | UAT_PS-6.1 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | sneakers in pelle |

    @UAT_PS1_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      |
      | UAT_PS-6.1 | webTest    | default_pl   | pl      | storefront   | BT2018       | skórzane trampki |

    @UAT_PS1_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      |
      | UAT_PS-6.1 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | skórzane trampki |

    @UAT_PS1_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      |
      | UAT_PS-6.1 | webTest    | default_sk   | sk      | storefront   | BT2018       | členkové topánky |

    @UAT_PS1_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      |
      | UAT_PS-6.1 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | členkové topánky |

  @UAT_PS2 @sid:testIdJIRA
  Scenario Outline: Make a search by name  gender
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name + gender with search bar
      | productName   | gender   |
      | <productName> | <gender> |
    And check that the results showed are six and click on show more
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @UAT_PS2_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName       | gender |
      | UAT_PS-6.2 | webTest    | default_ita  | ita     | storefront   | BT2018       | sneakers in pelle | uomo   |

    @UAT_PS2_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | gender |
      | UAT_PS-6.2 | webTest    | default_cz   | cz      | storefront   | BT2018       | boty na podpatku | boty   |

    @UAT_PS2_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName       | gender |
      | UAT_PS-6.2 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | sneakers in pelle | uomo   |

    @UAT_PS2_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | gender    |
      | UAT_PS-6.2 | webTest    | default_pl   | pl      | storefront   | BT2018       | skórzane trampki | mężczyzna |

    @UAT_PS2_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | gender    |
      | UAT_PS-6.2 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | skórzane trampki | mężczyzna |

    @UAT_PS2_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | gender    |
      | UAT_PS-6.2 | webTest    | default_sk   | sk      | storefront   | BT2018       | členkové topánky | pre mužov |

    @UAT_PS2_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | gender    |
      | UAT_PS-6.2 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | členkové topánky | pre mužov |

  @UAT_PS3 @sid:testIdJIRA
  Scenario Outline: Make a search by name  color
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name + color with search bar
      | productName   | color   |
      | <productName> | <color> |
    And check that the results showed are six and click on show more
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @UAT_PS3_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName       | color |
      | UAT_PS-6.3 | webTest    | default_ita  | ita     | storefront   | BT2018       | sneakers in pelle | nere  |

    @UAT_PS3_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color |
      | UAT_PS-6.3 | webTest    | default_cz   | cz      | storefront   | BT2018       | boty na podpatku | černé |

    @UAT_PS3_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName       | color |
      | UAT_PS-6.3 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | sneakers in pelle | nere  |

    @UAT_PS3_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color  |
      | UAT_PS-6.3 | webTest    | default_pl   | pl      | storefront   | BT2018       | skórzane trampki | czarny |

    @UAT_PS3_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | color  |
      | UAT_PS-6.3 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | skórzane trampki | czarny |

    @UAT_PS3_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color  |
      | UAT_PS-6.3 | webTest    | default_sk   | sk      | storefront   | BT2018       | členkové topánky | čierna |

    @UAT_PS3_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | color  |
      | UAT_PS-6.3 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | členkové topánky | čierna |

  @UAT_PS4 @sid:testIdJIRA
  Scenario Outline: Make a search by name  color  gender
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name + color + gender with search bar
      | productName   | color   | gender   |
      | <productName> | <color> | <gender> |
    And check that the results showed are six and click on show more
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @UAT_PS4_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName       | color | gender |
      | UAT_PS-6.4 | webTest    | default_ita  | ita     | storefront   | BT2018       | sneakers in pelle | nere  | uomo   |

    @UAT_PS4_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color | gender |
      | UAT_PS-6.4 | webTest    | default_cz   | cz      | storefront   | BT2018       | boty na podpatku | černé | boty   |

    @UAT_PS4_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName       | color | gender |
      | UAT_PS-6.4 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | sneakers in pelle | nere  | uomo   |

    @UAT_PS4_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color  | gender    |
      | UAT_PS-6.4 | webTest    | default_pl   | pl      | storefront   | BT2018       | skórzane trampki | czarny | mężczyzna |

    @UAT_PS4_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | color  | gender    |
      | UAT_PS-6.4 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | skórzane trampki | czarny | mężczyzna |

    @UAT_PS4_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color  | gender    |
      | UAT_PS-6.4 | webTest    | default_sk   | sk      | storefront   | BT2018       | členkové topánky | čierna | pre mužov |

    @UAT_PS4_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | color  | gender    |
      | UAT_PS-6.4 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | členkové topánky | čierna | pre mužov |

  @UAT_PS5 @sid:testIdJIRA
  Scenario Outline: Make a search by name  color  gender  material
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by name + color + gender with search bar
      | productName   | brand   | color   | gender   | material   |
      | <productName> | <brand> | <color> | <gender> | <material> |
    And check that the results showed are six and click on show more
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @UAT_PS5_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName       | color | gender | brand | color | material |
      | UAT_PS-6.4 | webTest    | default_ita  | ita     | storefront   | BT2018       | sneakers in pelle | nere  | uomo   | nike  | nere  | camoscio |

    @UAT_PS5_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color | gender | brand | color | material |
      | UAT_PS-6.5 | webTest    | default_cz   | cz      | storefront   | BT2018       | boty na podpatku | černé | boty   | nike  | crna  | antilop  |

    @UAT_PS5_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName       | color | gender | brand | color | material |
      | UAT_PS-6.5 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | sneakers in pelle | nere  | uomo   | nike  | nere  | camoscio |

    @UAT_PS5_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color  | gender    | brand | color  | material |
      | UAT_PS-6.5 | webTest    | default_pl   | pl      | storefront   | BT2018       | skórzane trampki | czarny | mężczyzna | nike  | czarny | zamsz    |

    @UAT_PS5_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | color  | gender    | brand | color  | material |
      | UAT_PS-6.5 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | skórzane trampki | czarny | mężczyzna | nike  | czarny | zamsz    |

    @UAT_PS5_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName      | color  | gender    | brand | color  | material |
      | UAT_PS-6.5 | webTest    | default_sk   | sk      | storefront   | BT2018       | členkové topánky | čierna | pre mužov | nike  | čierna | semiš    |

    @UAT_PS5_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName      | color  | gender    | brand | color  | material |
      | UAT_PS-6.5 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | členkové topánky | čierna | pre mužov | nike  | čierna | semiš    |

  @UAT_PS6 @sid:testIdJIRA
  Scenario Outline: Make a search by BataID
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item by bataID with search bar
      | bataID   |
      | <bataID> |
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @UAT_PS6_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | bataID     |
      | UAT_PS-6.6 | webTest    | default_ita  | ita     | storefront   | BT2018       | 170R_80112 |

    @UAT_PS6_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | bataID      |
      | UAT_PS-6.6 | webTest    | default_cz   | cz      | storefront   | BT2018       | 170R__50368 |

    @UAT_PS6_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | authUsername | authPassword | bataID     |
      | UAT_PS-6.6 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | 170R_80112 |

    @UAT_PS6_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | bataID     |
      | UAT_PS-6.6 | webTest    | default_pl   | pl      | storefront   | BT2018       | 170R_80112 |

    @UAT_PS6_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | bataID     |
      | UAT_PS-6.6 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | 170R_80112 |

    @UAT_PS6_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | bataID      |
      | UAT_PS-6.6 | webTest    | default_sk   | sk      | storefront   | BT2018       | 170R__79960 |

    @UAT_PS6_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | bataID      |
      | UAT_PS-6.6 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | 170R__79960 |

  @UAT_PS20 @sid:testIdJIRA
  Scenario Outline: Make a search and check category of results
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item and check category of results
      | brand   | categoryName   |
      | <brand> | <categoryName> |
    Then check that the user land on Searching page filtered
    And close the browser and clean all resources

    @UAT_PS20_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | brand | categoryName |
      | UAT_PS-6.20 | webTest    | default_ita  | ita     | storefront   | BT2018       | nike  | Uomo      |

    @UAT_PS20_cz
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | brand | categoryName |
      | UAT_PS-6.20 | webTest    | default_cz   | cz      | storefront   | BT2018       | nike  | Djeco        |

    @UAT_PS20_mobileIta
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig      | country | authUsername | authPassword | brand | categoryName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | nike  | Bambini      |

    @UAT_PS20_pl
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | brand | categoryName |
      | UAT_PS-6.20 | webTest    | default_pl   | pl      | storefront   | BT2018       | nike  | Dzieci       |

    @UAT_PS20_mobilePl
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | brand | categoryName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | nike  | Dzieci       |

    @UAT_PS20_sk
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | brand | categoryName |
      | UAT_PS-6.20 | webTest    | default_sk   | sk      | storefront   | BT2018       | nike  | Deti         |

    @UAT_PS20_mobileSk
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | brand | categoryName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | nike  | Deti         |

  @UAT_PS9 @sid:testIdJIRA
  Scenario Outline: Product search-No results scenario
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    Then the Sorry, we did not find any products message and other popular searches are displayed
    And close the browser and clean all resources

    @UAT_PS9_ita
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.9 | webTest    | default_ita  | ita     | storefront   | BT2018       | camicia     |

    @UAT_PS9_cz
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.9 | webTest    | default_cz   | cz      | storefront   | BT2018       | camicia     |

    @UAT_PS9_mobileIta
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName |
      | UAT_PS-6.9 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | camicia     |

    @UAT_PS9_pl
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.9 | webTest    | default_pl   | pl      | storefront   | BT2018       | camicia     |

    @UAT_PS9_mobilePl
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.9 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | camicia     |

    @UAT_PS9_sk
    Examples: 
      | testIdJIRA | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.9 | webTest    | default_sk   | sk      | storefront   | BT2018       | camicia     |

    @UAT_PS9_mobileSk
    Examples: 
      | testIdJIRA | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.9 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | camicica    |

  @UAT_PS10 @sid:testIdJIRA
  Scenario Outline: Product search-Misspelling query
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    Then Are you searching for Suggestions is displayed
    And click on suggestion link
    Then check that the user land on Searching page
    And close the browser and clean all resources

    @UAT_PS10_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.10 | webTest    | default_ita  | ita     | storefront   | BT2018       | polacchio   |

    @UAT_PS10_cz
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.10 | webTest    | default_cz   | cz      | storefront   | BT2018       | polob       |

    @UAT_PS10_mobileIta
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName |
      | UAT_PS-6.10 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | polacchio   |

    @UAT_PS10_pl
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.10 | webTest    | default_pl   | pl      | storefront   | BT2018       | botk        |

    @UAT_PS10_mobilePl
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.10 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | botk        |

    @UAT_PS10_sk
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName    |
      | UAT_PS-6.10 | webTest    | default_sk   | sk      | storefront   | BT2018       | členkové topán |

    @UAT_PS10_mobileSk
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName    |
      | UAT_PS-6.10 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | členkové topán |

  @UAT_PS11 @sid:testIdJIRA
  Scenario Outline: Product search-Refine search - Categories
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    And click on show more link
    Then check that the user land on Searching page filtered
    And close the browser and clean all resources

    @UAT_PS11_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   |

    @UAT_PS11_cz
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   |

    @UAT_PS11_mobileIta
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   |

    @UAT_PS11_pl
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     |

    @UAT_PS11_mobilePl
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     |

    @UAT_PS11_sk
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     |

    @UAT_PS11_mobileSk
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     |

  @UAT_PS12 @sid:testIdJIRA
  Scenario Outline: Product search-Refine search by filters
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    And click on show more link
    And check that the user land on Searching page filtered
    And filter results by
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And check the resulting page by applied filter
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And remove selected filter
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And check the resulting page by removed filter
      | filterKey   | filterValue   |
      | <filterKey> | <filterValue> |
    And close the browser and clean all resources

    @UAT_PS12_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName | filterKey | filterValue |
      | UAT_PS-6.12 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | size      |             |
      | UAT_PS-6.13 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | price     |             |
      | UAT_PS-6.14 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | material  |             |
      | UAT_PS-6.15 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | color     |             |
      | UAT_PS-6.16 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | brand     |             |

    @UAT_PS12_cz
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName | filterKey | filterValue |
      | UAT_PS-6.12 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | size      |             |
      | UAT_PS-6.13 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | price     |             |
      | UAT_PS-6.14 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | material  |             |
      | UAT_PS-6.15 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | color     |             |
      | UAT_PS-6.16 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | brand     |             |

    @UAT_PS12_mobileIta
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName | filterKey | filterValue |
      | UAT_PS-6.12 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | size      |             |
      | UAT_PS-6.13 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | price     |             |
      | UAT_PS-6.14 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | material  |             |
      | UAT_PS-6.15 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | color     |             |
      | UAT_PS-6.16 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | brand     |             |

    @UAT_PS12_pl
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName | filterKey | filterValue |
      | UAT_PS-6.12 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | size      |             |
      | UAT_PS-6.13 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | price     |             |
      | UAT_PS-6.14 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | material  |             |
      | UAT_PS-6.15 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | color     |             |
      | UAT_PS-6.16 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | brand     |             |

    @UAT_PS12_mobilePl
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName | filterKey | filterValue |
      | UAT_PS-6.12 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | size      |             |
      | UAT_PS-6.13 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | price     |             |
      | UAT_PS-6.14 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | material  |             |
      | UAT_PS-6.15 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | color     |             |
      | UAT_PS-6.16 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | brand     |             |

    @UAT_PS12_sk
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName | filterKey | filterValue |
      | UAT_PS-6.12 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | size      |             |
      | UAT_PS-6.13 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | price     |             |
      | UAT_PS-6.14 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | material  |             |
      | UAT_PS-6.15 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | color     |             |
      | UAT_PS-6.16 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | brand     |             |

    @UAT_PS12_mobileSk
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName | filterKey | filterValue |
      | UAT_PS-6.12 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | size      |             |
      | UAT_PS-6.13 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | price     |             |
      | UAT_PS-6.14 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | material  |             |
      | UAT_PS-6.15 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | color     |             |
      | UAT_PS-6.16 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | brand     |             |

  @UAT_PS17 @sid:testIdJIRA
  Scenario Outline: Product search-Sort result
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    And click on show more link
    And check that the user land on Searching page filtered
    And click on order filter
      | orderFilter   |
      | <orderFilter> |
    And check the resulting page after ordering list
      | orderFilter   |
      | <orderFilter> |
    And close the browser and clean all resources

    @UAT_PS17_ita
    Examples: 
      | testIdJIRA    | typeOfTest | driverConfig | country | authUsername | authPassword | productName | orderFilter       |
      | UAT_PS-6.17-1 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | recommended       |
      | UAT_PS-6.17-2 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | new-in            |
      | UAT_PS-6.17-3 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | top-sellers       |
      | UAT_PS-6.17-4 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | price-low-to-high |
      | UAT_PS-6.17-5 | webTest    | default_ita  | ita     | storefront   | BT2018       | cerimonia   | price-high-to-low |

    @UAT_PS17_cz
    Examples: 
      | testIdJIRA    | typeOfTest | driverConfig | country | authUsername | authPassword | productName | orderFilter       |
      | UAT_PS-6.17-1 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | recommended       |
      | UAT_PS-6.17-2 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | new-in            |
      | UAT_PS-6.17-3 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | top-sellers       |
      | UAT_PS-6.17-4 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | price-low-to-high |
      | UAT_PS-6.17-5 | webTest    | default_cz   | cz      | storefront   | BT2018       | ELEGANTNÍ   | price-high-to-low |

    @UAT_PS17_mobileIta
    Examples: 
      | testIdJIRA    | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName | orderFilter       |
      | UAT_PS-6.17-1 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | recommended       |
      | UAT_PS-6.17-2 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | new-in            |
      | UAT_PS-6.17-3 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | top-sellers       |
      | UAT_PS-6.17-4 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | price-low-to-high |
      | UAT_PS-6.17-5 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | cerimonia   | price-high-to-low |

    @UAT_PS17_pl
    Examples: 
      | testIdJIRA    | typeOfTest | driverConfig | country | authUsername | authPassword | productName | orderFilter       |
      | UAT_PS-6.17-1 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | recommended       |
      | UAT_PS-6.17-2 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | new-in            |
      | UAT_PS-6.17-3 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | top-sellers       |
      | UAT_PS-6.17-4 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | price-low-to-high |
      | UAT_PS-6.17-5 | webTest    | default_pl   | pl      | storefront   | BT2018       | kobiety     | price-high-to-low |

    @UAT_PS17_mobilePl
    Examples: 
      | testIdJIRA    | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName | orderFilter       |
      | UAT_PS-6.17-1 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | recommended       |
      | UAT_PS-6.17-2 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | new-in            |
      | UAT_PS-6.17-3 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | top-sellers       |
      | UAT_PS-6.17-4 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | price-low-to-high |
      | UAT_PS-6.17-5 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | kobiety     | price-high-to-low |

    @UAT_PS17_sk
    Examples: 
      | testIdJIRA    | typeOfTest | driverConfig | country | authUsername | authPassword | productName | orderFilter       |
      | UAT_PS-6.17-1 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | recommended       |
      | UAT_PS-6.17-2 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | new-in            |
      | UAT_PS-6.17-3 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | top-sellers       |
      | UAT_PS-6.17-4 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | price-low-to-high |
      | UAT_PS-6.17-5 | webTest    | default_sk   | sk      | storefront   | BT2018       | tenisky     | price-high-to-low |

    @UAT_PS17_mobileSk
    Examples: 
      | testIdJIRA    | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName | orderFilter       |
      | UAT_PS-6.17-1 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | recommended       |
      | UAT_PS-6.17-2 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | new-in            |
      | UAT_PS-6.17-3 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | top-sellers       |
      | UAT_PS-6.17-4 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | price-low-to-high |
      | UAT_PS-6.17-5 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | tenisky     | price-high-to-low |

  @UAT_PS18 @sid:testIdJIRA
  Scenario Outline: Product search-Popular Search
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    And click on popular search item
    Then check that the user land on Searching page filtered
    And close the browser and clean all resources

    @UAT_PS18_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.18 | webTest    | default_ita  | ita     | storefront   | BT2018       | nike        |

    @UAT_PS18_cz
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.18 | webTest    | default_cz   | cz      | storefront   | BT2018       | nike        |

    @UAT_PS18_mobileIta
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName |
      | UAT_PS-6.18 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | nike        |

    @UAT_PS18_pl
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.18 | webTest    | default_pl   | pl      | storefront   | BT2018       | nike        |

    @UAT_PS18_mobilePl
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.18 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | nike        |

    @UAT_PS18_sk
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.18 | webTest    | default_sk   | sk      | storefront   | BT2018       | nike        |

    @UAT_PS18_mobileSk
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.18 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | nike        |

  @UAT_PS19 @sid:testIdJIRA
  Scenario Outline: Product search-Popular Search
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA IT site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When User search an item
      | productName   |
      | <productName> |
    And click on popular search item
    Then check that the user land on Searching page filtered
    And close the browser and clean all resources

    @UAT_PS19_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_ita  | ita     | storefront   | BT2018       | nike        |

    @UAT_PS19_cz
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_cz   | cz      | storefront   | BT2018       | nike        |

    @UAT_PS19_mobileIta
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig      | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_ita | ita     | storefront   | BT2018       | nike        |

    @UAT_PS19_pl
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_pl   | pl      | storefront   | BT2018       | nike        |

    @UAT_PS19_mobilePl
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_pl | pl      | storefront   | BT2018       | nike        |

    @UAT_PS19_sk
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | webTest    | default_sk   | sk      | storefront   | BT2018       | nike        |

    @UAT_PS19_mobileSk
    Examples: 
      | testIdJIRA  | typeOfTest  | driverConfig     | country | authUsername | authPassword | productName |
      | UAT_PS-6.20 | androidTest | mobilebrowser_sk | sk      | storefront   | BT2018       | nike        |
