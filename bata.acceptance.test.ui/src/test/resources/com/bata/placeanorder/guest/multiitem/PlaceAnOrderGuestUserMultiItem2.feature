Feature: UAT_OD_GMI: Place an order Guest User Multi Item

##################################################################
#COUNTRY ITA
##################################################################
@ita
  Scenario Outline: Place an order no promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | mocassino   |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.3 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
 @ita
  Scenario Outline: Place an order promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @ita
  Scenario Outline: Place an order promo for guest user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
  
  @ita
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
  
  @ita
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  @ita
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  ##################################################################
  #COUNTRY ITA MOBILE
  ##################################################################
  @ita @mobile
  Scenario Outline: Place an order no promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | mocassino   |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.3 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 

  @ita @mobile
  Scenario Outline: Place an order promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for guest user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
  
  @ita @mobile
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  ##################################################################
  #COUNTRY PL
  ##################################################################
  @pl
  Scenario Outline: Place an order no promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName                | quantity | size | color | 
      | PIKOWANE TRAMPKI ZA KOSTKĘ |          |      |       | 
      | 5411600                    |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.2 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.3 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @pl
  Scenario Outline: Place an order promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.7  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.8  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @pl
  Scenario Outline: Place an order promo for guest user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
  
  @pl
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
  
  @pl
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  @pl
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  ##################################################################
  #COUNTRY PL MOBILE
  ##################################################################
  @pl @mobile
  Scenario Outline: Place an order no promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName                | quantity | size | color | 
      | PIKOWANE TRAMPKI ZA KOSTKĘ |          |      |       | 
      | 5411600                    |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.2 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.3 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
@pl @mobile
  Scenario Outline: Place an order promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.7  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.8  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for guest user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
  
  @pl @mobile
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        |
      
      
        ##################################################################
  #COUNTRY SK
  ##################################################################
  @sk
  Scenario Outline: Place an order no promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName                | quantity | size | color | 
      | PIKOWANE TRAMPKI ZA KOSTKĘ |          |      |       | 
      | 5411600                    |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.2 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.3 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @sk
  Scenario Outline: Place an order promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.7  | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.8  | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @sk
  Scenario Outline: Place an order promo for guest user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
  
  @sk
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
  
  @sk
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  @sk
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  ##################################################################
  #COUNTRY SK  MOBILE
  ##################################################################
  @sk @mobile
  Scenario Outline: Place an order no promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName                | quantity | size | color | 
      | PIKOWANE TRAMPKI ZA KOSTKĘ |          |      |       | 
      | 5411600                    |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.2 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.3 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.5 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
@sk @mobile
  Scenario Outline: Place an order promo for guest user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GMI-11.7  | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.8  | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GMI-11.10 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @sk @mobile
  Scenario Outline: Place an order promo for guest user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
  
  @sk @mobile
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 
  
  @sk @mobile
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.17 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.19 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.41 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.44 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.53 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.56 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
  
  @sk @mobile
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GMI-11.21 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.24 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.45 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.48 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.57 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.60 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.65 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | 
      | storefront   | BT2018       | UAT_OD_GMI-11.68 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        |  