Feature: UAT_OD_GSI: Place an order Guest User Single Item

############################################################
#COUNTRY ITA
############################################################
@ita
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest |  driverConfig |driverMailConfig|
      | <testIdJIRA> | webTest    | default|gmail|
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.1   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.2   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.3   | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  #

  @ita
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.11 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.12 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.13 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @ita
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.25 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.44 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.45 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.46 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.48 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.49 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.56 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.57 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.60   | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.61   | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146           | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146           | 
      | storefront | BT2018 | UAT_OD_GSI-10.69   | ita |          |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146           | 
  
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @ita
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.42 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.54 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  #  | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  
  @ita
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.42 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.54 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  #    | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 |
  @ita
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA         | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.22 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.3 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.4 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.5 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.6 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.46 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.7 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.8 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.9  | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.10 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.58 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.11 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.12 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.13 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.14 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.15 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.16 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  ############################################################
  #COUNTRY CZ
  ############################################################
  @cz
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | driverConfig |driverMailConfig|
      | <testIdJIRA> | webTest    | default | gmail |
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.2 | cz      |          |         | 4056070000000008 | 200 | test prisma |               12 |              20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha | Napoli   | Italia  | card      | Přijali jsme Vaši objednávku č. ${orderNumber} |
      #| storefront   | BT2018       | UAT_OD_GSI-10.2   | cz      |          |         | 4056070000000008    | 200 | test prisma | 12               | 20              | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com        | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha  | Napoli   | Italia  | card        | Přijali jsme Vaši objednávku č. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.3   | cz      |          |         | 4056070000000008    | 200 | test prisma | 12               | 20              | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha  | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  #
  @cz
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.11 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.12 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.13 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @cz
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.21 | cz      |        |         | 4056070000000008    | 200 | test prisma | 12               | 20              | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha  | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.25 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | !geoip! | 
      | storefront | BT2018 | UAT_OD_GSI-10.24 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | !geoip! | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.45 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.46 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.49 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.48 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.57 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.58 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | via orefici, 22 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.61 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @cz
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.18 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.42 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.54 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  
  @cz
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.42 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.54 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  @cz
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA         | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.22 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.3 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.4 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.5 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.6 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.46 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.7 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.8 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.9  | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.10 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.11 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.12 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.13 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.14 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.15 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.16 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  
    ############################################################
  #COUNTRY CZ MOBILE
  ############################################################
  @cz @mobile
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.1   | cz      |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha  | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.2   | cz      |          |         | 4056070000000008    | 200 | test prisma | 12               | 20              | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com        | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha  | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.3   | cz      |          |         | 4056070000000008    | 200 | test prisma | 12               | 20              | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha  | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @cz @mobile
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.11 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.12 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.13 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @cz @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142  | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.21 | cz      |        |         | 4056070000000008    | 200 | test prisma | 12               | 20              | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 109 00 | Praha  | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.25 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | !geoip! | 
      | storefront | BT2018 | UAT_OD_GSI-10.24 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | !geoip! | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.45 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.46 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.49 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.48 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.57 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.58 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | cod  | Ordine n. ${orderNumber} | via orefici, 22 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.61 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @cz @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.18 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.42 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.54 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  
  @cz @mobile
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.42 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.54 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.2 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  @cz @mobile
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA         | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.22 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.3 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.4 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.5 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.6 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.46 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.7 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.8 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.9  | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.10 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.11 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.12 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.13 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.14 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.15 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.16 | ita | prisma10 |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  
  ###########################################################
  #COUNTRY ITA MOBILE
  ###########################################################
  @ita @mobile
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | paypal      | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.3 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | cod         | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.5.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.5.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.5.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  
  @ita @mobile
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.11 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.12 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.13 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 |  tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento    | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.25 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.45 | ita |  |  | 4988 4388 4388 4305 | 737 | test prisma | Ottobre | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma | Via Picone n. 1 | 92100 | Agrigento | Agrigento | Italia | card | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.46 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.49 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 |  tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |           20146 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |
  
  @ita @mobile
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.68.1 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.68.2 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  @ita @mobile
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.3 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.4 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.5 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.6 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.7 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.8 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.9 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.10 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.11 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.12 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.13 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.14 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.15 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.16 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 80142 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  ############################################################
  #COUNTRY PL
  ############################################################
  @pl
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | driverConfig |driverMailConfig|
      | <testIdJIRA> | webTest    | default | gmail |
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | nike     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.2   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Poland | Napoli   | Italia  | card        | Otrzymaliśmy Twoje zamówienie nr ${orderNumber}. |
      #| storefront   | BT2018       | UAT_OD_GSI-10.3   | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.3 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  #
  @pl
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.7  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.8  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.12 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.13 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @pl
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.25 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.44 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.45 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.46 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.48 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.49 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.56 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.57 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.60   | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.61   | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146           | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146           | 
      | storefront | BT2018 | UAT_OD_GSI-10.69   | pl |          |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146           | 
  
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @pl
  Scenario Outline: Place an order no promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.19 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.42 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.44 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.54 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.56 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  #  | storefront   | BT2018       | UAT_OD_GSI-10.68 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  
  @pl
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.19 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.42 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.44 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.54 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.56 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.66 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.2 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  #    | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 |
  @pl
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA         | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.2 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.22 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.3 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.4 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.24 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.5 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.6 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.46 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.7 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.8 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.48 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.9  | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.10 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.58 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.11 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.12 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.13 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.14 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.15 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.16 | pl | prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  ###########################################################
  #COUNTRY PL MOBILE
  ###########################################################
  @pl @mobile
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.2 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.3 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | cod         | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.5.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.5.3 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  
  @pl @mobile
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.7  | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.8  | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.12 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.13 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.21 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 |  tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento    | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.25 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.45 | pl |  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma | Via Picone n. 1 | 92100 | Agrigento | Agrigento | Italia | card | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.46 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.49 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 |  tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.57 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.17 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.41 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.53 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.65 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |
  
  @pl @mobile
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.17 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.41 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.53 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.65 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.68.2 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  @pl @mobile
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.2 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.4 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.6 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.8 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.10 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.12 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.14 | pl     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.16 | pl     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |

    
  ############################################################
  #COUNTRY SK
  ############################################################
  @sk
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest |driverConfig|driverMailConfig|
      | <testIdJIRA> | webTest    | default|gmail|
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | nike     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA        | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       |UAT_OD_GSI-10.2   | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | Október          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 123 45 | Slovacchia | Napoli   | Italia  | card        | Prijali sme Vašu objednávku č. ${orderNumber} |
      #| storefront   | BT2018       | UAT_OD_GSI-10.3   | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      #| storefront   | BT2018       | UAT_OD_GSI-10.5.3 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @sk
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OD_GSI-10.7  | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.8  | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.12 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OD_GSI-10.13 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | 
  
  @sk
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.21 | sk     |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
      | storefront   | BT2018       | UAT_OD_GSI-10.25 | sk     | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.44 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.45 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.46 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.48 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.49 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.56 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.57 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.60   | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.61   | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146           | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146           | 
      | storefront | BT2018 | UAT_OD_GSI-10.69   | sk|          |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146           | 
  
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @sk
  Scenario Outline: Place an order no promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | sk     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.19 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.42 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.44 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.54 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.56 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  #  | storefront   | BT2018       | UAT_OD_GSI-10.68 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |
  
  @sk
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA       | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.17 | sk     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.18 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.19.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.19.2 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.19 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.40 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.41 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.42 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.44.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.44.2 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.44 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.52 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.53 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.54 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.56.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.56.2 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.56 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.64 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.65 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  #  | storefront   | BT2018       | UAT_OD_GSI-10.66 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.68.1 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.68.2 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  #    | testIdJIRA | sk    |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 |
  @sk
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA         | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore | newDataStore | 
      | storefront   | BT2018       | UAT_OD_GSI-10.76.2 | sk     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!   | 37036        | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.22 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.3 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | !geoip! | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.4 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | !geoip! | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.24 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.5 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.6 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.46 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.7 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | Roma | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.8 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | Roma | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.48 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.9  | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.10 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.58 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.11 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.12 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | via orefici, 22 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.60 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.13 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.14 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.66 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
      | storefront | BT2018 | UAT_OD_GSI-10.76.15 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | paypal | Ordine n. ${orderNumber} | 20146 | 37036 | 
      | storefront | BT2018 | UAT_OD_GSI-10.76.16 | sk| prisma10 |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli | Italia | card   | Ordine n. ${orderNumber} | 20146 | 37036 | 
  # | storefront   | BT2018       | UAT_OD_GSI-10.68 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  ###########################################################
  #COUNTRY SK MOBILE
  ###########################################################
  @sk @mobile
  Scenario Outline: Place an order no promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.2 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.3 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | cod         | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.5.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.5.3 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  
  @sk @mobile
  Scenario Outline: Place an order promo for guest user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.7  | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.8  | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.12 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OD_GSI-10.13 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} |
  
  @sk @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.21 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 |  tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento    | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.25 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.24 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | !geoip!         |
      | storefront | BT2018 | UAT_OD_GSI-10.45 | sk|  |  | 4111 1111 1111 1111 | 737 | test prisma | październik | 2020 | nicola.cora.buyer@bata.com | chu5ePha | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 | tester | prisma | Via Picone n. 1 | 92100 | Agrigento | Agrigento | Italia | card | Ordine n. ${orderNumber} | Roma | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.46 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.49 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 393333333333 |  tester | prisma  | Via Picone n. 1 | 92100 | Agrigento | Agrigento   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.48 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.57 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.58 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | via orefici, 22 |
  
  @sk @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     When check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.17 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |
  #| storefront   | BT2018       | UAT_OD_GSI-10.41 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |
  #| storefront   | BT2018       | UAT_OD_GSI-10.53 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.65 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |
  
  @sk @mobile
  Scenario Outline: Place an order no promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.17 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.19.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.41 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.44.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.53 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.56.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.65 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.68.2 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  
  @sk @mobile
  Scenario Outline: Place an order promo for guest user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
     When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | 
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.2 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.4 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.6 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.8 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.10 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.12 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.14 | sk    |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  #| storefront   | BT2018       | UAT_OD_GSI-10.76.16 | sk    | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3333333333 | tester | prisma  | Piazza Giuseppe Garibaldi n. 34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        37036 |
  