Feature: UAT_OD_RMI: Place an order Registered User Multi Item

###############################################################
#COUNTRY ITA
###############################################################
@ita
  Scenario Outline: Place an order no promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.3 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  #     | idid       | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | Francesco | Annunziata | via pantano gnilino n.2 | 84087 | Sarno | Salerno  | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
  #come sezione
  
  @ita
  Scenario Outline: Place an order promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  #      | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | email | password |
  
  @ita
  Scenario Outline: Place an order promo for registered user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  #      | idid       | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | Francesco | Annunziata | via pantano gnilino n.2 | 84087 | Sarno | Salerno  | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
  #    | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |     45011 | email | password |
  
  @ita
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  #    | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 | email | password |
  
  @ita
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  #    | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 | email | password |
  
  @ita
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  #      | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal        | Ordine n. ${orderNumber} |     45011 |37036|email | password |
  
  ###############################################################
  #COUNTRY ITA MOBILE
  ###############################################################
  @ita @mobile
  Scenario Outline: Place an order no promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.3 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      | 7711113     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  ###############################################################
  #COUNTRY PL
  ###############################################################
  @pl
  Scenario Outline: Place an order no promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.2 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.3 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  #     | idid       | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | Francesco | Annunziata | via pantano gnilino n.2 | 84087 | Sarno | Salerno  | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
  
  @pl
  Scenario Outline: Place an order promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.7  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.8  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  #      | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | email | password |
  
  @pl
  Scenario Outline: Place an order promo for registered user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  #    | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |     45011 | email | password |
  
  @pl
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  #    | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 | email | password |
  
  @pl
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  #      | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal        | Ordine n. ${orderNumber} |     45011 |37036|email | password |
  
  ###############################################################
  #COUNTRY PL MOBILE
  ###############################################################
  @pl @mobile
  Scenario Outline: Place an order no promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.2 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.3 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user home delivery multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.7  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.8  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | cod         | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based on Customer Logged in address / city entry / CAP / full address / GPS located - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - multi item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RMI-9.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RMI-9.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 