Feature: UAT_OD_RSI: Place an order Registered User Single Item

###########################################################################
#COUNTRY ITA
###########################################################################
@ita
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.3 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  #      | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | email | password |
  @ita
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  #      | idid       | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | Francesco | Annunziata | via pantano gnilino n.2 | 84087 | Sarno | Salerno  | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |
  #    | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} |     45011 | email | password |
  @ita
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  #    | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 | email | password |
  @ita
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  #    | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 | email | password |
  @ita
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  #      | testIdJIRA | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal        | Ordine n. ${orderNumber} |     45011 |37036|email | password |
  
  ###########################################################################
  #COUNTRY CZ
  ###########################################################################
  @cz
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.2 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.3 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.5 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.5 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  
  @cz
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.7  | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.8  | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.10 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.10 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  
  @cz
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.21 | cz     |          |         | 4056070000000008 | 200 | test prisma | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 109 00 | Praha | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.24 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.44 | cz     |          |         | 4056070000000008 | 200 | test prisma | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 109 00 | Praha | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.45 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.48 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.56 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.60 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.60 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  
  @cz
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.17 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.19 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.41 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.44 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.53 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.56 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.65 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.68 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #
  
  @cz
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.17 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.19 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.41 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.44 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.53 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.56 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.65 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.68 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #
  @cz
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.21 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.24 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.45 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.48 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.57 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.60 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.65 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.68 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #
  
    ###########################################################################
  #COUNTRY CZ MOBILE
  ###########################################################################
  @cz @mobile
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.2 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.3 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.5 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.5 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  
  @cz @mobile
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.7  | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.8  | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.10 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.10 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  
  @cz @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.21 | cz     |          |         | 4056070000000008 | 200 | test prisma | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 109 00 | Praha | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.24 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.44 | cz     |          |         | 4056070000000008 | 200 | test prisma | 12          |            20 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 109 00 | Praha | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.45 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.48 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.56 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.60 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.60 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  
  @cz @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.17 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.19 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.41 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.44 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.53 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.56 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.65 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.68 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #
  
  @cz @mobile
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.17 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.19 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.41 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.44 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.53 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.56 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.65 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.68 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #
  @cz @mobile
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | paymentType | emailSubject | dataStore | newDataStore | email | password | 
  #| storefront   | BT2018       | UAT_OD_RSI-8.21 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.24 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.45 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.48 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Brno            |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.57 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.60 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} | Spojovací 1346 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.65 | cz     |          |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #| storefront   | BT2018       | UAT_OD_RSI-8.68 | cz     | prisma10 |         | 4056070000000008 | 200 | Andrea prisma40 | 12          |            20  | nicola.cora.buyer@bata.com | chu5ePha       |  automation.test.prisma+cz.placeanorder@gmail.com | 3510660234 | Andrea | prisma40  | Piazza Giovanni Cavour n.33 | 109 00 | Praha| Roma   | Italia  | card        | Ordine n. ${orderNumber} |           20146 |        Znojmo | automation.test.prisma+cz.placeanorder@gmail.com | testPrisma1! |
  #
  
  ###########################################################################
  #COUNTRY ITA MOBILE
  ###########################################################################
  @ita @mobile
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.1 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.2 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.3 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.6  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.7  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.8  | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.16 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.40 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.52 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | sneakers     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.20 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.64 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | ita     |          |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | paypal      | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | ita     | prisma10 |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+it.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 00155 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 
  
  ###########################################################################
  #COUNTRY PL
  ###########################################################################
  @pl
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.2 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.3 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #come sezione
  @pl
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.7  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.8  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #      | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | email | password |
  @pl
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #    | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} |     45011 | email | password |
  @pl
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @pl
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #    | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 | email | password |
  @pl
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #      | testIdJIRA | pl     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | paypal        | Ordine n. ${orderNumber} |     45011 |37036|email | password |
  
  ###########################################################################
  #COUNTRY PL MOBILE
  ###########################################################################
  @pl @mobile
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.2 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.3 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.7  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.8  | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | pl      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | pl      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ |

 ###########################################################################
  #COUNTRY SK
  ###########################################################################
  @sk
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.2 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.3 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #come sezione
  @sk
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.7  | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.8  | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #      | testIdJIRA | sk     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | email | password |
  @sk
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #    | testIdJIRA | sk     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} |     45011 | email | password |
  @sk
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @sk
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #    | testIdJIRA | sk     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} |     45011 |        37036 | email | password |
  @sk
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  #      | testIdJIRA | sk     |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | paypal        | Ordine n. ${orderNumber} |     45011 |37036|email | password |
  
  ###########################################################################
  #COUNTRY SK MOBILE
  ###########################################################################
  @sk @mobile
  Scenario Outline: Place an order no promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA     | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.2 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.3 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.5 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @sk @mobile
  Scenario Outline: Place an order promo for registered user home delivery single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | home        | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.7  | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.8  | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.10 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | cod         | Ordine n. ${orderNumber} | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @sk @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @sk @mobile
  Scenario Outline: Place an order promo for registered user desire store based - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @sk @mobile
  Scenario Outline: Place an order no promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.17 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.19 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.41 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.44 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.53 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.56 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
  
  @sk @mobile
  Scenario Outline: Place an order promo for registered user desire store based on CAP and change store - single item
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "promo" and add it to cart
      | productName | quantity | size | color | 
      | xxx         |          |      |       | 
      And place an order to store and change with new store before payment
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | newDataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | <newDataStore> | 
      And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | 
      | store       | <emailAddress> | <emailSubject> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA      | country | coupon   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | dataStore       | newDataStore | email                                       | password   | 
      | storefront   | BT2018       | UAT_OD_RSI-8.21 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.24 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | !geoip!         | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.45 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.48 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | Roma            | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.57 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.60 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | via orefici, 22 | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.65 | sk      |          |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OD_RSI-8.68 | sk      | prisma10 |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | automation.test.prisma+pl.placeanorder@gmail.com | 3495047035 | tester | prisma  | Via Delia n. 23 | 12-345 | Roma | Roma   | Italia  | card        | Ordine n. ${orderNumber} | 20146           | 37036        | automation.test.prisma+pl.placeanorder@gmail.com | Alice.it0$ | 	  