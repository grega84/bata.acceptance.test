@UAT_CR
Feature: Customer registration
  Customer registration on MYACCOUNT page

  @UAT_CR11 @sid:testIdJIRA
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | gmail            |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   |
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> |
    Then The user will be redirect on the profile page
    And The user check the received email
    | driverMailConfig |
    | gmail            |
    And close the browser and clean all resources

    @UAT_CR11_ita
    Examples: 
      | authUsername | authPassword | testIdJIRA | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.1 | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |       |      01 | Gennaio   |     1950 | webTest    | default_ita  |

    @UAT_CR11_cz
    Examples: 
      | authUsername | authPassword | testIdJIRA | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.1 | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |       |      01 | Gennaio   |     1950 | webTest    | default_cz   |

    @UAT_CR11_mobileCz
    Examples: 
      | authUsername | authPassword | testIdJIRA | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone | dayBorn | monthBorn | yearBorn | typeOfTest  | driverConfig     |
      | storefront   | BT2018       | UAT_CR-1.1 | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |       |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_cz |

    @UAT_CR11_pl
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |       |      01 | maj       |     1950 | webTest    | default_pl   |

    @UAT_CR11_mobilePl
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone | dayBorn | monthBorn | yearBorn | typeOfTest  | driverConfig     |
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |       |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_pl |

    @UAT_CR11_sk
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |       |      01 | Gennaio   |     1950 | webTest    | default_sk   |

    @UAT_CR11_mobileSk
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone | dayBorn | monthBorn | yearBorn | typeOfTest  | driverConfig     |
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |       |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_sk |

  @UAT_CR12 @sid:testIdJIRA
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   |
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> |
    Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   |
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> |

    @UAT_CR12_ita
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                              | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | test0001prismabata@sharklasers.com | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 | webTest    | default_ita  |
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                    | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |        323 |      01 | Gennaio   |     1950 | webTest    | default_ita  |

    @UAT_CR12_cz
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                   | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | prisma_customerlogin_cz@sharklasers.com | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 | webTest    | default_cz   |
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                         | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |        323 |      20 | Giugno    |     2001 | webTest    | default_cz   |

    @UAT_CR12_mobileIta
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                              | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | typeOfTest  | driverConfig      |
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | test0001prismabata@sharklasers.com | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_ita |
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                    | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |        323 |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_ita |
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                    | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |        323 |      20 | Giugno    |     2001 | androidTest | mobilebrowser_ita |

    @UAT_CR12_pl
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 | webTest    | default_pl   |
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |        323 |      01 | Gennaio   |     1950 | webTest    | default_pl   |

    @UAT_CR12_mobilePl
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | typeOfTest  | driverConfig     |
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_pl |
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |        323 |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_pl |
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                             | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |        323 |      20 | Giugno    |     2001 | androidTest | mobilebrowser_pl |

    @UAT_CR12_sk
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | typeOfTest | driverConfig |
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 | webTest    | default_sk   |
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |        323 |      01 | Gennaio   |     1950 | webTest    | default_sk   |

    @UAT_CR12_mobileSk
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | typeOfTest  | driverConfig     |
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_sk |
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |        323 |      01 | Gennaio   |     1950 | androidTest | mobilebrowser_sk |
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                             | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   |        323 |      20 | Giugno    |     2001 | androidTest | mobilebrowser_sk |
