Feature: UAT_CR: Customer registration on MYACCOUNT page

###########################################################################
# COUNTRY ITA
###########################################################################
@ita
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest |driverConfig |driverMailConfig|
      | <testIdJIRA> | webTest    | default | gmail|
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.1 | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 2458977568 | 01      | Gennaio   | 1950     | 
  
  @ita
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                              | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | test0001prismabata@sharklasers.com | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                    | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 323        | 01      | Gennaio   | 1950     | 
  
  ###########################################################################
  # COUNTRY CZ
  ###########################################################################
  @cz
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | driverConfig |driverMailConfig|
      | <testIdJIRA> | webTest    |  default | gmail|
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.1 | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   |  | 01      | Gennaio   | 1950     | 
  
  @cz
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                   | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | prisma_customerlogin_cz@sharklasers.com | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                         | Prisma2018! | cz      | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 323        | 20      | Giugno    | 2001     | 
  
  ###########################################################################
  # COUNTRY ITA MOBILE
  ###########################################################################
  @ita @mobile
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.1 | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
  
  @ita @mobile
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                              | password    | country | confirmPassword | name   | surname | address      | cap   | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | test0001prismabata@sharklasers.com | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                    | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 323        | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                    | Prisma2018! | ita     | Prisma2018!     | Tester | Uno     | Via Casa mia | 80100 | Napoli | Napoli   | 323        | 20      | Giugno    | 2001     | 
  
  ###########################################################################
  # COUNTRY PL
  ###########################################################################
  @pl
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest |  driverConfig |driverMailConfig|
      | <testIdJIRA> | webTest    | default | gmail|
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | maj   | 1950     | 
  
  @pl
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 323        | 01      | Gennaio   | 1950     | 
  
  ###########################################################################
  # COUNTRY PL MOBILE
  ###########################################################################
  @pl @mobile
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
  
  @pl @mobile
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 323        | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                             | Alice.it0$ | pl      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 323        | 20      | Giugno    | 2001     |
      
      
        ###########################################################################
  # COUNTRY SK
  ###########################################################################
  @sk
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest |  driverConfig |driverMailConfig|
      | <testIdJIRA> | webTest    | default | gmail|
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
  
  @sk
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 323        | 01      | Gennaio   | 1950     | 
  
  ###########################################################################
  # COUNTRY PL MOBILE
  ###########################################################################
  @sk @mobile
  Scenario Outline: Customer Registration on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then The user will be redirect on the profile page
      And The user check the received email
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.1 | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
  
  @sk @mobile
  Scenario Outline: Customer Registration on MYACCOUNT errors check
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following data for the registration
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
     Then check the error message for the field
      | field   | email   | password   | confirmPassword   | name   | surname   | address   | cap   | city   | province   | phone   | dayBorn   | monthBorn   | yearBorn   | 
      | <field> | <email> | <password> | <confirmPassword> | <name> | <surname> | <address> | <cap> | <city> | <province> | <phone> | <dayBorn> | <monthBorn> | <yearBorn> | 
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | field                | email                                       | password   | country | confirmPassword | name   | surname | address      | cap    | city   | province | phone      | dayBorn | monthBorn | yearBorn | 
      | storefront   | BT2018       | UAT_CR-1.2 | email-already-exists | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 3333333333 | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.3 | invalid-phone-number |                                             | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 323        | 01      | Gennaio   | 1950     | 
      | storefront   | BT2018       | UAT_CR-1.5 | tc-not-accepted      |                                             | Alice.it0$ | sk      | Alice.it0$      | Tester | Uno     | Via Casa mia | 12-345 | Napoli | Napoli   | 323        | 20      | Giugno    | 2001     |  