Feature: UAT_OT: Track an order Guest User

#################################################################
#COUNTRY ITA
#################################################################
@ita
  Scenario Outline: Track an order for a guest user via footer link
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | orderId   | emailAddress   | withError |
      | <orderId> | <emailAddress> | no        |
    And check the order status is equal to
      | orderDetailHeader   | orderStatus   |
      | <orderDetailHeader> | <orderStatus> |
    Then check the order details
      | orderNumber   | contactData   | deliveryAddress   | billingAddress   | payment   | totalPrice   | productName   | productBrand   | productId   | productSize   | productColor   | productQuantity   | productPrice   |
      | <orderNumber> | <contactData> | <deliveryAddress> | <billingAddress> | <payment> | <totalPrice> | <productName> | <productBrand> | <productId> | <productSize> | <productColor> | <productQuantity> | <productPrice> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | orderId       | emailAddress                 | orderDetailHeader                       | orderStatus            | orderNumber   | contactData                                 | deliveryAddress                                                                    | billingAddress                                                  | payment                                 | totalPrice | productName               | productBrand       | productId | productSize | productColor | productQuantity | productPrice |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023919 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023919 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 59.99 €    | Sandali Insolia con zeppa | BATA               |   7699251 |          35 | Blu          |               1 | 59.99 €      |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024034 | f.annunziata0585@hotmail.com | 08.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024034 | f.annunziata0585@hotmail.com\n+3495047035   | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 44.99 €    | Nike Tanjun               | NIKE               |   3096177 |        10,5 | Nero         |               1 | 39.99 €      |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023904 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023904 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 99.99 €    | Mocassini con nappa       | BATA THE SHOEMAKER |   8533140 |          44 | Marrone      |               1 | 99.99 €      |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024118 | f.annunziata0585@hotmail.com | 12.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024118 | f.annunziata0585@hotmail.com\n+393495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 44.99 €    | Infradito in vera pelle   | BATA               |   8646186 |          45 | Nero         |               1 | 39.99 €      |
@ita
  Scenario Outline: Place an order for guest user and track it
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And User choose an item and add it to cart
      | productName | quantity | size | color |
      |     5716469 |          |      |       |
    And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   |
      | home        | <emailAddress> | <emailSubject> |
    And Access to the track order page from the email summary link
    And check the order status is equal to
      | orderDetailHeader   | orderStatus   |
      | <orderDetailHeader> | <orderStatus> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                              | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | orderDetailHeader                         | orderStatus |
      | storefront   | BT2018       | UAT_OT-13.2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_trackanorder_guest@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | ${orderDate} ORDINE CON SPEDIZIONE A CASA | ORDINATO    |
@ita
  Scenario Outline: Track an order - ERROR - for a guest user via footer link
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | orderId   | emailAddress   | withError |
      | <orderId> | <emailAddress> | yes       |
    Then check the error message is correctly displayed
      | trackErrorMessage   |
      | <trackErrorMessage> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | orderId       | emailAddress                      | trackErrorMessage                                                                 |
      | storefront   | BT2018       | UAT_OT-13.7 | ita     | 0027210023919 | f.annunziata0585_jani@hotmail.com | Siamo spiacenti ma non vi sono ordini associati a questa email e numero d'ordine. |

#################################################################
#CONTRY ITA MOBILE
#################################################################
@ita @mobile
  Scenario Outline: Track an order for a guest user via footer link
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | androidTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | orderId   | emailAddress   | withError |
      | <orderId> | <emailAddress> | no        |
    And check the order status is equal to
      | orderDetailHeader   | orderStatus   |
      | <orderDetailHeader> | <orderStatus> |
    Then check the order details
      | orderNumber   | contactData   | deliveryAddress   | billingAddress   | payment   | totalPrice   | productName   | productBrand   | productId   | productSize   | productColor   | productQuantity   | productPrice   |
      | <orderNumber> | <contactData> | <deliveryAddress> | <billingAddress> | <payment> | <totalPrice> | <productName> | <productBrand> | <productId> | <productSize> | <productColor> | <productQuantity> | <productPrice> |
    And close the browser and clean all resources

    Examples: 
     | authUsername | authPassword  | testIdJIRA  | country | orderId       | emailAddress                 | orderDetailHeader                       | orderStatus            | orderNumber   | contactData                                 | deliveryAddress                                                                    | billingAddress                                                  | payment                                 | totalPrice | productName               | productBrand       | productId | productSize | productColor | productQuantity | productPrice |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023919 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023919 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 59.99 €    | Sandali Insolia con zeppa | BATA               |   7699251 |          35 | Blu          |               1 | 59.99 €      |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024034 | f.annunziata0585@hotmail.com | 08.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024034 | f.annunziata0585@hotmail.com\n+3495047035   | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Carta di credito VISA *************4305 | 44.99 €    | Nike Tanjun               | NIKE               |   3096177 |        10,5 | Nero         |               1 | 39.99 €      |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210023904 | f.annunziata0585@hotmail.com | 07.08.2019 ORDINE CON SPEDIZIONE A CASA | ORDINE IN PREPARAZIONE | 0027210023904 | f.annunziata0585@hotmail.com\n+3495047035   | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087                    | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 99.99 €    | Mocassini con nappa       | BATA THE SHOEMAKER |   8533140 |          44 | Marrone      |               1 | 99.99 €      |
      | storefront   | BT2018       | UAT_OT-13.1 | ita     | 0027210024118 | f.annunziata0585@hotmail.com | 12.08.2019 ORDINE CLICK&COLLECT         | ORDINE IN PREPARAZIONE | 0027210024118 | f.annunziata0585@hotmail.com\n+393495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Francesco Annunziata\nvia pantano gnilino n.2\nSarno, SA, 84087 | Paga con Pay Pal                        | 44.99 €    | Infradito in vera pelle   | BATA               |   8646186 |          45 | Nero         |               1 | 39.99 €      |
@ita @mobile
  Scenario Outline: Place an order for guest user and track it
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | androidTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And User choose an item and add it to cart
      | productName | quantity | size | color |
      |     5716469 |          |      |       |
    And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   |
      | home        | <emailAddress> | <emailSubject> |
    And Access to the track order page from the email summary link
    And check the order status is equal to
      | orderDetailHeader   | orderStatus   |
      | <orderDetailHeader> | <orderStatus> |
    And close the browser and clean all resources

    Examples: 
     | authUsername | authPassword  | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                              | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | orderDetailHeader                         | orderStatus |
      | storefront   | BT2018       | UAT_OT-13.2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_trackanorder_guest@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | ${orderDate} ORDINE CON SPEDIZIONE A CASA | ORDINATO    |
@ita @mobile
  Scenario Outline: Track an order - ERROR - for a guest user via footer link
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | androidTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | orderId   | emailAddress   | withError |
      | <orderId> | <emailAddress> | yes       |
    Then check the error message is correctly displayed
      | trackErrorMessage   |
      | <trackErrorMessage> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | orderId       | emailAddress                      | trackErrorMessage                                                                 |
      | storefront   | BT2018       | UAT_OT-13.7 | ita     | 0027210023919 | f.annunziata0585_jani@hotmail.com | Siamo spiacenti ma non vi sono ordini associati a questa email e numero d'ordine. |

