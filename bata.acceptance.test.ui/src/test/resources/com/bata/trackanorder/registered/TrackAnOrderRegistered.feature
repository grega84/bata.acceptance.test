@UAT_OTR
Feature: Track an order Registered User

  @UAT_OTR1 @sid:testIdJIRA
  Scenario Outline: Track an order for a registered user via footer link
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | registeredUser | orderId   | orderDetailHeader | orderStatus   |
      | y              | <orderId> | <orderHeader>     | <orderStatus> |
    Then check the order details
      | orderNumber   | orderStatus   | contactData   | deliveryAddress   | billingAddress   | payment   | totalPrice   | productName   | productBrand   | productId   | productSize   | productColor   | productQuantity   | productPrice   |
      | <orderNumber> | <orderStatus> | <contactData> | <deliveryAddress> | <billingAddress> | <payment> | <totalPrice> | <productName> | <productBrand> | <productId> | <productSize> | <productColor> | <productQuantity> | <productPrice> |
    And close the browser and clean all resources

    @UAT_OTR1_ita
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA    | country | email                                            | password   | orderId       | orderHeader                             | orderNumber   | orderStatus            | contactData                                                     | deliveryAddress                              | billingAddress                               | payment          | totalPrice | productName       | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_OT-13.4-1 | ita     | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ | 0027210047665 | 23.02.2021 ORDINE CON SPEDIZIONE A CASA | 0027210047665 | ORDINE IN PREPARAZIONE | automation.test.prisma+it.placeanorder@gmail.com\n+396333333333 | Prisma SRL\nVia Delia n. 23\nRoma, RM, 00155 | Prisma SRL\nVia Delia n. 23\nRoma, RM, 00155 | Paga con Pay Pal | 51.99 €    | nike court vision | NIKE         |   5011572 |         5,5 | Bianco       |               1 | 51.99 €      |

  @UAT_OTR2 @sid:testIdJIRA
  Scenario Outline: Place an order for registered user and track it
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | registeredUser | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | y              | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    When the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   | driverMailConfig   |
      | home        | <emailAddress> | <emailSubject> | <driverMailConfig> |
    And Access to the track order page from the email summary link
    And check the order status is equal to
      | orderDetailHeader   | orderStatus   |
      | <orderDetailHeader> | <orderStatus> |
    And close the browser and clean all resources

    @UAT_OTR2_ita
    Examples: 
      | driverMailConfig | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | email                                            | password   | coupon | voucher | paymentMethod | emailAddress                                     | phone      | name   | surname | address         | cap   | city | province | country | paymentType | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailSubject             | orderDetailHeader                         | orderStatus | productName |
      | gmail            | webTest    | default_ita  | storefront   | BT2018       | UAT_OT-13.5 | ita     | automation.test.prisma+it.placeanorder@gmail.com | Alice.it0$ |        |         | paypal        | automation.test.prisma+it.placeanorder@gmail.com | 6333333333 | Prisma | SRL     | Via Delia n. 23 | 00155 | Roma | Roma     | Italia  | paypal      | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | Ordine n. ${orderNumber} | ${orderDate} ORDINE CON SPEDIZIONE A CASA | ORDINATO    | nike        |
