Feature: UAT_OT: Track an order Registered User
#############################################################
#COUNTRY ITA
#############################################################
@ita
  Scenario Outline: Track an order for a registered user via footer link
    Given the JIRA test
      | testIdJIRA   |
      | <testIdJIRA> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername | authPassword |
      | <country> | <email> | <password> |<authUsername> | <authPassword> |
    And Access to the track order page from the footer link with following data
      | orderId   | orderDetailHeader | orderStatus   |
      | <orderId> | <orderHeader>     | <orderStatus> |
    Then check the order details
      | orderNumber   | orderStatus   | contactData   | deliveryAddress   | billingAddress   | payment   | totalPrice   | productName   | productBrand   | productId   | productSize   | productColor   | productQuantity   | productPrice   |
      | <orderNumber> | <orderStatus> | <contactData> | <deliveryAddress> | <billingAddress> | <payment> | <totalPrice> | <productName> | <productBrand> | <productId> | <productSize> | <productColor> | <productQuantity> | <productPrice> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword || testIdJIRA  | country | email                                    | password   | orderId       | orderHeader                             | orderNumber   | orderStatus            | contactData                                           | deliveryAddress                                                                    | billingAddress                                           | payment                                 | totalPrice | productName        | productBrand | productId | productSize | productColor | productQuantity | productPrice |
      | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024201 | 13.08.2019 ORDINE CON SPEDIZIONE A CASA | 0027210024201 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142                           | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Carta di credito VISA *************4305 | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          37 | Bianco       |               1 | 23.99 €      |
      | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024202 | 13.08.2019 ORDINE CLICK&COLLECT         | 0027210024202 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Carta di credito VISA *************4305 | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          41 | Bianco       |               1 | 23.99 €      |
      | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024203 | 13.08.2019 ORDINE CON SPEDIZIONE A CASA | 0027210024203 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142                           | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Paga con Pay Pal                        | 28.99 €    | Ciabatte con zeppa | BATA         |   7711113 |          40 | Bianco       |               1 | 23.99 €      |
      | storefront   | BT2018       | UAT_OT-13.4 | ita     | prisma_trackanorder_test@sharklasers.com | Alice.it0$ | 0027210024204 | 13.08.2019 ORDINE CLICK&COLLECT         | 0027210024204 | ORDINE IN PREPARAZIONE | prisma_trackanorder_test@sharklasers.com\n+3495047035 | 72734 Foggia (Foggia)\nBATA VIALE DEGLI AVIATORI CC MONGOLFIERA\nFOGGIA, FG, 71100 | Prisma SRL\nvia degli ulivi gialli n.23\nRoma, RM, 00142 | Paga con Pay Pal                        | 28.99 €    | Ciabatte in pelle  | BATA         |   7711113 |          40 | Bianco       |               1 | 23.99 €      |
@ita
  Scenario Outline: Place an order for registered user and track it
    Given the JIRA test
      | testIdJIRA   |
      | <testIdJIRA> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername | authPassword |
      | <country> | <email> | <password> |<authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item and add it to cart
      | productName | quantity | size | color |
      | nike        |          |      |       |
    And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment
      | homeOrStore | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check confermation email received
      | homeOrStore | emailAddress   | emailSubject   |
      | home        | <emailAddress> | <emailSubject> |
    And Access to the track order page from the email summary link
    And check the order status is equal to
      | orderDetailHeader   | orderStatus   |
      | <orderDetailHeader> | <orderStatus> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword || testIdJIRA  | country | email                                    | password   | coupon | voucher | paymentMethod | emailAddress                             | phone      | name      | surname    | address                 | cap   | city  | province | country | paymentType | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailSubject             | orderDetailHeader                         | orderStatus |
      | storefront   | BT2018       | UAT_OT-13.5 | ita     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |        |         | card          | prisma_placeanorder_test@sharklasers.com | 3495047035 | Francesco | Annunziata | via pantano gnilino n.2 | 84087 | Sarno | Salerno  | Italia  | card        | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | Ordine n. ${orderNumber} | ${orderDate} ORDINE CON SPEDIZIONE A CASA | ORDINATO    |
