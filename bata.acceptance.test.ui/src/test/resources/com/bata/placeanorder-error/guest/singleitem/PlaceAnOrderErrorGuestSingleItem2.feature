Feature: UAT_OE: Place an order error for guest user - single item

##################################################################
#COUNTRY ITA
##################################################################
@ita
  Scenario Outline: place an order for a guest user - Single ITEM - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.25 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | 
      | storefront   | BT2018       | UAT_OE-12.25 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | 
  
  @ita
  Scenario Outline: place an order for guest user - Single ITEM - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | 
      | storefront   | BT2018       | UAT_OE-12.26 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | 
  
  @ita
  Scenario Outline: place an oder for guest user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
  
  @ita
  Scenario Outline: place an order for a guest user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.29 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  @ita
  Scenario Outline: place an order for a guest user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.24 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  @ita
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | store       | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.32 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.32 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | 45011     | 
  
  @ita
  Scenario Outline: place an order for guest user Single ITEM - store delivery - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.33 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | 45011     | 
  
  @ita
  Scenario Outline: place an oder for guest user Single ITEM - store delivery - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
  
  @ita
  Scenario Outline: place an order for a guest user - Single ITEM - store delivery - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.35 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45011     | 
  
  @ita
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.24 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 45011     | 
  
  @ita
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - Insert wrong info in select store field
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to store with wrong store info
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     Then check the store error message is displayed
      | dataStore   | 
      | <dataStore> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         | 
      | storefront   | BT2018       | UAT_OE-12.31 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente | 
      | storefront   | BT2018       | UAT_OE-12.31 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45848             | 
  
  @ita
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 8149175     | 6        | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.28 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  ##################################################################
  #COUNTRY ITA MOBILE
  ##################################################################
  @ita @mobile
  Scenario Outline: place an order for a guest user - Single ITEM - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | emailSubject | 
  
  #| storefront   | BT2018       | UAT_OE-12.25 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |
  #| storefront   | BT2018       | UAT_OE-12.25 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |
  @ita @mobile
  Scenario Outline: place an order for guest user - Single ITEM - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | 
      | storefront   | BT2018       | UAT_OE-12.26 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | 
  
  @ita @mobile
  Scenario Outline: place an oder for guest user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 
  
  @ita @mobile
  Scenario Outline: place an order for a guest user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.29 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  @ita @mobile
  Scenario Outline: place an order for a guest user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.24 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  @ita @mobile
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | store       | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | country | coupon | voucher | cardNumber | cvv | ownerCard | monthExpiredDate | yearExpiredDate | emailPaypal | passwordPaypal | emailAddress | phone | name | surname | address | cap | city | province | country | emailSubject | dataStore | 
  
  #| storefront   | BT2018       | UAT_OE-12.32 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |     45011 |
  #| storefront   | BT2018       | UAT_OE-12.32 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |     45011 |
  @ita @mobile
  Scenario Outline: place an order for guest user Single ITEM - store delivery - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.33 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | 45011     | 
  
  @ita @mobile
  Scenario Outline: place an oder for guest user Single ITEM - store delivery - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
      | storefront   | BT2018       | UAT_OE-12.34 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | 
  
  @ita @mobile
  Scenario Outline: place an order for a guest user - Single ITEM - store delivery - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.35 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45011     | 
  
  @ita @mobile
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.24 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 45011     | 
  
  @ita @mobile
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - Insert wrong info in select store field
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5716469     |          |      |       | 
      And place an order to store with wrong store info
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     Then check the store error message is displayed
      | dataStore   | 
      | <dataStore> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         | 
      | storefront   | BT2018       | UAT_OE-12.31 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente | 
      | storefront   | BT2018       | UAT_OE-12.31 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45848             | 
  
  @ita @mobile
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 8149175     | 6        | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.28 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  ##################################################################
  #COUNTRY CZ
  ##################################################################
  @cz
  Scenario Outline: place an order for a guest user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | test        |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.29 | cz      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  @cz
  Scenario Outline: place an order for a guest user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | test        |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.24 | cz      |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  @cz
  Scenario Outline: place an order for a guest user - Single ITEM - store delivery - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | test        |          |      |       | 
      And place an order to "store"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.35 | cz      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Napoli  | Italia      | paypal       | 45011     | 
  
  @cz
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | test        |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject             | dataStore | 
      | storefront   | BT2018       | UAT_OE-12.24 | cz      |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 45011     | 
  
  @cz
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - Insert wrong info in select store field
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | test        |          |      |       | 
      And place an order to store with wrong store info
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     Then check the store error message is displayed
      | dataStore   | 
      | <dataStore> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject | dataStore         | 
      | storefront   | BT2018       | UAT_OE-12.31 | cz      |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Napoli  | Italia      | paypal       | store inesistente | 
      | storefront   | BT2018       | UAT_OE-12.31 | cz      |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Napoli  | Italia      | paypal       | 45848             | 
  
  @cz
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User select items for a total amount of 500€
      | productName | quantity | 
      | test        | 6        | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject |                          | 
      | storefront   | BT2018       | UAT_OE-12.28 | cz      |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Napoli  | Italia      | paypal       | Ordine n. ${orderNumber} | 
  ##################################################################
  #COUNTRY PL
  ##################################################################
  @pl
  Scenario Outline: place an oder for guest user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber              | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | all_field_empty        |                         |     |               | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    | 4                       | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111      | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 111 | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee      | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111     | 737 | t             | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111     | 737 | 5435435435435 | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111     | 1   | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111     | ewe | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
  
  @pl
  Scenario Outline: place an order for a guest user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.24 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
  
  @pl
  Scenario Outline: place an order for a guest user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.29 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  #LO SCENARIO SEGUENTE è DA PROVARE SU PL
  @pl
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 5665615     | 10       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.28 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card      | Ordine n. ${orderNumber} | 
  
  ##################################################################
  #COUNTRY PL MOBILE
  ##################################################################
  @pl @mobile
  Scenario Outline: place an oder for guest user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber              | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | all_field_empty        |                         |     |               | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    | 4                       | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111      | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 111 | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee      | 737 | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111     | 737 | t             | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111     | 737 | 5435435435435 | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111     | 1   | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
      | storefront   | BT2018       | UAT_OE-12.27 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111     | ewe | test prisma   | październik      | 2020            | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | 
  
  @pl @mobile
  Scenario Outline: place an order for a guest user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.29 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 
  
  @pl @mobile
  Scenario Outline: place an order for a guest user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5665615     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.24 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 
  
  #LO SCENARIO SEGUENTE è DA PROVARE SU PL
  @pl @mobile
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 5665615     | 10       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | 
      | storefront   | BT2018       | UAT_OE-12.28 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card      | Ordine n. ${orderNumber} | 