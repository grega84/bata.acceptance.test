@UAT_OE
Feature: Place an order error

  @UAT_OE1 @sid:testIdJIRA
  Scenario Outline: place an order for a guest user - Single ITEM - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   |
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> |
    Then check the paypal error is displayed
    And close the browser and clean all resources

    @UAT_OE1_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.25-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.25-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |

    @UAT_OE1_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.25-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.25-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |

  @UAT_OE2 @sid:testIdJIRA
  Scenario Outline: place an order for guest user - Single ITEM - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the ts error is displayed
    And close the browser and clean all resources

    @UAT_OE2_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.26 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |

    @UAT_OE2_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.26 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |

  @UAT_OE3 @sid:testIdJIRA
  Scenario Outline: place an oder for guest user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user select to pay with credit card
    And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   |
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> |
    And close the browser and clean all resources

    @UAT_OE3_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.27-9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |

    @UAT_OE3_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.27-9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |

    @UAT_OE3_pl
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber              | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-1 | pl      |        |         | all_field_empty        |                         |     |               | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-2 | pl      |        |         | number_less_than_16    |                       4 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-3 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111      | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-4 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 111 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-5 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee      | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-6 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111     | 737 | t             | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-7 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111     | 737 | 5435435435435 | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-8 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111     |   1 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.27-9 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111     | ewe | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |

    @UAT_OE3_mobilePl
    Examples: 
      | productName | typeOfTest  | driverConfig     | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber              | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-1 | pl      |        |         | all_field_empty        |                         |     |               | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-2 | pl      |        |         | number_less_than_16    |                       4 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-3 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111      | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-4 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 111 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-5 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee      | 737 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-6 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111     | 737 | t             | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-7 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111     | 737 | 5435435435435 | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-8 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111     |   1 | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.27-9 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111     | ewe | test prisma   | październik      |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        |

  @UAT_OE4 @sid:testIdJIRA
  Scenario Outline: place an order for a guest user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the coupon error message is displayed
    And close the browser and clean all resources

    @UAT_OE4_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.29 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @UAT_OE4_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.29 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @UAT_OE4_cz
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject             |
      | nike        | webTest    | default_cz   | gmail            | storefront   | BT2018       | UAT_OE-12.29 | cz      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @UAT_OE4_pl
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             |
      | nike        | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.29 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

    @UAT_OE4_mobilePl
    Examples: 
      | productName | typeOfTest  | driverConfig     | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             |
      | nike        | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.29 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |

  @UAT_OE5 @sid:testIdJIRA
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   |
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> |
    Then check the paypal error is displayed
    And close the browser and clean all resources

    @UAT_OE5_ita
    Examples: 
      | dataStore | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             |
      |     45011 | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.32-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |
      |     45011 | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.32-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |

    @UAT_OE5_mobileIta
    Examples: 
      | dataStore | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             |
      |     45011 | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.32-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |
      |     45011 | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.32-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |

  @UAT_OE6 @sid:testIdJIRA
  Scenario Outline: place an order for guest user Single ITEM - store delivery - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | store       | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the ts error is displayed
    And close the browser and clean all resources

    @UAT_OE6_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | dataStore |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.33 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |     45011 |

    @UAT_OE6_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | dataStore |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.33 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |     45011 |

  @UAT_OE7 @sid:testIdJIRA
  Scenario Outline: place an oder for guest user Single ITEM - store delivery - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And the user select to pay with credit card
    And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   |
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> |
    And close the browser and clean all resources

    @UAT_OE7_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.34-9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |

    @UAT_OE7_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.34-9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 |

  @UAT_OE8 @sid:testIdJIRA
  Scenario Outline: place an order for a guest user - Single ITEM - store delivery - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the coupon error message is displayed
    And close the browser and clean all resources

    @UAT_OE8_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.35 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 |

    @UAT_OE8_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.35 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 |

    @UAT_OE8_cz
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject | dataStore |
      | sneakers    | webTest    | default_cz   | gmail            | storefront   | BT2018       | UAT_OE-12.35 | cz      | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Napoli  | Italia      | paypal       |     45011 |

  @UAT_OE9 @sid:testIdJIRA
  Scenario Outline: place an order for a guest user Single ITEM - store delivery - Insert wrong info in select store field
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to store with wrong store info
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    Then check the store error message is displayed
      | dataStore   |
      | <dataStore> |
    And close the browser and clean all resources

    @UAT_OE9_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.31-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.31-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |             45848 |

    @UAT_OE9_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                        | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.31-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.31-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder@sharklasers.com | 3333333333 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |             45848 |

    @UAT_OE9_cz
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                   | phone      | name   | surname  | address                     | cap    | city  | province | country | paymentType | emailSubject | dataStore         |
      | nike        | webTest    | default_cz   | gmail            | storefront   | BT2018       | UAT_OE-12.31-1 | cz      |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Napoli  | Italia      | paypal       | store inesistente |
      | nike        | webTest    | default_cz   | gmail            | storefront   | BT2018       | UAT_OE-12.31-2 | cz      |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | e2eceprisma+prisma40@gmail.com | 3510660234 | Andrea | prisma40 | Piazza Giovanni Cavour n.33 | 109 00 | Praha | Napoli   | Napoli  | Italia      | paypal       |             45848 |
