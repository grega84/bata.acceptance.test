Feature: UAT_OE: Place an order error for registered user - single item
############################################################################
#COUNTRY ITA
############################################################################

@ita
  Scenario Outline: place an order for a registered user - Single ITEM - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.7 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.7 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for registered user - Single ITEM - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.8 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an oder for registered user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.9.1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.2 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for a registered user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
  
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.11 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for a registered user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.6 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | store       | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.14 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.14 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for registered user Single ITEM - store delivery - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.15 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an oder for registered user Single ITEM - store delivery - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for a registered user - Single ITEM - store delivery - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.17 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.6 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - Insert wrong info in select store field
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to store with wrong store info
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     Then check the store error message is displayed
      | dataStore   | 
      | <dataStore> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45848             | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 8149175     | 6        | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.10 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  ############################################################################
  #COUNTRY ITA MOBILE
  ############################################################################
  
  @ita @mobile
  Scenario Outline: place an order for a registered user - Single ITEM - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.7 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.7 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for registered user - Single ITEM - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.8 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an oder for registered user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.9.1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.2 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for a registered user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.11 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for a registered user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.6 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   | 
      | store       | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> | 
     Then check the paypal error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.14 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.14 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for registered user Single ITEM - store delivery - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the ts error is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.15 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an oder for registered user Single ITEM - store delivery - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_less_than_16    | 4                        | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      | 1   | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.16 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          | 2020            | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for a registered user - Single ITEM - store delivery - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.17 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | store       | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.6 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | 45011     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - Insert wrong info in select store field
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 7711113     |          |      |       | 
      And place an order to store with wrong store info
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> | 
     Then check the store error message is displayed
      | dataStore   | 
      | <dataStore> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | 45848             | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita @mobile
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 8149175     | 6        | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   | 
      | storefront   | BT2018       | UAT_OE-12.10 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  ############################################################################
  #COUNTRY PL
  ############################################################################
  
  @pl
  Scenario Outline: place an oder for registered user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.9.1 | pl      |        |         | all_field_empty        |                          |     |               | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.2 | pl      |        |         | number_less_than_16    | 4                        | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.3 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111       | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.4 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 1111 | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.5 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee       | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.6 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.7 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.8 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      | 1   | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.9 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl
  Scenario Outline: place an order for a registered user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.11 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl
  Scenario Outline: place an order for a registered user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.6 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  #LO SCENARIO SEGUENTE è DA PROVARE SU PL          
  @pl
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | 
      | <testIdJIRA> | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 5665615     | 6        | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.10 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  ############################################################################
  #COUNTRY PL MOBILE
  ############################################################################
  
  @pl @mobile
  Scenario Outline: place an oder for registered user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user select to pay with credit card
      And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | 
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.9.1 | pl      |        |         | all_field_empty        |                          |     |               | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.2 | pl      |        |         | number_less_than_16    | 4                        | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.3 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111       | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.4 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 1111 | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.5 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee       | 737 | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.6 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.7 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.8 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      | 1   | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
      | storefront   | BT2018       | UAT_OE-12.9.9 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | październik      | 2020            | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: place an order for a registered user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the coupon error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.11 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl @mobile
  Scenario Outline: place an order for a registered user - Single ITEM - Apply Voucher that can't be combined with other promo
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User choose an item with "no promo" and add it to cart
      | productName | quantity | size | color | 
      | 5411600     |          |      |       | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
      And the user pay with following data payment and voucher that can't be combined with other promo
      | homeOrStore | voucher   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | home        | <voucher> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the voucher error message is displayed
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.6 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  #LO SCENARIO SEGUENTE è DA PROVARE SU PL     
  @pl @mobile
  Scenario Outline: place an order with amount greater than 500€, disappearing of cod payments type
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And User select items for a total amount of 500€
      | productName | quantity | 
      | 5665615     | 8        | 
      And place an order to "home"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | 
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | 
     Then check the disappearing of COD payment
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   | 
      | storefront   | BT2018       | UAT_OE-12.10 | pl      |        |         | 4111 1111 1111 1111 | 737 | test prisma | październik      | 2020            | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
