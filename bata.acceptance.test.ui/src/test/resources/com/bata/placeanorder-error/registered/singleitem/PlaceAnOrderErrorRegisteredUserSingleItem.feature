@UAT_OER
Feature: : Place an order error for registered user

  @UAT_OER1 @sid:testIdJIRA
  Scenario Outline: place an order for a registered user - Single ITEM - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   |
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> |
    Then check the paypal error is displayed
    And close the browser and clean all resources

    @UAT_OER1_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.7-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.7-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER1_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.7-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.7-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

  @UAT_OER2 @sid:testIdJIRA
  Scenario Outline: place an order for registered user - Single ITEM - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the ts error is displayed
    And close the browser and clean all resources

    @UAT_OER2_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.8 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER2_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA  | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.8 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

  @UAT_OER3 @sid:testIdJIRA
  Scenario Outline: place an oder for registered user - Single ITEM - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And the user select to pay with credit card
    And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   |
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> |
    And close the browser and clean all resources

    @UAT_OER3_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.9.9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER3_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.9.9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER3_pl
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | email                                       | password   |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.1 | pl      |        |         | all_field_empty        |                          |     |               | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.2 | pl      |        |         | number_less_than_16    |                        4 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.3 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111       | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.4 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 1111 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.5 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee       | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.6 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.7 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.8 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      |   1 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.9.9 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |

    @UAT_OER3_mobilePl
    Examples: 
      | productName | typeOfTest  | driverConfig     | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | email                                       | password   |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.1 | pl      |        |         | all_field_empty        |                          |     |               | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.2 | pl      |        |         | number_less_than_16    |                        4 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.3 | pl      |        |         | number_less_than_16    | 4111 1111 1111 111       | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.4 | pl      |        |         | number_greater_than_16 | 4111 1111 1111 1111 1111 | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.5 | pl      |        |         | number_with_alpha      | 4111 1111 1sdd eee       | 737 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.6 | pl      |        |         | owner_with_one_char    | 4111 1111 1111 1111      | 737 | t             | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.7 | pl      |        |         | owner_with_only_digits | 4111 1111 1111 1111      | 737 | 5435435435435 | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.8 | pl      |        |         | cvv_less_than_3        | 4111 1111 1111 1111      |   1 | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.9.9 | pl      |        |         | cvv_alphanumeric       | 4111 1111 1111 1111      | ewe | test prisma   | październik      |            2020 | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | card        | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |

  @UAT_OER4 @sid:testIdJIRA
  Scenario Outline: place an order for a registered user - Single ITEM - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "home"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the coupon error message is displayed
    And close the browser and clean all resources

    @UAT_OER4_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.11 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER4_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.11 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER4_pl
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   |
      | sneakers    | webTest    | default_pl   | gmail            | storefront   | BT2018       | UAT_OE-12.11 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |

    @UAT_OER4_mobilePl
    Examples: 
      | productName | typeOfTest  | driverConfig     | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                                | phone      | name   | surname | address               | cap    | city   | province | country | paymentType | emailSubject             | email                                       | password   |
      | sneakers    | androidTest | mobilebrowser_pl | gmail            | storefront   | BT2018       | UAT_OE-12.11 | pl      | PROVA_COUPON_INESISTENTE |         | 4111 1111 1111 1111 | 737 | test prisma | październik      |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test_pl@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 12-345 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |

  @UAT_OER5 @sid:testIdJIRA
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - wrong paypal account
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And the user pay with following data payment and incorrect paypal account
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | emailPaypal   | passwordPaypal   |
      | store       | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <emailPaypal> | <passwordPaypal> |
    Then check the paypal error is displayed
    And close the browser and clean all resources

    @UAT_OER5_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | dataStore | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.14-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.14-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER5_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                  | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | emailSubject             | dataStore | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.14-1 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.coraaa.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | Ordine n. ${orderNumber} |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.14-2 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com   | chu5ePhgghfgfa | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |                          |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

  @UAT_OER6 @sid:testIdJIRA
  Scenario Outline: place an order for registered user Single ITEM - store delivery - pay with paypal and not accept t&s
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And the user pay with following data payment and don't accept ts
      | homeOrStore | paymentType | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | paypal      | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the ts error is displayed
    And close the browser and clean all resources

    @UAT_OER6_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | dataStore | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.15 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER6_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | dataStore | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.15 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

  @UAT_OER7 @sid:testIdJIRA
  Scenario Outline: place an oder for registered user Single ITEM - store delivery - insert wrong credit card credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And the user select to pay with credit card
    And insert the following credit card info and check the error message
      | errorCheck   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   |
      | <errorCheck> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> |
    And close the browser and clean all resources

    @UAT_OER7_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.16-9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER7_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA     | country | coupon | voucher | errorCheck             | cardNumber               | cvv | ownerCard     | monthExpiredDate | yearExpiredDate | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | dataStore | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-1 | ita     |        |         | all_field_empty        |                          |     |               | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-2 | ita     |        |         | number_less_than_16    |                        4 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-3 | ita     |        |         | number_less_than_16    | 4988 4388 4388 430       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-4 | ita     |        |         | number_greater_than_16 | 4988 4388 4388 4305 4458 | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-5 | ita     |        |         | number_with_alpha      | 4988 4388 3sdd eee       | 737 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-6 | ita     |        |         | owner_with_one_char    | 4988 4388 4388 4305      | 737 | t             | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-7 | ita     |        |         | owner_with_only_digits | 4988 4388 4388 4305      | 737 | 5435435435435 | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-8 | ita     |        |         | cvv_less_than_3        | 4988 4388 4388 4305      |   1 | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.16-9 | ita     |        |         | cvv_alphanumeric       | 4988 4388 4388 4305      | ewe | test prisma   | Ottobre          |            2020 | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | card        |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

  @UAT_OER8 @sid:testIdJIRA
  Scenario Outline: place an order for a registered user - Single ITEM - store delivery - insert a wrong coupon
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to "store"
      | coupon | voucher | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      |        |         |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    And insert a wrong coupon
      | homeOrStore | coupon   | paymentType   | cardNumber   | cvv   | ownerCard   | monthExpiredDate   | yearExpiredDate   | emailPaypal   | passwordPaypal   | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   |
      | home        | <coupon> | <paymentType> | <cardNumber> | <cvv> | <ownerCard> | <monthExpiredDate> | <yearExpiredDate> | <emailPaypal> | <passwordPaypal> | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> |
    Then check the coupon error message is displayed
    And close the browser and clean all resources

    @UAT_OER8_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.17 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER8_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon                   | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.17 | ita     | PROVA_COUPON_INESISTENTE |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |     45011 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

  @UAT_OER9 @sid:testIdJIRA
  Scenario Outline: place an order for a registered user Single ITEM - store delivery - Insert wrong info in select store field
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And User choose an item with "no promo" and add it to cart
      | productName   | quantity | size | color |
      | <productName> |          |      |       |
    And place an order to store with wrong store info
      | coupon   | voucher   | paymentMethod | emailAddress   | phone   | name   | surname   | address   | cap   | city   | province   | country   | dataStore   |
      | <coupon> | <voucher> |               | <emailAddress> | <phone> | <name> | <surname> | <address> | <cap> | <city> | <province> | <country> | <dataStore> |
    Then check the store error message is displayed
      | dataStore   |
      | <dataStore> |
    And close the browser and clean all resources

    @UAT_OER9_ita
    Examples: 
      | productName | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         | email                                    | password   |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | webTest    | default_ita  | gmail            | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |             45848 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_OER9_mobileIta
    Examples: 
      | productName | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA   | country | coupon | voucher | cardNumber          | cvv | ownerCard   | monthExpiredDate | yearExpiredDate | emailPaypal                | passwordPaypal | emailAddress                             | phone      | name   | surname | address               | cap   | city   | province | country | paymentType | emailSubject             | dataStore         | email                                    | password   |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} | store inesistente | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | sneakers    | androidTest | mobilebrowser_ita | gmail            | storefront   | BT2018       | UAT_OE-12.13 | ita     |        |         | 4988 4388 4388 4305 | 737 | test prisma | Ottobre          |            2020 | nicola.cora.buyer@bata.com | chu5ePha       | prisma_placeanorder_test@sharklasers.com | 3495047035 | tester | prisma  | Piazza Garibaldi n.34 | 80100 | Napoli | Napoli   | Italia  | paypal      | Ordine n. ${orderNumber} |             45848 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
