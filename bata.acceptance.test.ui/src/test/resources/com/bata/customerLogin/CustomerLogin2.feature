Feature: UAT_LG: Customer login on MYACCOUNT page

############################################################################
# COUNTRY ITA
############################################################################
@ita
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | driverConfig |
      | <testIdJIRA> | webTest    | default |
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | authUsername | authPassword | 
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | storefront   | BT2018       | 
  
  @ita
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest |  driverConfig |
      | <testIdJIRA> | webTest    |  default |
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following wrong credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user is notified with an error message
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                       | password    | country | 
      | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | ita     | 
      | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | ita     | 
  
  @ita
  Scenario Outline: Customer login on MYACCOUNT page-Login again just after logout
    Given the JIRA test
      | testIdJIRA   | typeOfTest | driverConfig |
      | <testIdJIRA> | webTest    | default |
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
      And The user logout to Bata site
      And The user logs in again to Bata site
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | 
  
  ############################################################################
  # COUNTRY CZ
  ############################################################################
  @cz
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                          | password     | country | authUsername | authPassword | 
      | storefront   | BT2018       | UAT_LG-2.1 | e2eceprisma+prisma40@gmail.com | testPrisma1! | cz      | storefront   | BT2018       | 
  
  @cz
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following wrong credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user is notified with an error message
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                       | password    | country | authUsername | authPassword | 
      | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | cz      | storefront   | BT2018       | 
      | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | cz      | storefront   | BT2018       | 
  
  @cz
  Scenario Outline: Customer login on MYACCOUNT page-Login again just after logout
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
      And The user logout to Bata site
      And The user logs in again to Bata site
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                          | password     | country | authUsername | authPassword | 
      | storefront   | BT2018       | UAT_LG-2.5 | e2eceprisma+prisma40@gmail.com | testPrisma1! | cz      | storefront   | BT2018       | 
  
  ############################################################################
  # COUNTRY ITA MOBILE
  ############################################################################
  @ita @mobile
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | authUsername | authPassword | 
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | storefront   | BT2018       | 
  
  @ita @mobile
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following wrong credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user is notified with an error message
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                       | password    | country | 
      | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | ita     | 
      | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | ita     | 
  
  @ita @mobile
  Scenario Outline: Customer login on MYACCOUNT page-Login again just after logout
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
      And The user logout to Bata site
      And The user logs in again to Bata site
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | 
  
  ############################################################################
  # COUNTRY PL
  ############################################################################
  @pl
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                       | password   | country | authUsername | authPassword | 
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | storefront   | BT2018       | 
  
  @pl
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following wrong credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user is notified with an error message
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.2 | prisma_placeanorder_test_pl@sharklasers.com   | x          | pl      | 
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_placeanorder_test_paaa@sharklasers.com | Alice.it0$ | pl      | 
  
  @pl
  Scenario Outline: Customer login on MYACCOUNT page-Login again just after logout
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
      And The user logout to Bata site
      And The user logs in again to Bata site
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test_paaa@sharklasers.com | Alice.it0$ | pl      | 
  
  ############################################################################
  # COUNTRY PL MOBILE
  ############################################################################
  @pl @mobile
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                       | password   | country | authUsername | authPassword | 
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | storefront   | BT2018       | 
  
  @pl @mobile
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following wrong credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user is notified with an error message
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.2 | prisma_placeanorder_test_pl@sharklasers.com   | x          | pl      | 
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_placeanorder_test_paaa@sharklasers.com | Alice.it0$ | pl      | 
       ############################################################################
  # COUNTRY sk
  ############################################################################
  @sk
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | testIdJIRA | email                                       | password   | country | authUsername | authPassword | 
      | UAT_LG-2.1 | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | storefront   | BT2018       | 
  
  @sk
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following wrong credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user is notified with an error message
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.2 | e2eceprisma+automation_generic_sk@gmail.com   | x          | sk      | 
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_skaceanorder_test_paaa@sharklasers.com | Alice.it0$ | sk      | 
  
  @sk
  Scenario Outline: Customer login on MYACCOUNT page-Login again just after logout
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
      And The user logout to Bata site
      And The user logs in again to Bata site
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_skaceanorder_test_paaa@sharklasers.com | Alice.it0$ | sk      | 
  
  ############################################################################
  # COUNTRY sk MOBILE
  ############################################################################
  @sk @mobile
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user will be redirect on the profile page
      And close the browser and clean all resources
  
    Examples: 
      | testIdJIRA | email                                       | password   | country | authUsername | authPassword | 
      | UAT_LG-2.1 | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | storefront   | BT2018       | 
  
  @sk @mobile
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user go to BATA site
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
     When The user insert the following wrong credential
      | email   | password   | 
      | <email> | <password> | 
     Then The user is notified with an error message
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | 
      | storefront   | BT2018       | UAT_LG-2.2 | e2eceprisma+automation_generic_sk@gmail.com   | x          | sk      | 
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_skaceanorder_test_paaa@sharklasers.com | Alice.it0$ | sk      | 