@UAT_LG
Feature: Customer login
  Customer login on MYACCOUNT page

  @UAT_LG21 @sid:testIdJIRA
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with right credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following credential
      | email   | password   |
      | <email> | <password> |
    Then The user will be redirect on the profile page
    And close the browser and clean all resources

    @UAT_LG21_ita
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | authUsername | authPassword | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | storefront   | BT2018       | default_ita  | webTest    |

    @UAT_LG21_cz
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                          | password     | country | authUsername | authPassword | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.1 | e2eceprisma+prisma40@gmail.com | testPrisma1! | cz      | storefront   | BT2018       | default_cz   | webTest    |

    @UAT_LG21_mobileIta
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | authUsername | authPassword | driverConfig      | typeOfTest  |
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | storefront   | BT2018       | mobilebrowser_ita | androidTest |

    @UAT_LG21_pl
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                       | password   | country | authUsername | authPassword | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | storefront   | BT2018       | default_pl   | webTest    |

    @UAT_LG21_mobilePl
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                       | password   | country | authUsername | authPassword | driverConfig     | typeOfTest  |
      | storefront   | BT2018       | UAT_LG-2.1 | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | pl      | storefront   | BT2018       | mobilebrowser_pl | androidTest |

    @UAT_LG21_sk
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                       | password   | country | authUsername | authPassword | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.1 | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | storefront   | BT2018       | default_sk   | webTest    |

    @UAT_LG21_mobileSk
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                       | password   | country | authUsername | authPassword | driverConfig     | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.1 | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | sk      | storefront   | BT2018       | mobilebrowser_sk | webTest    |

  @UAT_LG22 @sid:testIdJIRA
  Scenario Outline: Customer login on MYACCOUNT page-Success Login with wrong credential
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following wrong credential
      | email   | password   |
      | <email> | <password> |
    Then The user is notified with an error message
    And close the browser and clean all resources

    @UAT_LG22_ita
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                       | password    | country | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | ita     | default_ita  | webTest    |
      | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | ita     | default_ita  | webTest    |

    @UAT_LG22_cz
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                       | password    | country | authUsername | authPassword | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | cz      | storefront   | BT2018       | default_cz   | webTest    |
      | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | cz      | storefront   | BT2018       | default_cz   | webTest    |

    @UAT_LG22_mobileIta
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                       | password    | country | driverConfig      | typeOfTest  |
      | storefront   | BT2018       | UAT_LG-2.2 | tester002@prismaprogetti.it | x           | ita     | mobilebrowser_ita | androidTest |
      | storefront   | BT2018       | UAT_LG-2.3 | tester001@prismaprogetti.it | Prisma2018! | ita     | mobilebrowser_ita | androidTest |

    @UAT_LG22_pl
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.2 | prisma_placeanorder_test_pl@sharklasers.com   | x          | pl      | default_pl   | webTest    |
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_placeanorder_test_paaa@sharklasers.com | Alice.it0$ | pl      | default_pl   | webTest    |

    @UAT_LG22_mobilePl
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | driverConfig     | typeOfTest  |
      | storefront   | BT2018       | UAT_LG-2.2 | prisma_placeanorder_test_pl@sharklasers.com   | x          | pl      | mobilebrowser_pl | androidTest |
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_placeanorder_test_paaa@sharklasers.com | Alice.it0$ | pl      | mobilebrowser_pl | androidTest |

    @UAT_LG22_sk
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.2 | e2eceprisma+automation_generic_sk@gmail.com   | x          | sk      | default_sk   | webTest    |
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_skaceanorder_test_paaa@sharklasers.com | Alice.it0$ | sk      | default_sk   | webTest    |

    @UAT_LG22_mobileSk
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | driverConfig     | typeOfTest  |
      | storefront   | BT2018       | UAT_LG-2.2 | e2eceprisma+automation_generic_sk@gmail.com   | x          | sk      | mobilebrowser_sk | androidTest |
      | storefront   | BT2018       | UAT_LG-2.3 | prisma_skaceanorder_test_paaa@sharklasers.com | Alice.it0$ | sk      | mobilebrowser_sk | androidTest |

  @UAT_LG25 @sid:testIdJIRA
  Scenario Outline: Customer login on MYACCOUNT page-Login again just after logout
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert the following credential
      | email   | password   |
      | <email> | <password> |
    And The user logout to Bata site
    And The user logs in again to Bata site
      | email   | password   |
      | <email> | <password> |
    Then The user will be redirect on the profile page
    And close the browser and clean all resources

    @UAT_LG25_ita
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | default_ita  | webTest    |

    @UAT_LG25_cz
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                          | password     | country | authUsername | authPassword | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.5 | e2eceprisma+prisma40@gmail.com | testPrisma1! | cz      | storefront   | BT2018       | default_cz   | webTest    |

    @UAT_LG25_mobileIta
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                    | password   | country | driverConfig      | typeOfTest  |
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | ita     | mobilebrowser_ita | androidTest |

    @UAT_LG25_pl
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_placeanorder_test_paaa@sharklasers.com | Alice.it0$ | pl      | default_pl   | webTest    |

    @UAT_LG25_sk
    Examples: 
      | authUsername | authPassword | testIdJIRA | email                                         | password   | country | driverConfig | typeOfTest |
      | storefront   | BT2018       | UAT_LG-2.5 | prisma_skaceanorder_test_paaa@sharklasers.com | Alice.it0$ | sk      | default_sk   | webTest    |
