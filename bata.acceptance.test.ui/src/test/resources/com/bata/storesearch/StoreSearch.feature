@UAT_STR
Feature: Store search

  @UAT_STR1 @sid:testIdJIRA
  Scenario Outline: Store search-Use -search engine
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert Negozi in the search bar
      | searchField   |
      | <searchField> |
    Then The user will be redirect on store locator
    And close the browser and clean all resources

    @UAT_STR1_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | searchField |
      | UAT_STR-4.1 | webTest    | default_ita  | ita     | storefront   | BT2018       | Negozi      |

  @UAT_STR2 @sid:testIdJIRA
  Scenario Outline: Store search-Store locator-City-Address-CAP - Map Mode
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert field in the search bar with Map Mode
      | searchField   |
      | <searchField> |
    Then Website show closest stores based on input received
    And close the browser and clean all resources

    @UAT_STR2_ita
    Examples: 
      | testIdJIRA    | typeOfTest | driverConfig | country | authUsername | authPassword | searchField                              |
      | UAT_STR-4.2-1 | webTest    | default_ita  | ita     | storefront   | BT2018       | Napoli                                   |
      | UAT_STR-4.2-2 | webTest    | default_ita  | ita     | storefront   | BT2018       | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA |
      | UAT_STR-4.2-3 | webTest    | default_ita  | ita     | storefront   | BT2018       |                                    70056 |

  @UAT_STR3 @sid:testIdJIRA
  Scenario Outline: Store search-Store locator-City-Address-CAP - List Mode
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert field in the search bar with Map Mode
      | searchField   |
      | <searchField> |
    Then Website show closest stores based on input received
    And close the browser and clean all resources

    @UAT_STR3_ita
    Examples: 
      | testIdJIRA    | typeOfTest | driverConfig | country | authUsername | authPassword | searchField                              |
      | UAT_STR-4.2-5 | webTest    | default_ita  | ita     | storefront   | BT2018       | Napoli                                   |
      | UAT_STR-4.2-6 | webTest    | default_ita  | ita     | storefront   | BT2018       | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA |
      | UAT_STR-4.2-7 | webTest    | default_ita  | ita     | storefront   | BT2018       |                                    70056 |

  @UAT_STR4 @sid:testIdJIRA
  Scenario Outline: Store search-Refine search with filter - Original
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert field in the search bar with Map Mode
      | searchField   |
      | <searchField> |
    Then Website show just store with the refinement selected original
    And close the browser and clean all resources

    @UAT_STR4_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | searchField                              |
      | UAT_STR-4.7 | webTest    | default_ita  | ita     | storefront   | BT2018       | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA |

  @UAT_STR5 @sid:testIdJIRA
  Scenario Outline: Store search-Refine search with filter - Franchising
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user go to BATA site
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    When The user insert field in the search bar with Map Mode
      | searchField   |
      | <searchField> |
    Then Website show just store with the refinement selected franchising
    And close the browser and clean all resources

    @UAT_STR5_ita
    Examples: 
      | testIdJIRA  | typeOfTest | driverConfig | country | authUsername | authPassword | searchField                              |
      | UAT_STR-4.8 | webTest    | default_ita  | ita     | storefront   | BT2018       | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA |
