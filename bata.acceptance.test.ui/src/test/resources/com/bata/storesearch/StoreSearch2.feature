Feature: UAT_STR: Store search
#######################################################
#COUNTRY ITA
#######################################################

@ita
  Scenario: Store search-Use -search engine
    Given the JIRA test
      | testIdJIRA  | typeOfTest  |
      | UAT_STR-4.1 | androidTest |
    When The user go to BATA site
      | country | authUsername | authPassword |
      | ita     | storefront   | BT2018       |
    When The user insert Negozi in the search bar
      | searchField |
      | Negozi      |
    Then The user will be redirect on store locator
    And close the browser and clean all resources
@ita
  Scenario Outline: Store search-Store locator-City-Address-CAP - Map Mode
    Given the JIRA test
      | testIdJIRA   | typeOfTest  |
      | <testIdJIRA> | androidTest |
    When The user go to BATA site
      | country   | authUsername | authPassword |
      | <country> | storefront   | BT2018       |
    When The user insert field in the search bar with Map Mode
      | searchField   |
      | <searchField> |
    Then Website show closest stores based on input received
    And close the browser and clean all resources

    Examples: 
      | testIdJIRA  | searchField                              | country |
      | UAT_STR-4.2 | Napoli                                   | ita     |
      | UAT_STR-4.2 | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA | ita     |
      | UAT_STR-4.2 |                                    70056 | ita     |
@ita
  Scenario Outline: Store search-Store locator-City-Address-CAP - List Mode
    Given the JIRA test
      | testIdJIRA   | typeOfTest  |
      | <testIdJIRA> | androidTest |
    When The user go to BATA site
      | country   | authUsername | authPassword |
      | <country> | storefront   | BT2018       |
    When The user insert field in the search bar with List Mode
      | searchField   |
      | <searchField> |
    Then Website show closest stores based on input received
    And close the browser and clean all resources

    Examples: 
      | testIdJIRA  | searchField                              | country |
      | UAT_STR-4.2 | Napoli                                   | ita     |
      | UAT_STR-4.2 | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA | ita     |
      | UAT_STR-4.2 |                                    70056 | ita     |
@ita
  Scenario: Store search-Refine search with filter - Original
    Given the JIRA test
      | testIdJIRA  | typeOfTest  |
      | UAT_STR-4.7 | androidTest |
    When The user go to BATA site
      | country | authUsername | authPassword |
      | ita     | storefront   | BT2018       |
    When The user insert field in the search bar with Original filter
      | searchField                              |
      | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA |
    Then Website show just store with the refinement selected original
    And close the browser and clean all resources
@ita
  Scenario: Store search-Refine search with filter - Franchising
    Given the JIRA test
      | testIdJIRA  | typeOfTest  |
      | UAT_STR-4.8 | androidTest |
    When The user go to BATA site
      | country | authUsername | authPassword |
      | ita     | storefront   | BT2018       |
    When The user insert field in the search bar with Franchising filter
      | searchField                              |
      | BATA VIAOLIVETTI CC GRANDSHOPMONGOLFIERA |
    Then Website show just store with the refinement selected franchising
    And close the browser and clean all resources

#######################################################
#COUNTRY PL OUT OF SCOPE (NEGOZI NON PRESENTE)
#######################################################