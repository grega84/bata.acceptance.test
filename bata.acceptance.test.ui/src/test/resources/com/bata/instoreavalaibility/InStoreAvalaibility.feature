@UAT_ISA
Feature: In store avalaibility

  @UAT_ISA14 @sid:testIdJIRA
  Scenario Outline: in store avalaibility for guest user
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    @UAT_ISA14_ita
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA   | country | productName                  | size | criteria   | key                 |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-14.1 | ita     | MOCASSINI IN VERNICE DA UOMO |      | geoip      |                     |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-14.2 | ita     | MOCASSINI IN VERNICE DA UOMO |      | city       | formia              |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-14.3 | ita     | MOCASSINI IN VERNICE DA UOMO |      | address    | VIA DUE MACELLI     |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-14.4 | ita     | MOCASSINI IN VERNICE DA UOMO |      | postalcode | 82100;Benevento     |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-14.5 | ita     | NIKE REVOLUTION 4            |      | wrong      | STORE NON ESISTENTE |

    @UAT_ISA14_cz
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA   | country | productName                   | size | criteria   | key                         |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-14.1 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | geoip      |                             |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-14.2 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | city       | Ostrava                     |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-14.3 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | address    | Shoping Park Rudná 114/3114 |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-14.4 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | postalcode | 71500;Ostrava               |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-14.5 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | wrong      | STORE NON ESISTENTE         |

    @UAT_ISA14_sk
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA   | country | productName       | size | criteria   | key                 |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-14.1 | sk      | členkové topánky  |      | geoip      |                     |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-14.2 | sk      | členkové topánky  |      | city       | Košice - Optima     |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-14.3 | sk      | členkové topánky  |      | address    | Moldavská cesta 32  |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-14.4 | sk      | členkové topánky  |      | postalcode | 82100;Bratislava    |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-14.5 | sk      | NIKE REVOLUTION 4 |      | wrong      | STORE NON ESISTENTE |

  @UAT_ISA15 @sid:testIdJIRA
  Scenario Outline: in store avalaibility for registered user
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    @UAT_ISA15_ita
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA   | country | productName                  | size | criteria   | key                 | email                                    | password   |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-15.1 | ita     | MOCASSINI IN VERNICE DA UOMO |      | geoip      |                     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-15.2 | ita     | MOCASSINI IN VERNICE DA UOMO |      | city       | formia              | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-15.3 | ita     | MOCASSINI IN VERNICE DA UOMO |      | address    | VIA DUE MACELLI     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-15.4 | ita     | MOCASSINI IN VERNICE DA UOMO |      | postalcode | 82100;Benevento     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_ISA-15.5 | ita     | NIKE REVOLUTION 4            |      | wrong      | STORE NON ESISTENTE | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_ISA15_cz
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA   | country | productName                   | size | criteria   | key                         | email                                    | password   |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-15.1 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | geoip      |                             | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-15.2 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | city       | Ostrava                     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-15.3 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | address    | Shoping Park Rudná 114/3114 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-15.4 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | postalcode | 71500;Ostrava               | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_ISA-15.5 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |      | wrong      | STORE NON ESISTENTE         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_ISA15_sk
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA   | country | productName       | size | criteria   | key                 | email                                       | password   |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-15.1 | sk      | členkové topánky  |      | geoip      |                     | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-15.2 | sk      | členkové topánky  |      | city       | Košice - Optima     | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-15.3 | sk      | členkové topánky  |      | address    | Moldavská cesta 32  | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-15.4 | sk      | členkové topánky  |      | postalcode | 82100;Bratislava    | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_ISA-15.5 | sk      | NIKE REVOLUTION 4 |      | wrong      | STORE NON ESISTENTE | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
