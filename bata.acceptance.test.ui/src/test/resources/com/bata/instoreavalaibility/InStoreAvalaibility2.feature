Feature: UAT_ISA: In store avalaibility

  ############################################################################
  # COUNTRY ITA
  ############################################################################
  @ita
  Scenario Outline: in store avalaibility for guest user
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    Examples: 
     | authUsername | authPassword  | testIdJIRA | country | productName                  | size | criteria   | key                 |
     | storefront   | BT2018        | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | geoip      |                     |
     | storefront   | BT2018        | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | city       | formia              |
     | storefront   | BT2018        | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | address    | VIA DUE MACELLI     |
     | storefront   | BT2018        | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | postalcode | 82100;Benevento     |
     | storefront   | BT2018        | UAT_ISA-14 | ita     | NIKE REVOLUTION 4            |    6 | wrong      | STORE NON ESISTENTE |

  @ita
  Scenario Outline: in store avalaibility for registered user
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername | authPassword |
      | <country> | <email> | <password> |<authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA | country | productName                  | size | criteria   | key                 | email                                    | password   |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | geoip      |                     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | city       | formia              | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | address    | VIA DUE MACELLI     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | MOCASSINI IN VERNICE DA UOMO |   45 | postalcode | 82100;Benevento     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | NIKE REVOLUTION 4            |    6 | wrong      | STORE NON ESISTENTE | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

  ############################################################################
  # COUNTRY CZ
  ############################################################################
  @cz
  Scenario Outline: in store avalaibility for guest user
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA | country | productName                   | size | criteria   | key                         |
      | storefront   | BT2018       | UAT_ISA-14 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | geoip      |                             |
      | storefront   | BT2018       | UAT_ISA-14 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | city       | Ostrava                     |
      | storefront   | BT2018       | UAT_ISA-14 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | address    | Shoping Park Rudná 114/3114 |
      | storefront   | BT2018       | UAT_ISA-14 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | postalcode | 71500;Ostrava               |
      | storefront   | BT2018       | UAT_ISA-14 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | wrong      | STORE NON ESISTENTE         |

  @cz
  Scenario Outline: in store avalaibility for registered user
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername | authPassword |
      | <country> | <email> | <password> |<authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA | country | productName                   | size | criteria   | key                         | email                                    | password   |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | geoip      |                             | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | city       | Ostrava                     | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | address    | Shoping Park Rudná 114/3114 | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | postalcode | 71500;Ostrava               | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | ita     | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |   42 | wrong      | STORE NON ESISTENTE         | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

      
   ############################################################################
  # COUNTRY SK
  ############################################################################
  @sk
  Scenario Outline: in store avalaibility for guest user
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA site without credential
      | country   | authUsername | authPassword |
      | <country> |<authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    Examples: 
     | authUsername | authPassword  | testIdJIRA | country | productName                  | size | criteria   | key                 |
     | storefront   | BT2018        | UAT_ISA-14 | sk     | členkové topánky |   37 | geoip      |                     |
     | storefront   | BT2018        | UAT_ISA-14 | sk     | členkové topánky |   37 | city       | Košice - Optima              |
     | storefront   | BT2018        | UAT_ISA-14 | sk     | členkové topánky |   37 | address    | Moldavská cesta 32     |
     | storefront   | BT2018        | UAT_ISA-14 | sk     | členkové topánky |   37 | postalcode | 82100;Bratislava     |
     | storefront   | BT2018        | UAT_ISA-14 | sk     | NIKE REVOLUTION 4            |    7 | wrong      | STORE NON ESISTENTE |

  @sk
  Scenario Outline: in store avalaibility for registered user
    Given the JIRA test
      | testIdJIRA   | typeOfTest |
      | <testIdJIRA> | webTest    |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername | authPassword |
      | <country> | <email> | <password> |<authUsername> | <authPassword> |
    And Go on a specific product detail page
      | productName   |
      | <productName> |
    And Select a size
      | size   |
      | <size> |
    And Click on find in store button
    And Search an store based on this criteria
      | criteria   | key   |
      | <criteria> | <key> |
    And the store list is displayed
      | criteria   | key   |
      | <criteria> | <key> |
    And click on load more button
      | criteria   | key   |
      | <criteria> | <key> |
    And close the browser and clean all resources

    Examples: 
      | authUsername | authPassword | testIdJIRA | country | productName                  | size | criteria   | key                 | email                                    | password   |
      | storefront   | BT2018       | UAT_ISA-14 | sk     | členkové topánky |   37 | geoip      |                     | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | sk     | členkové topánky |   37 | city       | Košice - Optima              | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | sk     | členkové topánky |   37 | address    | Moldavská cesta 32     | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | sk     | členkové topánky |   37 | postalcode | 82100;Bratislava     | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
      | storefront   | BT2018       | UAT_ISA-14 | sk     | NIKE REVOLUTION 4            |    7 | wrong      | STORE NON ESISTENTE | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
 