Feature: UAT_CONT: Send Contact request
############################################################################
# COUNTRY ITA
############################################################################
@ita
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And go to send contact page
      And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> | 
     When send the request
     Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   | 
      | storefront   | BT2018       | UAT_CONT-15.1 | ita     | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.2 | ita     | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.3 | ita     | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.4 | ita     | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.5 | ita     | automation.test.prisma@gmail.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
  
  ############################################################################
  # COUNTRY CZ
  ############################################################################
  @cz
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And go to send contact page
      And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> | 
     When send the request
     Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   | 
      | storefront   | BT2018       | UAT_CONT-15.1 | cz      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.2 | cz      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.3 | cz      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.4 | cz      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.5 | cz      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
  
  ############################################################################
  # COUNTRY ITA MOBILE
  ############################################################################
  @ita @mobile
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And go to send contact page
      And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> | 
     When send the request
     Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   | 
      | storefront   | BT2018       | UAT_CONT-15.1 | ita     | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.2 | ita     | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.3 | ita     | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.4 | ita     | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.5 | ita     | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
  
  ############################################################################
  # COUNTRY PL
  ############################################################################
  @pl
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And go to send contact page
      And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> | 
     When send the request
     Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   | 
      | storefront   | BT2018       | UAT_CONT-15.1 | pl      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.2 | pl      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.3 | pl      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.4 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.5 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
  
  ############################################################################
  # COUNTRY PL MOBILE
  ############################################################################
  @pl @mobile
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And go to send contact page
      And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> | 
     When send the request
     Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   | 
      | storefront   | BT2018       | UAT_CONT-15.1 | pl      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.2 | pl      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.3 | pl      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.4 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.5 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      
      
       ############################################################################
  # COUNTRY PL
  ############################################################################
 @sk
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And go to send contact page
      And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> | 
     When send the request
     Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   | 
      | storefront   | BT2018       | UAT_CONT-15.1 | sk      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.2 | sk      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.3 | sk      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.4 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.5 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
  
  ############################################################################
  # COUNTRY PL MOBILE
  ############################################################################
 @sk @mobile
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
      And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And go to send contact page
      And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> | 
     When send the request
     Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   | 
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> | 
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   | 
      | storefront   | BT2018       | UAT_CONT-15.1 | sk      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.2 | sk      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.3 | sk      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.4 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} | 
      | storefront   | BT2018       | UAT_CONT-15.5 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |  