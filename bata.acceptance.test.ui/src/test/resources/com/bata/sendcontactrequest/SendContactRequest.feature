@UAT_CONT
Feature: Send Contact request

  @UAT_CONT15 @sid:testIdJIRA
  Scenario Outline: Send Contact request email notification
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   | driverMailConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> | <driverMailConfig> |
    And The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And go to send contact page
    And complete all fields with following data
      | email          | phone   | name   | surname   | reason   | comment   |
      | <emailAddress> | <phone> | <name> | <surname> | <reason> | <comment> |
    When send the request
    Then check the notification mail is sent to the email address
      | email          | emailSubject   | phone   | name   | surname   | reason   | comment   |
      | <emailAddress> | <emailSubject> | <phone> | <name> | <surname> | <reason> | <comment> |
    And close the browser and clean all resources

    @UAT_CONT15_ita
    Examples: 
      | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | emailAddress                           | phone      | name  | surname   | reason   | comment                      | emailSubject                                   |
      | webTest    | default_ita  | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.1 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_ita  | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.2 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_ita  | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.3 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_ita  | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.4 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_ita  | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.5 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |

    @UAT_CONT15_cz
    Examples: 
      | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   |
      | webTest    | default_cz   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.1 | cz      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_cz   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.2 | cz      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_cz   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.3 | cz      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_cz   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.4 | cz      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_cz   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.5 | cz      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |

    @UAT_CONT15_mobileIta
    Examples: 
      | typeOfTest  | driverConfig      | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | emailAddress                           | phone      | name  | surname   | reason   | comment                      | emailSubject                                   |
      | androidTest | mobilebrowser_ita | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.1 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_ita | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.2 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_ita | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.3 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_ita | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.4 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_ita | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.5 | ita     | automation.test.prisma@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |

    @UAT_CONT15_pl
    Examples: 
      | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   |
      | webTest    | default_pl   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.1 | pl      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_pl   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.2 | pl      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_pl   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.3 | pl      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_pl   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.4 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_pl   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.5 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |

    @UAT_CONT15_mobilePl
    Examples: 
      | typeOfTest  | driverConfig     | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   |
      | androidTest | mobilebrowser_pl | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.1 | pl      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_pl | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.2 | pl      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_pl | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.3 | pl      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_pl | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.4 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_pl | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.5 | pl      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |

    @UAT_CONT15_sk
    Examples: 
      | typeOfTest | driverConfig | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   |
      | webTest    | default_sk   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.1 | sk      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_sk   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.2 | sk      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_sk   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.3 | sk      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_sk   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.4 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | webTest    | default_sk   | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.5 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |

    @UAT_CONT15_mobileSk
    Examples: 
      | typeOfTest  | driverConfig     | driverMailConfig | authUsername | authPassword | testIdJIRA    | country | emailAddress                       | phone      | name  | surname   | reason   | comment                      | emailSubject                                   |
      | androidTest | mobilebrowser_sk | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.1 | sk      | test0002prismabata@sharklasers.com | 3333333333 | Nello | Specifico | delivery | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_sk | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.2 | sk      | test0003prismabata@sharklasers.com | 3333333333 | Nello | Specifico | payment  | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_sk | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.3 | sk      | test0004prismabata@sharklasers.com | 3333333333 | Nello | Specifico | discount | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_sk | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.4 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | bataclub | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
      | androidTest | mobilebrowser_sk | guerrillaMail    | storefront   | BT2018       | UAT_CONT-15.5 | sk      | test0005prismabata@sharklasers.com | 3333333333 | Nello | Specifico | other    | test prova commento con link | BATA richiesta di contatto ricevuta: ${reason} |
