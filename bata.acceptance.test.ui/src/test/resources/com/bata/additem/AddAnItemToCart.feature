@UAT_AIC
Feature: Add an item to cart

  @UAT_AIC1 @sid:testIdJIRA
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   |
      | <productName> | <quantity> | <size> | <color> |
    Then Check that item will be correctly add to cart
    And close the browser and clean all resources

    @UAT_AIC1_ita
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       |

    @UAT_AIC1_mobileIta
    Examples: 
      | typeOfTest  | driverConfig      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | androidTest | mobilebrowser_ita | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       |

    @UAT_AIC1_cz
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName                   | quantity | size | color |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_AIC-7.1 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |          |      |       |

    @UAT_AIC1_pl
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | default_pl   | storefront   | BT2018       | UAT_AIC-7.1 | pl      | nike        |          |      |       |

    @UAT_AIC1_mobilePl
    Examples: 
      | typeOfTest  | driverConfig     | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | androidTest | mobilebrowser_pl | storefront   | BT2018       | UAT_AIC-7.1 | pl      | nike        |          |      |       |

    @UAT_AIC1_sk
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_AIC-7.1 | sk      | nike        |          |      |       |

    @UAT_AIC1_mobileSk
    Examples: 
      | typeOfTest  | driverConfig     | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | androidTest | mobilebrowser_sk | storefront   | BT2018       | UAT_AIC-7.1 | sk      | nike        |          |      |       |

  @UAT_AIC2 @sid:testIdJIRA
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   |
      | <productName> | <quantity> | <size> | <color> |
    Then Check that item will be correctly add to cart
    And close the browser and clean all resources

    @UAT_AIC2_ita
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_AIC2_mobileIta
    Examples: 
      | typeOfTest  | driverConfig      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   |
      | androidTest | mobilebrowser_ita | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_AIC2_cz
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                          | password     |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_AIC-7.1 | cz      |     5411600 |          |      |       | e2eceprisma+prisma40@gmail.com | testPrisma1! |

    @UAT_AIC2_pl
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   |
      | webTest    | default_pl   | storefront   | BT2018       | UAT_AIC-7.1 | pl      | nike        |          |      |       | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |

    @UAT_AIC2_mobilePl
    Examples: 
      | typeOfTest  | driverConfig     | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   |
      | androidTest | mobilebrowser_pl | storefront   | BT2018       | UAT_AIC-7.1 | pl      | nike        |          |      |       | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |

    @UAT_AIC2_sk
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_AIC-7.1 | sk      | nike        |          |      |       | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |

    @UAT_AIC2_mobileSk
    Examples: 
      | typeOfTest  | driverConfig     | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   |
      | androidTest | mobilebrowser_sk | storefront   | BT2018       | UAT_AIC-7.1 | sk      | nike        |          |      |       | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |

  @UAT_AIC3 @sid:testIdJIRA
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   |
      | <country> | <authUsername> | <authPassword> |
    And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   |
      | <productName> | <quantity> | <size> | <color> |
    Then Check that item will be correctly add to cart
    And close the browser and clean all resources

    @UAT_AIC3_ita
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       |

    @UAT_AIC3_cz
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_AIC-7.1 | cz      | SANDÁLY     |          |      |       |

    @UAT_AIC3_pl
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | default_pl   | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       |

    @UAT_AIC3_sk
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       |

  @UAT_AIC4 @sid:testIdJIRA
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest   | driverConfig   |
      | <testIdJIRA> | <typeOfTest> | <driverConfig> |
    When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   |
      | <country> | <email> | <password> | <authUsername> | <authPassword> |
    And if the cart isnt empty
    Then clear the cart
    And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   |
      | <productName> | <quantity> | <size> | <color> |
    Then Check that item will be correctly add to cart
    And close the browser and clean all resources

    @UAT_AIC4_ita
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   |
      | webTest    | default_ita  | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ |

    @UAT_AIC4_cz
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                          | password     |
      | webTest    | default_cz   | storefront   | BT2018       | UAT_AIC-7.1 | cz      | SANDÁLY     |          |      |       | e2eceprisma+prisma40@gmail.com | testPrisma1! |

    @UAT_AIC4_pl
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   |
      | webTest    | default_pl   | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |

    @UAT_AIC4_sk
    Examples: 
      | typeOfTest | driverConfig | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   |
      | webTest    | default_sk   | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |
