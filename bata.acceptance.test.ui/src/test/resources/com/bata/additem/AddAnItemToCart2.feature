Feature: UAT_AIC: Add an item to cart

###########################################################################
# COUNTRY ITA
###########################################################################
@ita
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest |  driverConfig |
      | <testIdJIRA> | webTest    |  default |
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       | 
  
  @ita
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest |  driverConfig |
      | <testIdJIRA> | webTest    |  default |
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   | 
      | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  @ita
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest | driverConfig | 
      | <testIdJIRA> | webTest    |  default |
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       | 
  
  @ita
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest | driverConfig | 
      | <testIdJIRA> | webTest    |  default |
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   | 
      | storefront   | BT2018       | UAT_AIC-7.2 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  #
  ###########################################################################
  # COUNTRY CZ
  ###########################################################################
  @cz
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName                   | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | cz      | KOŽENÉ PÁNSKÉ NAZOUVÁKY MODRÉ |          |      |       | 
  
  @cz
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                          | password     | 
      | storefront   | BT2018       | UAT_AIC-7.1 | cz      | 5411600     |          |      |       | e2eceprisma+prisma40@gmail.com | testPrisma1! | 
  @cz
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | cz      | SANDÁLY     |          |      |       | 
  @cz
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                          | password     | 
      | storefront   | BT2018       | UAT_AIC-7.1 | cz      | SANDÁLY     |          |      |       | e2eceprisma+prisma40@gmail.com | testPrisma1! | 
  
  ###########################################################################
  # COUNTRY ITA MOBILE
  ###########################################################################
  @ita @mobile
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       | 
  
  @ita @mobile
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                    | password   | 
      | storefront   | BT2018       | UAT_AIC-7.1 | ita     | nike        |          |      |       | prisma_placeanorder_test@sharklasers.com | Alice.it0$ | 
  
  ###########################################################################
  # COUNTRY PL
  ###########################################################################
  @pl
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | pl      | nike        |          |      |       | 
  
  @pl
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   | 
      | storefront   | BT2018       | UAT_AIC-7.1 | pl      | nike        |          |      |       | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  @pl
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       | 
  
  @pl
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   | 
      | storefront   | BT2018       | UAT_AIC-7.2 | pl      | nike        |          |      |       | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ | 
  
  ###########################################################################
  # COUNTRY PL MOBILE
  ###########################################################################
  @pl @mobile
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | pl     | nike        |          |      |       | 
  
  @pl @mobile
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   | 
      | storefront   | BT2018       | UAT_AIC-7.1 | pl      | nike        |          |      |       | prisma_placeanorder_test_pl@sharklasers.com | Alice.it0$ |
      
        ###########################################################################
  # COUNTRY SK
  ###########################################################################
  @sk
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | sk      | nike        |          |      |       | 
  
  @sk
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   | 
      | storefront   | BT2018       | UAT_AIC-7.1 | sk      | nike        |          |      |       | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | 
  
  @sk
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       | 
  
  @sk
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest | 
      | <testIdJIRA> | webTest    | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PLP - Quick Buy"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   | 
      | storefront   | BT2018       | UAT_AIC-7.2 | sk      | nike        |          |      |       | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ | 
  
  ###########################################################################
  # COUNTRY PL MOBILE
  ###########################################################################
  @sk @mobile
  Scenario Outline: Add item to cart-From PDP - guest
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA site without credential
      | country   | authUsername   | authPassword   | 
      | <country> | <authUsername> | <authPassword> | 
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | 
      | storefront   | BT2018       | UAT_AIC-7.1 | sk     | nike        |          |      |       | 
  
  @sk @mobile
  Scenario Outline: Add item to cart-From PDP - registered
    Given the JIRA test
      | testIdJIRA   | typeOfTest  | 
      | <testIdJIRA> | androidTest | 
     When The user log in to BATA IT site with credential
      | country   | email   | password   | authUsername   | authPassword   | 
      | <country> | <email> | <password> | <authUsername> | <authPassword> | 
      And if the cart isnt empty
     Then clear the cart
      And add an item to cart from "PDP"
      | productName   | quantity   | size   | color   | 
      | <productName> | <quantity> | <size> | <color> | 
     Then Check that item will be correctly add to cart
      And close the browser and clean all resources
  
    Examples: 
      | authUsername | authPassword | testIdJIRA  | country | productName | quantity | size | color | email                                       | password   | 
      | storefront   | BT2018       | UAT_AIC-7.1 | sk      | nike        |          |      |       | e2eceprisma+automation_generic_sk@gmail.com | Alice.it0$ |  